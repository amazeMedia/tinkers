(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = function (carousel, settings) {

    var effect_base = require('./effect_base.js');
    var vector = require('./../vector.js');
    var rotation = require('./../rotation.js');

    var base = new effect_base(carousel, {
        
    });

    this.xDirection = new vector();
    this.xDirection.x = 1;

    this.yDirection = new vector();
    this.yDirection.y = 1;
    
    $.extend(this, base);
    $.extend(this.settings, settings);

    this.apply = function (elementTransform, element, value) {

        var currentPoint = this.carousel.path.getPoint(value);
        var directionPoint = this.carousel.path.getPoint(value + 0.1);
        
        var direction = new vector();
        direction.initFromPoints(currentPoint, directionPoint);
        var angle = direction.angle(this.xDirection);

        angle *= this.carousel.options.allignElementsWithPathCoefficient;

        elementTransform.rotations.push(new rotation(this.yDirection, angle));
    };
};
},{"./../rotation.js":30,"./../vector.js":35,"./effect_base.js":2}],2:[function(require,module,exports){
module.exports = function (carousel, settings) {

    this.carousel = carousel;

    this.applyPhase = 'positioning';

    this.settings = {
    };

    $.extend(this.settings, settings);

    this.getApplyingPriority = function () {
        return 0;
    };

    this.apply = function (elementTransform, element, value) {

    };

    this.revert = function () {

    };
}
},{}],3:[function(require,module,exports){
module.exports = function (carousel, settings) {

    var effect_base = require('./effect_base.js');
    var getBezier = require('./../bezier.js');
    
    var base = new effect_base(carousel, {

    });

    
    $.extend(this, base);
    $.extend(this.settings, settings);

    this.apply = function (elementTransform, element, value) {
        var root = this.carousel.path.rootValue();
        var distance = Math.abs(value - root);
        var absDist = this.carousel.options.distance * this.carousel.options.fadeAwayNumberOfConfigurableElements;

        var bezierT = 1 - Math.min(distance / absDist, 1);

        var points = this.carousel.options.fadeAwayBezierPoints;
        var opacity = getBezier(bezierT, points.p1, points.p2, points.p3, points.p4).y;
        element.$element.css({ opacity: opacity / 100 });
    };
};
},{"./../bezier.js":10,"./effect_base.js":2}],4:[function(require,module,exports){
module.exports = function (carousel, settings) {

    var effect_base = require('./effect_base.js');

    var base = new effect_base(carousel, {

    });

    $.extend(this, base);
    $.extend(this.settings, settings);

    this.apply = function (elementTransform, element, value) {
        var distFromRoot = Math.abs(value - this.carousel.path.rootValue());
        var coef = 1 - distFromRoot / this.carousel.options.distance;
        coef = Math.max(coef, 0);

        elementTransform.translateX += this.carousel.options.popoutSelectedShiftX * coef;
        elementTransform.translateY += this.carousel.options.popoutSelectedShiftY * coef;
        elementTransform.translateZ += this.carousel.options.popoutSelectedShiftZ * coef;
    };
};
},{"./effect_base.js":2}],5:[function(require,module,exports){
module.exports = function (carousel, settings) {

    var effect_base = require('./effect_base.js');

    var base = new effect_base(carousel, {

    });

    $.extend(this, base);
    $.extend(this.settings, settings);

    this.apply = function (elementTransform, element, value) {
        
        if(element.isReflectionApplied)
            return;

        element.isReflectionApplied = true;
        
        element.$element.css("box-reflect", "below " + this.carousel.options.reflectionBelow +
            "px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(" +
             (1 - this.carousel.options.reflectionHeight) + ", transparent), to(black))");
    };

    this.revert = function () {
        for (var i = 0; i < this.carousel.elements.length; i++) {
            this.carousel.elements[i].isReflectionApplied = false;
            this.carousel.elements[i].$element.css("box-reflect", '');
        }
    };
};
},{"./effect_base.js":2}],6:[function(require,module,exports){
module.exports = function (carousel, settings) {

    var effect_base = require('./effect_base.js');
    var getBezier = require('./../bezier.js');
    var vector = require('./../vector.js');
    var rotation = require('./../rotation.js');

    var base = new effect_base(carousel, {

    });

    $.extend(this, base);
    $.extend(this.settings, settings);

    this.apply = function (elementTransform, element, value) {
        var root = this.carousel.path.rootValue();
        var distance = Math.abs(value - root);
        var absDist = this.carousel.options.distance * this.carousel.options.rotationNumberOfConfigurableElements;

        var bezierT = 1 - Math.min(distance / absDist, 1);

        var points = this.carousel.options.rotationBezierPoints;
        var angle = getBezier(bezierT, points.p1, points.p2, points.p3, points.p4).y;

        if (value < root && this.carousel.options.rotationInvertForNegative)
            angle *= -1;

        var rotationVector = new vector();
        rotationVector.x = this.carousel.options.rotationVectorX,
        rotationVector.y = this.carousel.options.rotationVectorY,
        rotationVector.z = this.carousel.options.rotationVectorZ,
        elementTransform.rotations.push(new rotation(rotationVector, angle));
    };
};
},{"./../bezier.js":10,"./../rotation.js":30,"./../vector.js":35,"./effect_base.js":2}],7:[function(require,module,exports){
module.exports = function (carousel, settings) {

    var effect_base = require('./effect_base.js');

    var base = new effect_base(carousel, {

    });

    $.extend(this, base);
    $.extend(this.settings, settings);

    this.apply = function (elementTransform, element, value) {

        var boxShadow = '0px 0px ' + this.carousel.options.shadowBlurRadius + 'px ' + this.carousel.options.shadowSpreadRadius + 'px #000000';

        element.$element.css({
            '-webkit-box-shadow': boxShadow,
            '-moz-box-shadow': boxShadow,
            'box-shadow': boxShadow
        });
    };

    this.revert = function () {
        for (var i = 0; i < this.carousel.elements.length; i++) {
            this.carousel.elements[i].$element.css({
                '-webkit-box-shadow': '',
                '-moz-box-shadow': '',
                'box-shadow': ''
            });
        }
    };
};
},{"./effect_base.js":2}],8:[function(require,module,exports){
module.exports = function (carousel, settings) {

    var effect_base = require('./effect_base.js');
    var getBezier = require('./../bezier.js');

    var base = new effect_base(carousel, {

    });


    $.extend(this, base);
    $.extend(this.settings, settings);

    this.apply = function (elementTransform, element, value) {
        var root = this.carousel.path.rootValue();
        var distance = Math.abs(value - root);
        var absDist = this.carousel.options.distance * this.carousel.options.sizeAdjustmentNumberOfConfigurableElements;

        var bezierT = 1 - Math.min(distance / absDist, 1);

        var points = this.carousel.options.sizeAdjustmentBezierPoints;
        var scale = getBezier(bezierT, points.p1, points.p2, points.p3, points.p4).y;
        elementTransform.scale = scale / 100;
    };
};
},{"./../bezier.js":10,"./effect_base.js":2}],9:[function(require,module,exports){
module.exports = function (carousel) {

    this.autorotationStarted = false;
    this.carousel = carousel;
    this.autorotationTimer = null;

    extend(this);
    this.carousel.widget().on("motionEnd", this.onMotionEnd);
    this.applySettings();
};

function extend(obj) {
    obj.applySettings = function () {
        if (this.carousel.options.autorotation)
            this.ensureStarted();
        else {
            this.autorotationStarted = false;
            if (this.autorotationTimer)
                clearTimeout(this.autorotationTimer);
        }
    };

    obj.ensureStarted = function() {
        if (!this.autorotationStarted) {
            this.autorotationStarted = !!(this.move());
        }
    };

    obj.move = $.proxy(function() {
        if (this.carousel.options.autorotationDirection == 'left') {
            return this.carousel.moveBack();
        }

        if (this.carousel.options.autorotationDirection == 'right') {
            return this.carousel.moveForward();
        }
    }, obj);

    obj.onMotionEnd = $.proxy(function () {
        if (this.autorotationStarted) {
            this.autorotationTimer = setTimeout(this.move, this.carousel.options.autorotationPause);
        }
    }, obj);

    obj.destroy = function() {
        this.carousel.widget().off("motionEnd", this.onMotionEnd);
    }
}
},{}],10:[function(require,module,exports){
function b1(t) { return t * t * t; };

function b2(t) { return 3 * t * t * (1 - t); };

function b3(t) { return 3 * t * (1 - t) * (1 - t); };

function b4(t) { return (1 - t) * (1 - t) * (1 - t); };

module.exports = function(t, p1, cp1, cp2, p2) {
    var pos = {x:0,y:0};
    pos.x = p1.x*b1(t) + cp1.x*b2(t) + cp2.x*b3(t) + p2.x*b4(t);
    pos.y = p1.y*b1(t) + cp1.y*b2(t) + cp2.y*b3(t) + p2.y*b4(t);
    return pos;
};
},{}],11:[function(require,module,exports){
var getBezier = require('./bezier.js')

var bezierBoxCache = [];

function getBezierBox(p1, cp1, cp2, p2) {
    var point = getBezier(0, p1, cp1, cp2, p2);
    var minX = point.x;
    var minY = point.y;
    var maxX = point.x;
    var maxY = point.y;

    for (var i = 0; i <= 20; i++) {
        var tempPoint = getBezier(i * 0.05, p1, cp1, cp2, p2);
        minX = Math.min(tempPoint.x, minX);
        minY = Math.min(tempPoint.y, minY);
        maxX = Math.max(tempPoint.x, maxX);
        maxY = Math.max(tempPoint.y, maxY);
    }

    return {
        minX: minX,
        minY: minY,
        maxX: maxX,
        maxY: maxY,
        width: maxX - minX,
        height: maxY - minY
    };
};

module.exports = function(p1, cp1, cp2, p2) {
    var key = p1.x + '.' + p1.y +
        '.' + cp1.x + '.' + cp1.y +
        '.' + cp2.x + '.' + cp2.y +
        '.' + p2.x + '.' + p2.y;

    if (typeof (bezierBoxCache[key]) == "undefined")
        bezierBoxCache[key] = getBezierBox(p1, cp1, cp2, p2);
    return bezierBoxCache[key];
};
},{"./bezier.js":10}],12:[function(require,module,exports){
require('./easings.js')(); // patch jquery with easings

/*!
 * TODO: add about
 * 
 */

(function ($) {
    if (!$.theta) {
        $.theta = new Object();
    };

    var comulative_animation = require('./comulative_animation.js');
    var fluid_layout = require('./fluid_layout.js');
    var elements_size_updater = require('./elements_size_updater.js');
    var input_controller = require('./input_controller.js');
    var motion_controller = require('./motion_controller.js');
    var rotation_logic_controller = require('./rotation_logic_controller.js');
    var endless_rotation_logic_controller = require('./endless_rotation_logic_controller.js');
    var fallback_rotation_logic_controller = require('./fallback_rotation_logic_controller.js');
    var auto_rotator = require('./auto_rotator.js');
    var size = require('./size.js');
    var utils = require('./utils.js');

    var path_archimedes_spiral = require('./paths/path_archimedes_spiral.js');
    var path_cubic = require('./paths/path_cubic.js');
    var path_cubic_bezier = require('./paths/path_cubic_bezier.js');
    var path_ellipse = require('./paths/path_ellipse.js');
    var path_line = require('./paths/path_line.js');
    var path_parabola = require('./paths/path_parabola.js');
    
    var effect_allign_to_path = require('./VisualEffects/effect_allign_to_path.js');
    var effect_fade_away = require('./VisualEffects/effect_fade_away.js');
    var effect_pop_out_selected = require('./VisualEffects/effect_pop_out_selected.js');
    var effect_rotation = require('./VisualEffects/effect_rotation.js');
    var effect_shadow = require('./VisualEffects/effect_shadow.js');
    var effect_size_adjustment = require('./VisualEffects/effect_size_adjustment.js');
    var effect_reflection = require('./VisualEffects/effect_reflection.js');
    
    var version = '1.6.4';
    var defaultOptions = {
        filter: "div",
        selectedIndex: 0,
        distance: 70,
        mode3D: 'z',
        scaleX: 1,
        scaleY: 1,
        scaleZ: 1,
        perspective: 1000,
        numberOfElementsToDisplayRight: null,
        numberOfElementsToDisplayLeft: null,
        sensitivity: 1,
        verticalRotation: false,
        minKeyDownFrequency: 0,
        rotationAnimationEasing: 'easeOutCubic',
        rotationAnimationDuration: 500,
        inertiaFriction: 10,
        inertiaHighFriction: 50,
        path: {
            type: "parabola",
            settings: {}
        },
        designedForWidth: null,
        designedForHeight: null,
        enabled: true,
        mousewheelEnabled: true,
        keyboardEnabled: true,
        gesturesEnabled: true,

        autorotation: false,
        autorotationDirection: 'right', /* left, right */
        autorotationPause: 0,

        // effects
        allignElementsWithPath: false,
        allignElementsWithPathCoefficient: 1,

        fadeAway: false,
        fadeAwayNumberOfConfigurableElements: 5,
        fadeAwayBezierPoints: { p1: { x: 0, y: 100 }, p2: { x: 50, y: 50 }, p3: { x: 50, y: 50 }, p4: { x: 100, y: 0 } },

        rotation: false,
        rotationVectorX: 0,
        rotationVectorY: 0,
        rotationVectorZ: 0,
        rotationNumberOfConfigurableElements: 5,
        rotationBezierPoints: { p1: { x: 0, y: 0 }, p2: { x: 50, y: 0 }, p3: { x: 50, y: 0 }, p4: { x: 100, y: 0 } },
        rotationInvertForNegative: false,

        sizeAdjustment: false,
        sizeAdjustmentNumberOfConfigurableElements: 5,
        sizeAdjustmentBezierPoints: { p1: { x: 0, y: 100 }, p2: { x: 50, y: 100 }, p3: { x: 50, y: 100 }, p4: { x: 100, y: 100 } },

        shadow: false,
        shadowBlurRadius: 100,
        shadowSpreadRadius: 0,

        popoutSelected: false,
        popoutSelectedShiftX: 0,
        popoutSelectedShiftY: 0,
        popoutSelectedShiftZ: 0,

        reflection: false,
        reflectionBelow: 0,
        reflectionHeight: 0.3,

        fallback: 'auto', // auto, always, never
        distanceInFallbackMode: 200
    };

    $.theta.carousel = function (domElement, options) {

        var carousel = this;
        carousel.$element = $(domElement);
        carousel.$element.data("theta.carousel", carousel);
        carousel.$element.addClass('theta-carousel');

        carousel._create = function () {
            this.options = $.extend(true, {}, $.theta.carousel.defaultOptions, options);

            // prepare container
            var containerSize = new size(this.widget().width(), this.widget().height());

            this.container = $('<div class="theta-carousel-inner-container"></div>');
            this.container.appendTo(this.widget());
            this.container.css({
                width: containerSize.width + 'px',
                height: containerSize.height + 'px'
            });

            this.widget().attr('tabindex', 0).css({ outline: 'none', overflow: 'hidden' });
            this.container.css({
                transformStyle: 'preserve-3d',
                overflow: 'hidden',
                perspective: this.options.perspective + 'px',
                transform: 'translate3d(0px,0px, 100000px)'
            });

            // init elements
            this.update();

            // prepare widget
            this.effects = [];
            this._createPath();
            this._createEffects();
            this._createRotationLogicController();
            this.elementsSizeUpdater = new elements_size_updater(this);
            this.fluidLayout = new fluid_layout(this);
            this._alignElements();
            this.animation = new comulative_animation(this);
            this.motionController = new motion_controller(this, $.proxy(this._motionConsumer, this));
            this.inputController = new input_controller(this);
            this.autoRotator = new auto_rotator(this);

            // attach event listeners
            $(this.animation).on('step', $.proxy(function (e, shift) { this._alignElements(shift); }, this));
            $(this.animation).on('done', $.proxy(function (e, value) {
                this._rotationAnimationCompleted(value);
                this._raiseMotionEnd();
            }, this));
            $(this.motionController).on('end', $.proxy(function (e, value) { this._motionEnd(value); }, this));
            $(this.motionController).on('start', $.proxy(this._raiseMotionStart, this));

            this.initialized = true;
        };

        carousel.destroy = function () {

            for (var i = 0; i < this.effects.length; i++) {
                this.effects[i].revert();
            }

            if (this.rotationLogicController != null)
                this.rotationLogicController.destroy();
            this.inputController.destroy();
            this.fluidLayout.destroy();
            this.elementsSizeUpdater.destroy();
            this.autoRotator.destroy();
            for (var i = 0; i < this.elements.length; i++) {
                this.elements[i].$element.off('tap', this.moveTo);
                this.elements[i].$element.off("click", this.moveTo);
            }
            this.widget().data('theta.carousel', null);
        };

        carousel.moveBack = function () {
            return this.rotationLogicController.moveBack();
        };

        carousel.moveForward = function () {
            return this.rotationLogicController.moveForward();
        };

        carousel.moveTo = function (index) {
            this.rotationLogicController.moveTo(index);
        };

        carousel.invalidate = function () {
            if (!this._isInMotion)
                this._alignElements();
        };

        carousel.update = function () {

            var itemsToAdd = this.widget().contents().filter(
                function() {
                    return (
                        this.nodeType == 8 /*we need comments for AngularJS*/
                        ||
                        !$(this).hasClass('theta-ignore')
                    ) && !$(this).hasClass('theta-carousel-inner-container'); // ignore internal container
                });

            itemsToAdd.appendTo(this.container);

            this.elements = this.container.children().filter(this.options.filter).map(function (i, e) {
                var $e = $(e);

                var order = $e.data('theta-order');

                if (!order)
                    order = i;
                
                return { $element: $e, element: e, order: order };
            }).toArray();

            this.elements.sort(function (e1, e2) { return e1.order - e2.order; });

            for (var i = 0; i < this.elements.length; i++) {

                this.elements[i].index = i;
                this.elements[i].element.index = i;

                if (!this.elements[i].$element.hasClass('theta-carousel-element')) {

                    this.elements[i].$element.addClass('theta-carousel-element');
                    this.elements[i].$element.css({ position: 'absolute' });
                    this.elements[i].$element.on('tap', $.proxy(this.moveTo, this, i));
                    this.elements[i].$element.click($.proxy(function (e) {
                        if (this.options.enabled && !this.options.autorotation) {
                            this.moveTo(e.index);
                        }
                    }, this, this.elements[i].element));

                }
            }

            this.options.selectedIndex = Math.max(Math.min(this.options.selectedIndex, this.elements.length - 1), 0);

            if (this.elements.length == 0)
                this.options.selectedIndex = -1;

            if (this.initialized) {
                this.elementsSizeUpdater.update();
                this.invalidate();
            }

            this._applyCurrentItemStyle();
        };

        carousel.getIsInMotion = function () {
            return this._isInMotion;
        };

        carousel.getIsFallbackMode = function () {
            return this.rotationLogicController.isFallbackMode();
        };

        carousel.widget = function () {
            return this.$element;
        };

        carousel._setOption = function (name, value) {
            
            utils.setObjectPropertyValue(carousel.options, name, value);

            if (name === 'rotationAnimationDuration' || name === 'rotationAnimationEasing') {
                // don't need to do something if these properties has been changed
                return;
            }

            if (name === 'filter') {
                this.update();
            }

            if (name === 'perspective') {
                this.container.css({ perspective: value + 'px' });
                if (this.options.mode3D == 'scale')
                    this._alignElements();
            }

            if (name.indexOf('path') == 0 || name === 'fallback') {
                this._createPath();
                this._createRotationLogicController();
                this._alignElements();
            }

            if (name === "selectedIndex" || name === "distance" || name === "mode3D"
                || name === "numberOfElementsToDisplayRight" || name === "numberOfElementsToDisplayLeft"
                || name === "scaleX" || name === "scaleY" || name === "scaleZ"
                || name === "allignElementsWithPathCoefficient"
                || name === "fadeAwayBezierPoints" || name === "fadeAwayNumberOfConfigurableElements"
                || name === "rotationBezierPoints" || name === "rotationNumberOfConfigurableElements" || name === "rotationInvertForNegative"
                || name === "rotationVectorX" || name === "rotationVectorY" || name === "rotationVectorZ"
                || name === "sizeAdjustmentNumberOfConfigurableElements" || name === "sizeAdjustmentBezierPoints"
                || name === "shadowBlurRadius" || name === "shadowSpreadRadius"
                || name === "popoutSelectedShiftX" || name === "popoutSelectedShiftY" || name === "popoutSelectedShiftZ"
                || name === "distanceInFallbackMode"
                ) {
                this._alignElements();
            }

            if (name.indexOf('autorotation') != -1) {
                this.autoRotator.applySettings();
            }

            if (name.indexOf('allignElementsWithPath') != -1 || name.indexOf('fadeAway') != -1 || name.indexOf('rotation') != -1
                || name.indexOf('sizeAdjustment') != -1 || name.indexOf('shadow') != -1 || name.indexOf('popoutSelected') != -1
                || name.indexOf('reflection') != -1) {
                this._createEffects();
                this._alignElements();
            }

            if (name === 'selectedIndex') {
                this._applyCurrentItemStyle();
            }
        };

        carousel._createRotationLogicController = function()
        {
            if (this.rotationLogicController != null)
                this.rotationLogicController.destroy();

            if (this.path.isEndless())
                this.rotationLogicController = new endless_rotation_logic_controller(this);
            else
                this.rotationLogicController = new rotation_logic_controller(this);

            if (this.options.fallback == 'always' || (this.options.fallback == 'auto' && fallback_rotation_logic_controller.fallback())) {
                this.rotationLogicController = new fallback_rotation_logic_controller(this, this.rotationLogicController);
            }

            if (this.autoRotator) {
                this.autoRotator.applySettings();
            }
        };

        carousel._createEffects = function () {

            for (var i = 0; i < this.effects.length; i++) {
                this.effects[i].revert();
            }

            this.effects = [];

            if (this.options.allignElementsWithPath)
                this.effects.push(new effect_allign_to_path(this, {}));

            if (this.options.fadeAway)
                this.effects.push(new effect_fade_away(this, {}));

            if (this.options.rotation)
                this.effects.push(new effect_rotation(this, {}));

            if (this.options.sizeAdjustment)
                this.effects.push(new effect_size_adjustment(this, {}));

            if (this.options.shadow)
                this.effects.push(new effect_shadow(this, {}));

            if (this.options.popoutSelected)
                this.effects.push(new effect_pop_out_selected(this, {}));

            if (this.options.reflection)
                this.effects.push(new effect_reflection(this, {}));
        };

        carousel._createPath = function () {
            var newPath = null;

            if (this.options.path.type == "parabola") {
                newPath = new path_parabola(this, this.options.path.settings);
            }

            if (this.options.path.type == "line") {
                newPath = new path_line(this, this.options.path.settings);
            }

            if (this.options.path.type == "cubic") {
                newPath = new path_cubic(this, this.options.path.settings);
            }

            if (this.options.path.type == "archimedes_spiral") {
                newPath = new path_archimedes_spiral(this, this.options.path.settings);
            }

            if (this.options.path.type == "ellipse") {
                newPath = new path_ellipse(this, this.options.path.settings);
            }

            if (this.options.path.type == "cubic_bezier") {
                newPath = new path_cubic_bezier(this, this.options.path.settings);
            }

            if (newPath != null) {
                this.path = newPath;
                this.options.path.settings = this.path.settings;
            } else
                throw "path " + this.options.path.type + " is not supported.";
        };

        carousel._raiseChangeEvent = function () {
            this.widget().trigger("change", { index: this.options.selectedIndex });
            this._applyCurrentItemStyle();
        };

        carousel._applyCurrentItemStyle = function () {
            for (var i = 0; i < this.elements.length; i++) {
                if (i === this.options.selectedIndex) {
                    this.elements[i].$element.addClass('theta-current-item');
                }
                else {
                    this.elements[i].$element.removeClass('theta-current-item');
                }
            }
        };

        carousel._raiseMotionStart = function () {
            this._isInMotion = true;
            this.widget().addClass('theta-in-motion');
            this.widget().trigger("motionStart", { index: this.options.selectedIndex });
        };

        carousel._raiseMotionEnd = function () {
            this.inputController.nonInterruptibleMode(false);
            this.widget().removeClass('theta-in-motion');
            this._isInMotion = false;
            this.widget().trigger("motionEnd", { index: this.options.selectedIndex });
        };

        carousel._rotationAnimationCompleted = function (index) {
            if (this.options.selectedIndex != index) {
                this.options.selectedIndex = index;
                this._raiseChangeEvent();
            }
            this._alignElements();
        };

        carousel._motionConsumer = function (distance) {
            return this.rotationLogicController.consumeMotion(distance);
        };

        carousel._motionEnd = function (remainingDistance) {
            this.rotationLogicController.handleMotionEnd(remainingDistance);
        };

        carousel._alignElements = function (animationShift) {
            return this.rotationLogicController.alignElements(animationShift);
        };

        carousel._getContainerSize = function () {
            var container = $('.theta-carousel-inner-container', this.widget());
            return new size(container.width(), container.height());
        };

        carousel._create();
    };

    $.theta.carousel.defaultOptions = defaultOptions;
    $.theta.carousel.version = version;

    $.fn.theta_carousel = function (options) {
        var callArguments = arguments;

        var hasCallRes = false;
        var callRes = null;

        var eachRes = this.each(function () {
            var $el = $(this);
            var instance = $el.data('theta.carousel');

            if (instance) {
                if (typeof options === 'string') {

                    if (typeof instance[options] === 'function') {
                        var args = Array.prototype.slice.call(callArguments, 1);
                        hasCallRes = true;
                        callRes = instance[options].apply(instance, args);
                    }

                    if (options == 'option') {
                        if (callArguments.length == 2) {
                            hasCallRes = true;
                            callRes = utils.getObjectPropertyValue(instance.options, callArguments[1]);
                        }

                        if (callArguments.length == 3) {
                            instance._setOption(callArguments[1], callArguments[2]);
                        }
                    }
                    
                } else {
                    var clone = $.extend(true, {}, options);
                    $.each(clone, $.proxy($el.data('theta.carousel')._setOption, $el.data('theta.carousel')));
                }
            }
            else 
                (new $.theta.carousel(this, options));
        });

        if (!hasCallRes)
            return eachRes;
        else
            return callRes;
    };

})(jQuery);
},{"./VisualEffects/effect_allign_to_path.js":1,"./VisualEffects/effect_fade_away.js":3,"./VisualEffects/effect_pop_out_selected.js":4,"./VisualEffects/effect_reflection.js":5,"./VisualEffects/effect_rotation.js":6,"./VisualEffects/effect_shadow.js":7,"./VisualEffects/effect_size_adjustment.js":8,"./auto_rotator.js":9,"./comulative_animation.js":13,"./easings.js":14,"./elements_size_updater.js":15,"./endless_rotation_logic_controller.js":16,"./fallback_rotation_logic_controller.js":17,"./fluid_layout.js":18,"./input_controller.js":20,"./motion_controller.js":21,"./paths/path_archimedes_spiral.js":22,"./paths/path_cubic.js":24,"./paths/path_cubic_bezier.js":25,"./paths/path_ellipse.js":26,"./paths/path_line.js":27,"./paths/path_parabola.js":28,"./rotation_logic_controller.js":31,"./size.js":32,"./utils.js":34}],13:[function(require,module,exports){
module.exports = function(carousel) {
    this.carousel = carousel;

    this.isInProgress = false;
    this.inProgressAnimationTarget = null;
    this.queue = [];
    this.distance = [];
    this.currentElement = null;

    this.clearQueue = function () {
        this.queue = [];
        this.distance = [];
    };

    this.animate = function (from, to, targetValue, easing, duration) {

        if (typeof (easing) == "undefined")
            easing = null;
        if (typeof (duration) == "undefined")
            duration = null;

        this.addDistance(Math.abs(to - from));

        if (this.queue.length > 5)
            return;

        this.queue.push({ from: from, to: to, targetValue: targetValue, easing: easing, duration: duration });

        if (!this.isInProgress) {
            this.peekFromQueue();
        }
    };

    this.completeCurrentImmediately = function () {
        if (this.currentElement != null) {
            this.currentElement.stop(true, true);
        }
    };

    this.peekFromQueue = function () {

        if (this.queue.length > 0) {
            var element = this.queue[0];
            this.queue = this.queue.slice(1);
            this.inProgressAnimationTarget = element.targetValue;
            this.currentElement = $(element);
            var stepDist = Math.abs(element.from - element.to);

            var easing = element.easing == null ? this.getEasing(stepDist) : element.easing;
            var duration = (element.duration == null ? this.carousel.options.rotationAnimationDuration : element.duration) * this.getDurationCoefficient(stepDist);

            this.currentElement.animate({ from: element.to }, {
                easing: easing,
                duration: duration,
                start: $.proxy(this.onStart, this),
                step: $.proxy(this.onStep, this),
                done: $.proxy(this.onDone, this),
                always: $.proxy(this.onAlways, this)
            });
        }
    };

    this.getTargetValue = function () {
        if (this.queue.length > 0)
            return this.queue[this.queue.length - 1].targetValue;
        return this.inProgressAnimationTarget;
    };

    this.onStart = function () {
        this.isInProgress = true;
    };

    this.onStep = function (val) {
        $(this).trigger('step', val);
    };

    this.onDone = function () {
        $(this).trigger('done', this.inProgressAnimationTarget);
    };

    this.onAlways = function () {
        this.isInProgress = false;
        this.peekFromQueue();
        this.currentElement = null;
    };

    this.addDistance = function (value) {
        this.distance.push({ date: new Date(), value: value });

        this.distance = $(this.distance).filter(function (i, d) {
            return (new Date() - d.date) < 5000;
        });
    };

    this.getActualDistance = function () {

        var distance = 0;
        var date = new Date();
        for (var i = 0; i < this.distance.length; i++) {

            var d = this.distance[i];

            if ((date - d.date) < this.carousel.options.rotationAnimationDuration)
                distance += d.value;
        }

        return distance;
    };

    // adjust rotation duration, if user quiqly press next button several times
    this.getDurationCoefficient = function (oneStepDist) {
        if (this.carousel.options.autorotation) // adjustment is not required for auto rotation
            return 1; // 

        if (this.getActualDistance() == 0)
            return 1;
        return 1 / (this.getActualDistance() / oneStepDist);
    };

    this.getEasing = function (oneStepDist) {
        if (this.getDurationCoefficient(oneStepDist) > 0.4)
            return this.carousel.options.rotationAnimationEasing;
        else
            return "linear";
    };
};
},{}],14:[function(require,module,exports){
module.exports = function () {

    // based on easing equations from Robert Penner (http://www.robertpenner.com/easing)

    var baseEasings = {};

    $.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function (i, name) {
        baseEasings[name] = function (p) {
            return Math.pow(p, i + 2);
        };
    });

    $.extend(baseEasings, {
        Sine: function (p) {
            return 1 - Math.cos(p * Math.PI / 2);
        },
        Circ: function (p) {
            return 1 - Math.sqrt(1 - p * p);
        },
        Elastic: function (p) {
            return p === 0 || p === 1 ? p :
                -Math.pow(2, 8 * (p - 1)) * Math.sin(((p - 1) * 80 - 7.5) * Math.PI / 15);
        },
        Back: function (p) {
            return p * p * (3 * p - 2);
        },
        Bounce: function (p) {
            var pow2,
                bounce = 4;

            while (p < ((pow2 = Math.pow(2, --bounce)) - 1) / 11) { }
            return 1 / Math.pow(4, 3 - bounce) - 7.5625 * Math.pow((pow2 * 3 - 2) / 22 - p, 2);
        }
    });

    var allEasings = {};
    $.each(baseEasings, function (name, easeIn) {
        allEasings["easeIn" + name] = easeIn;
        allEasings["easeOut" + name] = function (p) {
            return 1 - easeIn(1 - p);
        };
        allEasings["easeInOut" + name] = function (p) {
            return p < 0.5 ?
                easeIn(p * 2) / 2 :
                1 - easeIn(p * -2 + 2) / 2;
        };
    });

    $.each(allEasings, function (name, e) {
        if (typeof ($.easing[name]) == "undefined")
            $.easing[name] = e;
    });

};
},{}],15:[function(require,module,exports){
module.exports = function (carousel) {

    extend(this);

    this.carousel = carousel;
    this.interval = setInterval($.proxy(this.update, this), 500);

    this.update();
};

function extend(obj) {
    
    obj.update = function () {
        var invalidate = false;
        for (var i = 0; i < this.carousel.elements.length; i++) {
            var e = this.carousel.elements[i];
            
            var oldSize = null;
            if (e.size)
                oldSize = e.size;

            e.size = {
                height: e.$element.height(),
                width: e.$element.width()
            }

            if (!oldSize || oldSize.height != e.size.height || oldSize.width != e.size.width)
                invalidate = true;
        }

        if (invalidate)
            this.carousel.invalidate();
    };

    obj.destroy = function () {
        clearInterval(this.interval);
    };
}
},{}],16:[function(require,module,exports){
module.exports = function (carousel) {

    var rotation_logic_controller = require('./rotation_logic_controller.js');
    this.base = new rotation_logic_controller(carousel);
    $.extend(this, this.base);

    extend(this);
};

function extend(obj) {

    obj.moveToInternal = function (index) {
        var straight = this.carousel.options.selectedIndex - index;
        var reverse = 0;
        var reversePreferable = false;
        if (index > this.carousel.options.selectedIndex) {
            reverse = this.carousel.elements.length - index + this.carousel.options.selectedIndex;
        }
        else {
            reverse = (this.carousel.elements.length - this.carousel.options.selectedIndex + index) * -1;
            reversePreferable = true;
        }

        var distance = straight * this.getActualDistance();
        if (Math.abs(reverse) < Math.abs(straight) || (reversePreferable && Math.abs(reverse) == Math.abs(straight)))
            distance = reverse * this.getActualDistance();
        
        this.carousel.inputController.nonInterruptibleMode(true);
        this.carousel._raiseMotionStart();

        this.carousel.animation.animate(0, distance, index, Math.abs(reverse) === 1 ? null : "linear");
    };

    obj.moveBack = function () {
        var pendingTarget = this.carousel.animation.isInProgress ? this.carousel.animation.getTargetValue() : this.carousel.options.selectedIndex;

        pendingTarget--;

        if (pendingTarget < 0)
            pendingTarget = this.carousel.elements.length + pendingTarget;

        this.carousel.inputController.nonInterruptibleMode(true);
        this.carousel._raiseMotionStart();
        this.carousel.animation.animate(0, this.getActualDistance(), pendingTarget, null);
        return true;
    };

    obj.moveForward = function () {
        var pendingTarget = this.carousel.animation.isInProgress ? this.carousel.animation.getTargetValue() : this.carousel.options.selectedIndex;

        pendingTarget++;
        if (pendingTarget >= this.carousel.elements.length)
            pendingTarget -= this.carousel.elements.length;

        this.carousel.inputController.nonInterruptibleMode(true);
        this.carousel._raiseMotionStart();
        this.carousel.animation.animate(0, -1 * this.getActualDistance(), pendingTarget, null);
        return true;
    };

    obj.consumeMotion = function (distance) {
        var highFrictionRange = this.carousel._alignElements(distance);

        var scrolledElements = parseInt(distance / this.getActualDistance(), 10);

        var prevIndex = this.carousel.options.selectedIndex;
        this.carousel.options.selectedIndex -= scrolledElements;

        var consumedDistance = prevIndex - this.carousel.options.selectedIndex;
        if (this.carousel.options.selectedIndex < 0) {
            this.carousel.options.selectedIndex = this.carousel.options.selectedIndex % this.carousel.elements.length + this.carousel.elements.length;
            consumedDistance = this.carousel.elements.length - this.carousel.options.selectedIndex + prevIndex;
        }
        if (this.carousel.options.selectedIndex >= this.carousel.elements.length) {
            this.carousel.options.selectedIndex = this.carousel.options.selectedIndex % this.carousel.elements.length;
            consumedDistance = this.carousel.elements.length - prevIndex + this.carousel.options.selectedIndex;
            consumedDistance *= -1;
        }
        
        if (prevIndex != this.carousel.options.selectedIndex)
            this.carousel._raiseChangeEvent();

        return { distance: consumedDistance * this.getActualDistance(), highFrictionRange: highFrictionRange };
    };

    obj.handleMotionEnd = function (remainingDistance) {
        if (remainingDistance == 0)
            return;

        var targetIndex = this.carousel.options.selectedIndex;

        if (Math.abs(remainingDistance) > this.getActualDistance() / 2) {
            if (remainingDistance < 0)
                targetIndex++;
            else
                targetIndex--;
        }

        var reverse = false;
        if (this.carousel.elements.length == 0)
            targetIndex = 0;
        else {
            if (targetIndex < 0) {
                targetIndex = targetIndex % this.carousel.elements.length + this.carousel.elements.length;
                reverse = 'back';
            }
            if (targetIndex >= this.carousel.elements.length) {
                targetIndex = targetIndex % this.carousel.elements.length;
                reverse = 'forward';
            }
        }

        var targetDistance = 0;
        if (!reverse) {
            targetDistance = (this.carousel.options.selectedIndex - targetIndex) * this.getActualDistance();
        }
        else {
            if(reverse === 'back')
                targetDistance = (this.carousel.elements.length - targetIndex + this.carousel.options.selectedIndex) * this.getActualDistance();
            else
                targetDistance = (this.carousel.elements.length - this.carousel.options.selectedIndex + targetIndex) * this.getActualDistance() * -1;
        }
        
        var duration = Math.abs(this.carousel.options.rotationAnimationDuration * (remainingDistance / this.getActualDistance()));
        duration = Math.min(duration, this.carousel.options.rotationAnimationDuration / 2);

        this.carousel.animation.animate(remainingDistance, targetDistance, targetIndex, null, duration);
    };

    obj.alignElements = function (animationShift) {

        if (this.carousel.elements.length == 0 || this.carousel.options.selectedIndex < 0)
            return false;

        this.carousel.containerSize = this.carousel._getContainerSize();

        var shift = 0;
        if (typeof (animationShift) != "undefined")
            shift = animationShift;

        var location = this.getRootValue();
        var ranges = this.getFadeRanges(location);

        for (var i = 0; i < this.carousel.elements.length; i++) {
            this.carousel.elements[i].isEndlessProcessed = false;
        }

        var visibilityInfo = [];

        for (var i = new endlessIterator(this.carousel, this.carousel.options.selectedIndex) ; !i.isCycleCompleted(); i.moveNext()) {
            var visible = this.setElementPosition(this.carousel.elements[i.getCurrentIndex()], location + shift, ranges);
            if (visible)
                this.carousel.elements[i.getCurrentIndex()].isEndlessProcessed = true;
            visibilityInfo[i.getCurrentIndex()] = visible;
            location = this.incrementValue(location, this.getActualDistance());
        }

        location = this.getRootValue();

        for (var i = new endlessIterator(this.carousel, this.carousel.options.selectedIndex - 1) ; !i.isCycleCompleted() ; i.movePrev()) {
            if (this.carousel.elements[i.getCurrentIndex()].isEndlessProcessed)
                break;

            location = this.decrementValue(location, this.getActualDistance());
            visibilityInfo[i.getCurrentIndex()] = this.setElementPosition(this.carousel.elements[i.getCurrentIndex()], location + shift, ranges);
        }

        for (var i = 0; i < this.carousel.elements.length; i++) {
            this.base.setElementVisibilityInternal(!visibilityInfo[i], this.carousel.elements[i]);
        }

        this.setZIndexes();

        return false;
    };

    obj.setElementVisibilityInternal = function (hide) { };
}

var endlessIterator = function(carousel, currentIndex) {
    this.currentIndex = currentIndex;
    if (this.currentIndex == -1)
        this.currentIndex = carousel.elements.length - 1;
    this.iterations = 0;
    this.carousel = carousel;
    
    this.moveNext = function () {
        this.currentIndex++;
        if (this.currentIndex == this.carousel.elements.length)
            this.currentIndex = 0;
        this.iterations++;
    };

    this.movePrev = function () {
        this.currentIndex--;
        if (this.currentIndex == -1)
            this.currentIndex = this.carousel.elements.length - 1;
        this.iterations--;
    };

    this.isCycleCompleted = function () {
        return Math.abs(this.iterations) >= this.carousel.elements.length;
    };

    this.getCurrentIndex = function () {
        return this.currentIndex;
    };
}
},{"./rotation_logic_controller.js":31}],17:[function(require,module,exports){
var transform = require('./transform.js');

module.exports = function (carousel, baseRotator) {
    this.base = baseRotator;
    $.extend(this, this.base);
    extend(this);
};

module.exports.fallback = function(){
    return !has3d();
}

function extend(obj) {

    obj.isFallbackMode = function () { return true; };

    obj.getActualDistance = function () {
        return this.carousel.options.distanceInFallbackMode;
    };

    obj.getRootValue = function() {
        return 0;
    };

    obj.incrementValue = function (value, increment) {
        return value + increment;
    };

    obj.decrementValue = function (value, decrement) {
        return value - decrement;
    };

    obj.minValue = function () {
        return -1 * this.maxValue();
    };

    obj.maxValue = function () {
        return  ( this.carousel.containerSize.width + this.getActualDistance() ) / 2;
    };

    obj.setElementPosition = function(element, value) {

        var left = value - element.size.width / 2 + this.carousel.containerSize.width / 2;
        var top = this.carousel.containerSize.height / 2 - element.size.height / 2;

        element.$element.css({
            transform: '',
            left: left + 'px',
            top: top + 'px'
        });

        var visible = value > this.minValue() && value < this.maxValue()

        if (visible)
            element.$element.show();
        else
            element.$element.hide();

        return visible;
    };

    obj.destroy = function () {

        this.base.destroy();

        for (var i = 0; i < this.carousel.elements.length; i++) {
            this.carousel.elements[i].$element.css({
                left: '0',
                top: '0'
            });
        }
    };
};

function has3d() {
    if (document.body && document.body.style.perspective !== undefined) {
        return true;
    }
    var _tempDiv = document.createElement("div"),
        style = _tempDiv.style,
        a = ["Webkit", "Moz", "O", "Ms", "ms"],
        i = a.length;
    if (_tempDiv.style.perspective !== undefined) {
        return true;
    }
    while (--i > -1) {
        if (style[a[i] + "Perspective"] !== undefined) {
            return true;
        }
    }
    return false;
}


},{"./transform.js":33}],18:[function(require,module,exports){
module.exports = function(carousel) {

    extend(this);

    this.carousel = carousel;
    this.interval = setInterval($.proxy(this.update, this), 200);

    this.update();

    this.appliedWidth = null;
    this.appliedHeight = null;
    this.appliedScale = null;
};

function extend(obj) {
    obj.update = function () {

        var widthToApply = this.carousel.widget().width();
        var heightToApply = this.carousel.widget().height();
        var scaleToApply = null;

        if (widthToApply == 0 || heightToApply == 0)
            return;

        if (this.carousel.options.designedForWidth != null && this.carousel.options.designedForHeight != null) {
            scaleToApply = Math.min(widthToApply / this.carousel.options.designedForWidth, heightToApply / this.carousel.options.designedForHeight);
        }

        if (widthToApply != this.appliedWidth || heightToApply != this.appliedHeight || scaleToApply != this.appliedScale) {
            var container = $('.theta-carousel-inner-container', this.carousel.widget());

            if (scaleToApply != null) {
                widthToApply = widthToApply / scaleToApply;
                heightToApply = heightToApply / scaleToApply;

                // keep proportion
                if (widthToApply / heightToApply < this.carousel.options.designedForWidth / this.carousel.options.designedForHeight) {
                    heightToApply = widthToApply * (this.carousel.options.designedForHeight / this.carousel.options.designedForWidth);
                } else {
                    widthToApply = heightToApply * (this.carousel.options.designedForWidth / this.carousel.options.designedForHeight);
                }

                // reposition
                container.css({
                    left: this.carousel.widget().width() / 2 - widthToApply / 2,
                    top: this.carousel.widget().height() / 2 - heightToApply / 2,
                });

            }

            // applaying
            if (widthToApply != this.appliedWidth) {
                container.width(widthToApply);
                this.carousel.invalidate();
                this.appliedWidth = widthToApply;
            }

            if (heightToApply != this.appliedHeight) {
                container.height(heightToApply);
                this.carousel.invalidate();
                this.appliedHeight = heightToApply;
            }

            if (scaleToApply != this.appliedScale) {
                if (scaleToApply == null) {
                    container.css({
                        transform: 'translate3d(0px,0px, 100000px)',
                        position: 'static'
                    });
                } else {
                    container.css({
                        transform: 'translate3d(0px,0px, 100000px) scale(' + scaleToApply + ')',
                        position: 'relative'
                    });
                }
                this.carousel.invalidate();

                this.appliedScale = scaleToApply;
            }
        }
    };

    obj.getAppliedScale = function () {
        if (this.appliedScale)
            return this.appliedScale;
        return 1;
    };

    obj.destroy = function () {
        clearInterval(this.interval);
    };
}
},{}],19:[function(require,module,exports){
module.exports = function(carousel) {
    this.carousel = carousel;
    this.movements = [];
    this.passedBrakingDistance = 0;
    this.isInProgress = false;
    this.lowFrictionInProgress = false;
    this.switchingToHighFriction = false;

    this.registerMovement = function (distance) {
        this.movements.push({ date: new Date(), distance: distance });
        this.clearOldMovements();
    };

    this.stop = function () {
        this.switchingToHighFriction = false;
        $(this).stop(false, false);
    };

    this.movedIntoHighFrictionRange = function () {
        if (this.lowFrictionInProgress) {
            this.lowFrictionInProgress = false;
            this.switchingToHighFriction = true;
            $(this).stop(false, false);

            var brakingDistLeft = this.motionData.fromPosition + this.motionData.brakingDistance - this.passedBrakingDistance;
            var speedLeft = this.motionData.initialSpeed * Math.abs(brakingDistLeft / this.motionData.brakingDistance);

            var friction = this.carousel.options.inertiaHighFriction * 100;
            var brakingDistance = (speedLeft * speedLeft) / (2 * friction);
            if (speedLeft < 0)
                brakingDistance *= -1;

            var brakingTime = brakingDistance / (speedLeft / 2);

            $(this).animate({ passedBrakingDistance: this.passedBrakingDistance + brakingDistance }, {
                easing: 'easeOutQuad',
                duration: brakingTime * 1000,
                step: $.proxy(this.onStep, this),
                complete: $.proxy(this.onComplete, this),
                fail: $.proxy(this.onFail, this)
            });
        }
    };

    this.run = function (fromPosition) {

        var speed = this.getSpeed();
        this.movements = [];

        if (speed == 0) {
            $(this).trigger('complete');
            return;
        }

        var friction = this.carousel.options.inertiaFriction * 100;
        var brakingDistance = (speed * speed) / (2 * friction);
        if (speed < 0)
            brakingDistance *= -1;

        var brakingTime = brakingDistance / (speed / 2);

        this.passedBrakingDistance = fromPosition;

        this.isInProgress = true;
        this.lowFrictionInProgress = true;
        this.motionData = {
            initialSpeed: speed,
            brakingDistance: brakingDistance,
            fromPosition: fromPosition
        };

        $(this).animate({ passedBrakingDistance: fromPosition + brakingDistance }, {
            easing: 'easeOutCirc',
            duration: brakingTime * 1000,
            step: $.proxy(this.onStep, this),
            complete: $.proxy(this.onComplete, this),
            fail: $.proxy(this.onFail, this)
        });
    };

    this.onStep = function (val) {
        if (isNaN(val))
            return; //for some easings we can get NaNs

        this.passedBrakingDistance = val;
        $(this).trigger('step', val);
    };

    this.onComplete = function () {
        this.isInProgress = false;
        $(this).trigger('complete');
    };

    this.onFail = function () {
        if (!this.switchingToHighFriction) {
            this.isInProgress = false;
            $(this).trigger('stop');
        }
    };

    this.clearOldMovements = function () {
        this.movements = $(this.movements).filter(function (i, d) {
            return (new Date() - d.date) < 5000;
        });
    };

    this.getSpeed = function () {

        var distance = 0;
        var date = new Date();
        for (var i = 0; i < this.movements.length; i++) {

            var d = this.movements[i];

            if ((date - d.date) < 200)
                distance += d.distance;
        }

        return distance * 5;
    };
}
},{}],20:[function(require,module,exports){
module.exports = function (carousel) {
    extend(this);

    this.carousel = carousel;
    this.lastKeyDownTime = new Date();
    this.lastProcessedEvent = null;
    this.nonInterruptible = false;
    this.destroyables = [];

    this.carousel.widget().keyup(this.destroyable("keyup", $.proxy(this.onKeyUp, this)));
    this.carousel.widget().keydown(this.destroyable("keydown", $.proxy(this.onKeyDown, this)));

    this.carousel.widget().mousedown(this.destroyable("mousedown", $.proxy(function (e) {
        if (this.isReadonly() || !this.getGesturesEnabled())
            return;

        this.carousel.widget().focus();
        this.motionStarted(this.isVerticalRotation() ? e.pageY : e.pageX);
        e.preventDefault();
    }, this)));
    this.carousel.widget().mousemove(this.destroyable("mousemove", $.proxy(function (e) {
        if (this.isReadonly() || !this.getGesturesEnabled())
            return;

        if (this.canProcessEvent()) {
            this.registerEventAsProcessed();
            this.motionContinued(this.isVerticalRotation() ? e.pageY : e.pageX);
        }
        e.preventDefault();
    }, this)));
    this.carousel.widget().mouseleave(this.destroyable("mouseleave", $.proxy(function (e) {
        if (this.isReadonly() || !this.getGesturesEnabled())
            return;

        if (this.nonInterruptible)
            return;
        this.carousel.motionController.motionEnded(true);
        e.preventDefault();
    }, this)));
    this.carousel.widget().mouseup(this.destroyable("mouseup", $.proxy(function (e) {
        if (this.isReadonly() || !this.getGesturesEnabled())
            return;

        this.carousel.motionController.motionEnded(true);
        e.preventDefault();
    }, this)));

    if (typeof (this.carousel.widget().get(0).onmousewheel) != "undefined") {
        this.carousel.widget().on("mousewheel", this.destroyable("mousewheel", $.proxy(this.onMousewheel, this)));
    } else {
        this.carousel.widget().on("DOMMouseScroll", this.destroyable("DOMMouseScroll", $.proxy(this.onMousewheel, this)));
    }

    this.carousel.widget().on('touchstart', this.destroyable("touchstart", $.proxy(function (e) {
        if (this.isReadonly() || !this.getGesturesEnabled())
            return;

        this.motionStarted(this.isVerticalRotation() ? e.originalEvent.touches[0].screenY : e.originalEvent.touches[0].screenX);
    }, this)));
    this.carousel.widget().on('touchmove', this.destroyable("touchmove", $.proxy(function (e) {
        if (this.isReadonly() || !this.getGesturesEnabled())
            return;

        if (this.canProcessEvent()) {
            this.registerEventAsProcessed();
            this.motionContinued(this.isVerticalRotation() ? e.originalEvent.touches[0].screenY : e.originalEvent.touches[0].screenX);
        }
        e.preventDefault();
    }, this)));
    this.carousel.widget().on('touchend', this.destroyable("touchend", $.proxy(function (e) {
        if (this.isReadonly() || !this.getGesturesEnabled())
            return;

        this.carousel.motionController.motionEnded(true);
    }, this)));
    this.carousel.widget().on('touchcancel', this.destroyable("touchcancel", $.proxy(function (e) {
        if (this.isReadonly() || !this.getGesturesEnabled())
            return;

        this.carousel.motionController.motionEnded(true);
    }, this)));
    this.carousel.widget().on('taphold', this.destroyable("taphold", function (e) { e.preventDefault(); }));
};

function extend(obj) {

    obj.isVerticalRotation = function() {
        return this.carousel.options.verticalRotation && !this.carousel.rotationLogicController.isFallbackMode();
    };

    obj.getSensitivity = function () {

        if (this.carousel.rotationLogicController.isFallbackMode())
            return 1;
        return this.carousel.options.sensitivity / this.carousel.fluidLayout.getAppliedScale();
    };

    obj.isReadonly = function() {
        return !this.carousel.options.enabled ||
            this.carousel.options.autorotation;
    };

    obj.getGesturesEnabled = function () {
        return this.carousel.options.gesturesEnabled;
    };

    obj.nonInterruptibleMode = function (nonInterruptible) {
        this.nonInterruptible = nonInterruptible;
    };

    obj.canProcessEvent = function () {
        if (this.lastProcessedEvent == null)
            return true;

        if (new Date() - this.lastProcessedEvent > 50)
            return true;

        return false;
    };

    obj.registerEventAsProcessed = function () {
        this.lastProcessedEvent = new Date();
    };

    obj.motionStarted = function (x) {
        this.carousel.motionController.motionStarted(x);
        this.initialX = x;
    };

    obj.motionContinued = function (x) {
        var delta = this.initialX - x;
        delta *= this.getSensitivity();
        x = this.initialX - delta;

        this.carousel.motionController.motionContinued(x);
    };

    obj.onMousewheel = function (event) {
        if (!this.carousel.options.mousewheelEnabled)
            return;

        if (this.isReadonly())
            return;

        event.preventDefault();

        if (this.carousel.getIsInMotion())
            return;

        var up = false;
        var down = false;
        var original = event.originalEvent;

        
        // jquery mousewheel plugin
        if (event.deltaY) {
            if (event.deltaY == 1)
                up = true;
            else
                if (event.deltaY == -1)
                    down = true;
        }

        if (original.wheelDelta) {
            if (original.wheelDelta >= 120) {
                up = true;
            }
            else {
                if (original.wheelDelta <= -120) {
                    down = true;
                }
            }
        }

        if (original.detail) {
            if (original.detail == -3)
                up = true;
            else
                if (original.detail == 3)
                    down = true;
        }
        
        if (up) {
            if (this.getSensitivity() > 0)
                this.carousel.moveBack();
            if (this.getSensitivity() < 0)
                this.carousel.moveForward();
        }
        if (down) {
            if (this.getSensitivity() < 0)
                this.carousel.moveBack();
            if (this.getSensitivity() > 0)
                this.carousel.moveForward();
        }
    };

    obj.onKeyDown = function (event) {
        if (!this.carousel.options.keyboardEnabled)
            return;

        if (this.isReadonly())
            return;

        if (event.which == this.getBackKey() || event.which == this.getForwardKey())
            event.preventDefault();

        if (this.carousel.motionController.isInMotion || this.carousel.motionController.inertia.isInProgress)
            return;

        if ((new Date() - this.lastKeyDownTime) < this.carousel.options.minKeyDownFrequency)
            return;

        this.lastKeyDownTime = new Date();

        if (event.which == this.getBackKey()) {
            if (this.getSensitivity() > 0)
                this.carousel.moveBack();
            if (this.getSensitivity() < 0)
                this.carousel.moveForward();
        }
        if (event.which == this.getForwardKey()) {
            if (this.getSensitivity() < 0)
                this.carousel.moveBack();
            if (this.getSensitivity() > 0)
                this.carousel.moveForward();
        }
    };

    obj.onKeyUp = function (event) {
        if (this.isReadonly())
            return;

        if (this.carousel.motionController.isInMotion || this.carousel.motionController.inertia.isInProgress)
            return;

        if (event.which == this.getBackKey() || event.which == this.getForwardKey()) {
            this.carousel.animation.clearQueue();
        }
    };

    obj.destroyable = function (key, func) {
        this.destroyables[key] = func;
        return func;
    };

    obj.destroy = function () {
        for (var key in this.destroyables) {
            this.carousel.widget().off(key, this.destroyables[key]);
        }
    };

    obj.getForwardKey = function() {
        if (this.carousel.options.verticalRotation)
            return 40; //down
        return 37; //left
    };

    obj.getBackKey = function () {
        if (this.carousel.options.verticalRotation)
            return 38; //up
        return 39; //right
    };
}
},{}],21:[function(require,module,exports){
module.exports = function (carousel, motionConsumer) {
    var inertia = require('./inertia.js');
    
    extend(this);

    this.isInMotion = false;
    this.lastPosition = null;
    this.distance = 0;
    this.motionConsumer = motionConsumer;
    this.inertia = new inertia(carousel);
    $(this.inertia).on('complete', $.proxy(this.motionEnded, this, false));
    $(this.inertia).on('stop', $.proxy(this.inertiaStop, this));
    $(this.inertia).on('step', $.proxy(function (e, value) { this.motionContinuedInternal(value); }, this));
};

function extend(obj) {
    obj.motionInProgress = function () {
        return this.inertia.isInProgress || this.isInMotion;
    };

    obj.motionStarted = function (position) {

        if (!this.inertia.isInProgress)
            this.distance = 0;

        this.inertia.stop();
        this.isInMotion = true;
        this.lastPosition = position;
        this.raiseStart = true;
    };

    obj.motionContinued = function (position) {

        if (!this.isInMotion)
            return;

        if (this.raiseStart && position != this.lastPosition)
        {
            $(this).trigger('start');
            this.raiseStart = false;
        }

        this.inertia.registerMovement(position - this.lastPosition);
        this.motionContinuedInternal(position);
    };

    obj.motionContinuedInternal = function (position) {

        this.distance += position - this.lastPosition;
        this.lastPosition = position;
        var consumingData = this.motionConsumer(this.distance);
        this.distance -= consumingData.distance;

        if (consumingData.highFrictionRange && this.inertia.isInProgress) {
            this.inertia.movedIntoHighFrictionRange();
        }
    };

    obj.motionEnded = function (useInertia) {

        if (this.inertia.isInProgress)
            return;

        this.isInMotion = false;

        if (!useInertia) {
            $(this).trigger('end', this.distance);
            this.distance = 0;
        } else {
            this.inertia.run(this.lastPosition);
        }
    };

    obj.inertiaStop = function () {
        this.isInMotion = false;
    };
}
},{"./inertia.js":19}],22:[function(require,module,exports){
module.exports = function (carousel, settings) {
    
    var path_base = require('./path_base.js');
    var point = require('./../point.js');

    var base = new path_base(carousel, {
        fi: 10,
        flatness: 10
    });

    $.extend(this, base);
    $.extend(this.settings, settings);

    this.getPointInternal = function (value) {

        var angle = value * Math.PI * 2 / 360;

        var x = this.settings.fi * value * Math.cos(angle) / (2 * Math.PI);
        var y = this.settings.fi * value * Math.sin(angle) / (2 * Math.PI);

        var z = -1 * Math.pow(value - this.rootValue(), 2) / this.settings.flatness;

        return new point(x, y, z);
    };

    this.rootValue = function () {
        return 450;
    };

    this.minValue = function () {
        return 10;
    };

    this.maxValue = function () {
        return 650;
    };
};
},{"./../point.js":29,"./path_base.js":23}],23:[function(require,module,exports){
module.exports = function (carousel, settings) {
    var rotation = require('./../rotation.js');
    var vector = require('./../vector.js');

    this.settings = {
        shiftX: 0,
        shiftY: 0,
        shiftZ: 0,
        rotationAngleXY: 0,
        rotationAngleZY: 0,
        rotationAngleZX: 0,
        rotateElements: false,
        endless: false
    };

    this.carousel = carousel;
    $.extend(this.settings, settings);


    this.getContainerSize = function () {
        return this.carousel._getContainerSize();
    };

    this.isEndless = function () {
        return this.settings.endless;
    };

    this.minValue = function () {
        return null;
    };

    this.maxValue = function () {
        return null;
    };

    this.rootValue = function () {
        return 0;
    };

    this.incrementValue = function (value, distance) {
        return value + distance;
    };

    this.decrementValue = function (value, distance) {
        return value - distance;
    };

    this.getPoint = function (value) {

        var res = this.getPointInternal(value);

        res.x = res.x + this.settings.shiftX;
        res.y = res.y + this.settings.shiftY;
        res.z = res.z + this.settings.shiftZ;

        var pair = null;

        pair = this.rotate({ a: res.x, b: res.y }, { a: 0, b: 0 }, this.settings.rotationAngleXY);
        res.x = pair.a;
        res.y = pair.b;

        pair = this.rotate({ a: res.z, b: res.y }, { a: 0, b: 0 }, this.settings.rotationAngleZY);
        res.z = pair.a;
        res.y = pair.b;

        pair = this.rotate({ a: res.z, b: res.x }, { a: 0, b: 0 }, this.settings.rotationAngleZX);
        res.z = pair.a;
        res.x = pair.b;

        return res;
    };

    this.rotate = function (pairToRotate, pairCenter, angle) {
        if (angle == 0)
            return pairToRotate;

        var angleInRadians = angle * (Math.PI / 180);
        var cosTheta = Math.cos(angleInRadians);
        var sinTheta = Math.sin(angleInRadians);

        var a = (cosTheta * (pairToRotate.a - pairCenter.a) - sinTheta * (pairToRotate.b - pairCenter.b) + pairCenter.a);
        var b = (sinTheta * (pairToRotate.a - pairCenter.a) + cosTheta * (pairToRotate.b - pairCenter.b) + pairCenter.b);
        pairToRotate.a = a;
        pairToRotate.b = b;

        return pairToRotate;
    };

    this.elementsRotation = function () {
        if (!this.settings.rotateElements)
            return null;

        if (this.settings.rotationAngleZY == 0 && this.settings.rotationAngleZX == 0 && this.settings.rotationAngleXY == 0)
            return null;
        
        var v = new vector();
        v.x = this.settings.rotationAngleZY;
        v.y = -1 * this.settings.rotationAngleZX;
        v.z = -1 * this.settings.rotationAngleXY;

        var angle = -1 * v.length();

        return new rotation(v, angle);
    };
}
},{"./../rotation.js":30,"./../vector.js":35}],24:[function(require,module,exports){
module.exports = function (carousel, settings) {

    var path_base = require('./path_base.js');
    var point = require('./../point.js');

    var base = new path_base(carousel, {
        wideness: 200,
    });

    $.extend(this, base);
    $.extend(this.settings, settings);

    this.getPointInternal = function (value) {
        var y = (value * value * value + value * 20) / (1000 * this.settings.wideness);
        var z = -2 * Math.abs(y);

        return new point(value, y, z);
    };
};
},{"./../point.js":29,"./path_base.js":23}],25:[function(require,module,exports){
module.exports = function(carousel, settings) {

    var path_base = require('./path_base.js');
    var point = require('./../point.js');
    var getBezierBox = require('./../bezier_box.js');
    var getBezier = require('./../bezier.js');

    var base = new path_base(carousel, {
        xyPathBezierPoints: { p1: { x: -100, y: 0 }, p2: { x: 0, y: 0 }, p3: { x: 0, y: 0 }, p4: { x: 100, y: 0 } },
        xzPathBezierPoints: { p1: { x: -100, y: 0 }, p2: { x: 0, y: 0 }, p3: { x: 0, y: 0 }, p4: { x: 100, y: 0 } },
        xyArcLengthBezierPoints: { p1: { x: 0, y: 0 }, p2: { x: 50, y: 50 }, p3: { x: 50, y: 50 }, p4: { x: 100, y: 100 } },
        pathLength: 1000,
        zeroPosition: 0.5,
        width: 1000,
        height: 1000,
        depth: 1000
    });

    $.extend(this, base);
    $.extend(this.settings, settings);

    this.getPointInternal = function (value) {
        var distance = Math.abs(value - this.minValue());
        var absDist = Math.abs(this.maxValue() - this.minValue());

        var bezierT = Math.min(distance / absDist, 1);

        var xyPoints = this.settings.xyPathBezierPoints;
        var xyArcLengthPoints = this.settings.xyArcLengthBezierPoints;
        var xzPoints = this.settings.xzPathBezierPoints;

        var boxXY = getBezierBox(xyPoints.p1, xyPoints.p2, xyPoints.p3, xyPoints.p4);
        var boxXZ = getBezierBox(xzPoints.p1, xzPoints.p2, xzPoints.p3, xzPoints.p4);

        bezierT = getBezier(bezierT, xyArcLengthPoints.p1, xyArcLengthPoints.p2, xyArcLengthPoints.p3, xyArcLengthPoints.p4).y / 100;

        var pointXY = getBezier(bezierT, xyPoints.p1, xyPoints.p2, xyPoints.p3, xyPoints.p4);
        var x = this.normalizeVal(pointXY.x, boxXY.minX, boxXY.maxX);
        var y = this.normalizeVal(pointXY.y, boxXY.minY, boxXY.maxY);
        var z = getBezier(bezierT, xzPoints.p1, xzPoints.p2, xzPoints.p3, xzPoints.p4).y;
        z = this.normalizeVal(z, boxXZ.minY, boxXZ.maxY);

        x = this.settings.width * x - this.settings.width / 2;
        y = this.settings.height * y * -1;
        z = this.settings.depth * z * -1;

        return new point(x, y, z);
    };

    this.normalizeVal = function (val, a, b) {
        var dist = Math.abs(b - a);
        var min = Math.min(a, b);

        if (dist != 0)
            return (val - min) / dist;
        return val - min;
    };

    this.minValue = function () {
        return 0;
    };

    this.maxValue = function () {
        return this.settings.pathLength;
    };

    this.rootValue = function () {
        return this.settings.pathLength * this.settings.zeroPosition;
    };
};
},{"./../bezier.js":10,"./../bezier_box.js":11,"./../point.js":29,"./path_base.js":23}],26:[function(require,module,exports){
module.exports = function (carousel, settings) {

    var path_base = require('./path_base.js');
    var point = require('./../point.js');

    var base = new path_base(carousel, {
        a: 200,
        b: 200,
    });

    $.extend(this, base);
    $.extend(this.settings, settings);

    this.getPointInternal = function (value) {
        value *= -1;
        value -= 180;
        var angle = value * Math.PI * 2 / 360;
        var z = this.settings.b * Math.sin(angle);
        var x = this.settings.a * Math.cos(angle);

        return new point(x, 0, z);
    };

    this.rootValue = function () {
        return 90;
    };

    this.minValue = function () {
        return this.settings.endless ? -90 : 0;
    };

    this.maxValue = function () {
        return this.settings.endless ? 270 : 180;
    };
};
},{"./../point.js":29,"./path_base.js":23}],27:[function(require,module,exports){
module.exports = function (carousel, settings) {

    var path_base = require('./path_base.js');
    var point = require('./../point.js');

    var base = new path_base(carousel, {
        
    });

    $.extend(this, base);
    $.extend(this.settings, settings);

    this.getPointInternal = function (value) {
        var z = -1 * value;
        return new point(0, 0, z);
    };
};
},{"./../point.js":29,"./path_base.js":23}],28:[function(require,module,exports){
module.exports = function (carousel, settings) {

    var path_base = require('./path_base.js');
    var point = require('./../point.js');

    var base = new path_base(carousel, {
        wideness: 200,
    });

    $.extend(this, base);
    $.extend(this.settings, settings);

    this.getPointInternal = function (value) {
        var z = -1 * value * value * (1 / this.settings.wideness);
        return new point(value, 0, z);
    };
};
},{"./../point.js":29,"./path_base.js":23}],29:[function(require,module,exports){
module.exports = function (x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
}
},{}],30:[function(require,module,exports){
module.exports = function(vector, angle) {
    this.vector = vector;
    this.angle = angle;

    this.getString = function () {
        return 'rotate3d(' + this.vector.x + ', ' + this.vector.y + ', ' + this.vector.z + ', ' + this.angle + 'deg)';
    };
};
},{}],31:[function(require,module,exports){
var transform = require('./transform.js');

module.exports = function (carousel) {

    this.carousel = carousel;
    extend(this);
};

function extend(obj) {

    obj.isFallbackMode = function () { return false; };

    obj.getActualDistance = function() {
        return this.carousel.options.distance;
    };

    obj.getRootValue = function() {
        return this.carousel.path.rootValue();
    };

    obj.incrementValue = function (value, increment) {
        return this.carousel.path.incrementValue(value, increment);
    };

    obj.decrementValue = function (value, decrement) {
        return this.carousel.path.decrementValue(value, decrement);
    };

    obj.minValue = function () {
        return this.carousel.path.minValue();
    };

    obj.maxValue = function () {
        return this.carousel.path.maxValue();
    };

    obj.moveTo = function (index) {
        if (this.carousel.motionController.motionInProgress())
            return;

        this.carousel.animation.clearQueue();
        this.carousel.animation.completeCurrentImmediately();

        if (index == this.carousel.options.selectedIndex)
            return;
        if (index == this.carousel.options.selectedIndex + 1) {
            this.carousel.moveForward();
            return;
        }
        if (index == this.carousel.options.selectedIndex - 1) {
            this.carousel.moveBack();
            return;
        }

        index = Math.max(0, index);
        index = Math.min(index, this.carousel.elements.length - 1);

        this.moveToInternal(index);
    };

    obj.moveToInternal = function (index) {
        var distance = this.getActualDistance() * (this.carousel.options.selectedIndex - index);
        this.carousel.inputController.nonInterruptibleMode(true);
        this.carousel._raiseMotionStart();
        this.carousel.animation.animate(0, distance, index, "linear");
    };

    obj.moveBack = function () {
        var pendingTarget = this.carousel.animation.isInProgress ? this.carousel.animation.getTargetValue() : this.carousel.options.selectedIndex;

        if (pendingTarget > 0) {
            pendingTarget--;

            this.carousel.inputController.nonInterruptibleMode(true);
            this.carousel._raiseMotionStart();
            this.carousel.animation.animate(0, this.getActualDistance(), pendingTarget, null);
            return true;
        }
        return false;
    };

    obj.moveForward = function () {
        var pendingTarget = this.carousel.animation.isInProgress ? this.carousel.animation.getTargetValue() : this.carousel.options.selectedIndex;

        if (pendingTarget < this.carousel.elements.length - 1) {
            pendingTarget++;

            this.carousel.inputController.nonInterruptibleMode(true);
            this.carousel._raiseMotionStart();
            this.carousel.animation.animate(0, -1 * this.getActualDistance(), pendingTarget, null);
            return true;
        }
        return false;
    };

    obj.consumeMotion = function (distance) {
        var highFrictionRange = this.carousel._alignElements(distance);

        var optDistance = this.getActualDistance();

        var scrolledElements = parseInt(distance / optDistance, 10);

        var prevIndex = this.carousel.options.selectedIndex;
        this.carousel.options.selectedIndex -= scrolledElements;

        this.carousel.options.selectedIndex = Math.max(0, this.carousel.options.selectedIndex);
        this.carousel.options.selectedIndex = Math.min(this.carousel.options.selectedIndex, this.carousel.elements.length - 1);

        if (prevIndex != this.carousel.options.selectedIndex)
            this.carousel._raiseChangeEvent();

        return { distance: (prevIndex - this.carousel.options.selectedIndex) * optDistance, highFrictionRange: highFrictionRange };
    };

    obj.handleMotionEnd = function (remainingDistance) {
        if (remainingDistance == 0)
            return;

        var targetIndex = this.carousel.options.selectedIndex;

        if (Math.abs(remainingDistance) > this.getActualDistance() / 2) {
            if (remainingDistance < 0)
                targetIndex++;
            else
                targetIndex--;
        }

        if (this.carousel.elements.length == 0)
            targetIndex = 0;
        else {
            targetIndex = Math.max(0, targetIndex);
            targetIndex = Math.min(targetIndex, this.carousel.elements.length - 1);
        }

        var targetDistance = (this.carousel.options.selectedIndex - targetIndex) * this.getActualDistance();

        var duration = Math.abs(this.carousel.options.rotationAnimationDuration * (remainingDistance / this.getActualDistance()));
        duration = Math.min(duration, this.carousel.options.rotationAnimationDuration / 2);

        this.carousel.animation.animate(remainingDistance, targetDistance, targetIndex, null, duration);
    };

    obj.alignElements = function (animationShift) {
        if (this.carousel.elements.length == 0 || this.carousel.options.selectedIndex < 0)
            return false;

        this.carousel.containerSize = this.carousel._getContainerSize();

        var shift = 0;
        if (typeof (animationShift) != "undefined")
            shift = animationShift;

        var highFrictionRange = false;
        // slow down at the ends
        if (
            (this.carousel.options.selectedIndex == 0 && shift > 0) ||
            (this.carousel.options.selectedIndex == this.carousel.elements.length - 1 && shift < 0)
        ) {
            shift = Math.pow(Math.abs(shift), 0.7) * (shift / Math.abs(shift));
            highFrictionRange = true;
        }

        var location = this.getRootValue();
        var rangeShift = 0;
        if ((this.carousel.options.selectedIndex == 0 && shift > 0) || (this.carousel.options.selectedIndex == this.carousel.elements.length - 1 && shift < 0))
            rangeShift = shift;
        var ranges = this.getFadeRanges(location + rangeShift);

        for (var i = this.carousel.options.selectedIndex; i < this.carousel.elements.length; i++) {
            this.setElementPosition(this.carousel.elements[i], location + shift, ranges);
            location = this.incrementValue(location, this.getActualDistance());
        }

        location = this.getRootValue();

        for (var i = this.carousel.options.selectedIndex - 1; i >= 0; i--) {
            location = this.decrementValue(location, this.getActualDistance());
            this.setElementPosition(this.carousel.elements[i], location + shift, ranges);
        }

        this.setZIndexes();

        return highFrictionRange;
    };

    obj.setElementPosition = function (element, value, ranges) {
        //this method is performance critical so we trying to avoid jQuery usage

        if (this.setElementVisibility(ranges, element, value)) {

            var point = this.carousel.path.getPoint(value);

            var elementTransform = new transform();

            elementTransform.translateZ = point.z * this.carousel.options.scaleZ;
            elementTransform.translateX = point.x * this.carousel.options.scaleX + this.carousel.containerSize.width / 2 - element.size.width / 2;
            elementTransform.translateY = point.y * this.carousel.options.scaleY + this.carousel.containerSize.height / 2 - element.size.height / 2;

            var pathRotation = this.carousel.path.elementsRotation();
            if (pathRotation)
                elementTransform.rotations.push(pathRotation);

            if (this.carousel.options.mode3D == 'scale') {
                elementTransform.scale = this.carousel.options.perspective / (this.carousel.options.perspective - elementTransform.translateZ);
                elementTransform.translateZ = 0;
            }

            for (var i = 0; i < this.carousel.effects.length; i++) {
                if (this.carousel.effects[i].applyPhase === 'positioning')
                    this.carousel.effects[i].apply(elementTransform, element, value);
            }

            elementTransform.apply(element.element);
            element.location = point;
            return true;
        }
        return false;
    };

    obj.setZIndexes = function () {

        var tmpElements = [];
        for (var i = 0; i < this.carousel.elements.length; i++) {
            var e = this.carousel.elements[i];
            if (e.location) // element has been positioned
                tmpElements.push(e);
        }

        tmpElements.sort(function (e1, e2) {
            return e1.location.z - e2.location.z;
        });
        for (var i = 0; i < tmpElements.length; i++) {
            tmpElements[i].$element.get(0).style.zIndex = i;
        }
    };

    obj.getFadeRanges = function (root) {
        var location = root;

        var res = [];

        if (this.carousel.options.numberOfElementsToDisplayLeft != null) {
            res.push({
                from: this.decrementValue(location, this.getActualDistance() * (this.carousel.options.numberOfElementsToDisplayLeft + 1)),
                to: this.decrementValue(location, this.getActualDistance() * (this.carousel.options.numberOfElementsToDisplayLeft)),
                hide: 'before'
            });
        }

        if (this.carousel.options.numberOfElementsToDisplayRight != null) {
            res.push({
                from: this.incrementValue(location, this.getActualDistance() * (this.carousel.options.numberOfElementsToDisplayRight)),
                to: this.incrementValue(location, this.getActualDistance() * (this.carousel.options.numberOfElementsToDisplayRight + 1)),
                hide: 'after'
            });
        }

        return res;
    };

    obj.setElementVisibility = function (fadeRanges, element, value) {
        var $element = element.$element;
        var hidden = false;

        if ((this.minValue() != null && value < this.minValue())
            || (this.maxValue() != null && value > this.maxValue()))
            hidden = true;
        else {

            if (fadeRanges.length == 0)
                $element.css({ opacity: 1 });

            for (var i = 0; i < fadeRanges.length; i++) {
                var range = fadeRanges[i];

                if ((range.hide == 'before' && value <= range.from) || (range.hide == 'after' && value >= range.to)) {
                    hidden = true;
                    break;
                }

                if (value > range.from && value < range.to) {
                    var distance = range.to - range.from;
                    var passed = Math.abs(value - (range.hide == 'after' ? range.from : range.to));
                    var opacity = (distance - passed) / distance;
                    $element.css({ opacity: opacity });
                    break;
                } else {
                    $element.css({ opacity: 1 });
                }
            }
        }

        this.setElementVisibilityInternal(hidden, element);
        return !hidden;
    };

    obj.destroy = function () {
        for (var i = 0; i < this.carousel.elements.length; i++) {
            this.carousel.elements[i].$element.css({
                left: '0',
                top: '0',
                transform: '',
                opacity: '1'
            });
            this.carousel.elements[i].$element.show();
        }
    };

    obj.setElementVisibilityInternal = function (hide, element) {
        if (hide) {
            element.$element.hide();
        } else {
            element.$element.show();
        }
    };
}
},{"./transform.js":33}],32:[function(require,module,exports){
module.exports = function (width, height) {
    this.width = width;
    this.height = height;
};
},{}],33:[function(require,module,exports){
module.exports = function() {
    this.translateX = 0;
    this.translateY = 0;
    this.translateZ = 0;
    this.scale = 1;

    this.scaleX = 1;
    this.scaleY = 1;
    this.scaleZ = 1;

    this.rotations = [];

    this.apply = function (e) {
        var str = '';
        if (this.translateX != 0 || this.translateY != 0 || this.translateZ != 0)
            str = 'translate3d(' + this.translateX + 'px, ' + this.translateY + 'px, ' + this.translateZ + 'px)';

        if (this.scale != 1)
            str += ' scale(' + this.scale + ')';

        if (this.scaleX != 1)
            str += ' scaleX(' + this.scaleX + ')';
        if (this.scaleY != 1)
            str += ' scaleY(' + this.scaleY + ')';
        if (this.scaleZ != 1)
            str += ' scaleZ(' + this.scaleZ + ')';

        for (var i = 0; i < this.rotations.length; i++)
            str += ' ' + this.rotations[i].getString();

        e.style.transform = str;
        e.style.webkitTransform = str;
        e.style.msTransform = str;
    };
};
},{}],34:[function(require,module,exports){
var utils = function() {
    this.getObjectPropertyValue = function (obj, property) {
        var parts = property.split('.');
        var res = obj;
        for (var i = 0; i < parts.length; i++) {
            res = res[parts[i]];
        }

        return res;
    };

    this.setObjectPropertyValue = function (obj, property, value) {
        var parts = property.split('.');
        var target = obj;
        for (var i = 0; i < parts.length - 1; i++) {
            target = target[parts[i]];
        }

        target[parts[parts.length - 1]] = value;
    }
}

module.exports = new utils();
},{}],35:[function(require,module,exports){
module.exports = function () {

    this.x = 0;
    this.y = 0;
    this.z = 0;

    this.initFromPoints = function (p1, p2) {
        this.x = p2.x - p1.x;
        this.y = p2.y - p1.y;
        this.z = p2.z - p1.z;
    };

    this.normalize = function () {
        var length = this.length();
        this.x /= length;
        this.y /= length;
        this.z /= length;
    };

    this.length = function () {
        return Math.sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
    };

    this.angle = function (v) {

        var scalarMultiplication = this.x * v.x + this.y * v.y + this.z * v.z;
        var absThis = this.length();
        var absV = Math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z);

        var cosA = scalarMultiplication / (absThis * absV);

        var a = Math.acos(cosA);

        if (this.z > v.z)
            a *= -1;

        return a * 180 / Math.PI;
    };

    this.perpendicularX = function () {
        var res = new vector();
        res.y = this.y;
        res.z = this.z;
        res.x = -1 * (this.y * res.y + this.z * res.z) / this.x;
        return res;
    };
};
},{}]},{},[12])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL1Zpc3VhbEVmZmVjdHMvZWZmZWN0X2FsbGlnbl90b19wYXRoLmpzIiwiLi4vQ2Fyb3VzZWwvSlNTb3VyY2VzL0Nhcm91c2VsQ29udHJvbC9WaXN1YWxFZmZlY3RzL2VmZmVjdF9iYXNlLmpzIiwiLi4vQ2Fyb3VzZWwvSlNTb3VyY2VzL0Nhcm91c2VsQ29udHJvbC9WaXN1YWxFZmZlY3RzL2VmZmVjdF9mYWRlX2F3YXkuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL1Zpc3VhbEVmZmVjdHMvZWZmZWN0X3BvcF9vdXRfc2VsZWN0ZWQuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL1Zpc3VhbEVmZmVjdHMvZWZmZWN0X3JlZmxlY3Rpb24uanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL1Zpc3VhbEVmZmVjdHMvZWZmZWN0X3JvdGF0aW9uLmpzIiwiLi4vQ2Fyb3VzZWwvSlNTb3VyY2VzL0Nhcm91c2VsQ29udHJvbC9WaXN1YWxFZmZlY3RzL2VmZmVjdF9zaGFkb3cuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL1Zpc3VhbEVmZmVjdHMvZWZmZWN0X3NpemVfYWRqdXN0bWVudC5qcyIsIi4uL0Nhcm91c2VsL0pTU291cmNlcy9DYXJvdXNlbENvbnRyb2wvYXV0b19yb3RhdG9yLmpzIiwiLi4vQ2Fyb3VzZWwvSlNTb3VyY2VzL0Nhcm91c2VsQ29udHJvbC9iZXppZXIuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL2Jlemllcl9ib3guanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL2Nhcm91c2VsLmpzIiwiLi4vQ2Fyb3VzZWwvSlNTb3VyY2VzL0Nhcm91c2VsQ29udHJvbC9jb211bGF0aXZlX2FuaW1hdGlvbi5qcyIsIi4uL0Nhcm91c2VsL0pTU291cmNlcy9DYXJvdXNlbENvbnRyb2wvZWFzaW5ncy5qcyIsIi4uL0Nhcm91c2VsL0pTU291cmNlcy9DYXJvdXNlbENvbnRyb2wvZWxlbWVudHNfc2l6ZV91cGRhdGVyLmpzIiwiLi4vQ2Fyb3VzZWwvSlNTb3VyY2VzL0Nhcm91c2VsQ29udHJvbC9lbmRsZXNzX3JvdGF0aW9uX2xvZ2ljX2NvbnRyb2xsZXIuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL2ZhbGxiYWNrX3JvdGF0aW9uX2xvZ2ljX2NvbnRyb2xsZXIuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL2ZsdWlkX2xheW91dC5qcyIsIi4uL0Nhcm91c2VsL0pTU291cmNlcy9DYXJvdXNlbENvbnRyb2wvaW5lcnRpYS5qcyIsIi4uL0Nhcm91c2VsL0pTU291cmNlcy9DYXJvdXNlbENvbnRyb2wvaW5wdXRfY29udHJvbGxlci5qcyIsIi4uL0Nhcm91c2VsL0pTU291cmNlcy9DYXJvdXNlbENvbnRyb2wvbW90aW9uX2NvbnRyb2xsZXIuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL3BhdGhzL3BhdGhfYXJjaGltZWRlc19zcGlyYWwuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL3BhdGhzL3BhdGhfYmFzZS5qcyIsIi4uL0Nhcm91c2VsL0pTU291cmNlcy9DYXJvdXNlbENvbnRyb2wvcGF0aHMvcGF0aF9jdWJpYy5qcyIsIi4uL0Nhcm91c2VsL0pTU291cmNlcy9DYXJvdXNlbENvbnRyb2wvcGF0aHMvcGF0aF9jdWJpY19iZXppZXIuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL3BhdGhzL3BhdGhfZWxsaXBzZS5qcyIsIi4uL0Nhcm91c2VsL0pTU291cmNlcy9DYXJvdXNlbENvbnRyb2wvcGF0aHMvcGF0aF9saW5lLmpzIiwiLi4vQ2Fyb3VzZWwvSlNTb3VyY2VzL0Nhcm91c2VsQ29udHJvbC9wYXRocy9wYXRoX3BhcmFib2xhLmpzIiwiLi4vQ2Fyb3VzZWwvSlNTb3VyY2VzL0Nhcm91c2VsQ29udHJvbC9wb2ludC5qcyIsIi4uL0Nhcm91c2VsL0pTU291cmNlcy9DYXJvdXNlbENvbnRyb2wvcm90YXRpb24uanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL3JvdGF0aW9uX2xvZ2ljX2NvbnRyb2xsZXIuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL3NpemUuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL3RyYW5zZm9ybS5qcyIsIi4uL0Nhcm91c2VsL0pTU291cmNlcy9DYXJvdXNlbENvbnRyb2wvdXRpbHMuanMiLCIuLi9DYXJvdXNlbC9KU1NvdXJjZXMvQ2Fyb3VzZWxDb250cm9sL3ZlY3Rvci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN0QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNqQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMvQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNiQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzdIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDckRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDckNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9NQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM3RkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMzRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEhBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdFFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDNUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNyRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNsQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1BBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNUQTtBQUNBO0FBQ0E7QUFDQTs7QUNIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3RCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNhcm91c2VsLCBzZXR0aW5ncykge1xyXG5cclxuICAgIHZhciBlZmZlY3RfYmFzZSA9IHJlcXVpcmUoJy4vZWZmZWN0X2Jhc2UuanMnKTtcclxuICAgIHZhciB2ZWN0b3IgPSByZXF1aXJlKCcuLy4uL3ZlY3Rvci5qcycpO1xyXG4gICAgdmFyIHJvdGF0aW9uID0gcmVxdWlyZSgnLi8uLi9yb3RhdGlvbi5qcycpO1xyXG5cclxuICAgIHZhciBiYXNlID0gbmV3IGVmZmVjdF9iYXNlKGNhcm91c2VsLCB7XHJcbiAgICAgICAgXHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLnhEaXJlY3Rpb24gPSBuZXcgdmVjdG9yKCk7XHJcbiAgICB0aGlzLnhEaXJlY3Rpb24ueCA9IDE7XHJcblxyXG4gICAgdGhpcy55RGlyZWN0aW9uID0gbmV3IHZlY3RvcigpO1xyXG4gICAgdGhpcy55RGlyZWN0aW9uLnkgPSAxO1xyXG4gICAgXHJcbiAgICAkLmV4dGVuZCh0aGlzLCBiYXNlKTtcclxuICAgICQuZXh0ZW5kKHRoaXMuc2V0dGluZ3MsIHNldHRpbmdzKTtcclxuXHJcbiAgICB0aGlzLmFwcGx5ID0gZnVuY3Rpb24gKGVsZW1lbnRUcmFuc2Zvcm0sIGVsZW1lbnQsIHZhbHVlKSB7XHJcblxyXG4gICAgICAgIHZhciBjdXJyZW50UG9pbnQgPSB0aGlzLmNhcm91c2VsLnBhdGguZ2V0UG9pbnQodmFsdWUpO1xyXG4gICAgICAgIHZhciBkaXJlY3Rpb25Qb2ludCA9IHRoaXMuY2Fyb3VzZWwucGF0aC5nZXRQb2ludCh2YWx1ZSArIDAuMSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdmFyIGRpcmVjdGlvbiA9IG5ldyB2ZWN0b3IoKTtcclxuICAgICAgICBkaXJlY3Rpb24uaW5pdEZyb21Qb2ludHMoY3VycmVudFBvaW50LCBkaXJlY3Rpb25Qb2ludCk7XHJcbiAgICAgICAgdmFyIGFuZ2xlID0gZGlyZWN0aW9uLmFuZ2xlKHRoaXMueERpcmVjdGlvbik7XHJcblxyXG4gICAgICAgIGFuZ2xlICo9IHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5hbGxpZ25FbGVtZW50c1dpdGhQYXRoQ29lZmZpY2llbnQ7XHJcblxyXG4gICAgICAgIGVsZW1lbnRUcmFuc2Zvcm0ucm90YXRpb25zLnB1c2gobmV3IHJvdGF0aW9uKHRoaXMueURpcmVjdGlvbiwgYW5nbGUpKTtcclxuICAgIH07XHJcbn07IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoY2Fyb3VzZWwsIHNldHRpbmdzKSB7XHJcblxyXG4gICAgdGhpcy5jYXJvdXNlbCA9IGNhcm91c2VsO1xyXG5cclxuICAgIHRoaXMuYXBwbHlQaGFzZSA9ICdwb3NpdGlvbmluZyc7XHJcblxyXG4gICAgdGhpcy5zZXR0aW5ncyA9IHtcclxuICAgIH07XHJcblxyXG4gICAgJC5leHRlbmQodGhpcy5zZXR0aW5ncywgc2V0dGluZ3MpO1xyXG5cclxuICAgIHRoaXMuZ2V0QXBwbHlpbmdQcmlvcml0eSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gMDtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5hcHBseSA9IGZ1bmN0aW9uIChlbGVtZW50VHJhbnNmb3JtLCBlbGVtZW50LCB2YWx1ZSkge1xyXG5cclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5yZXZlcnQgPSBmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgfTtcclxufSIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNhcm91c2VsLCBzZXR0aW5ncykge1xyXG5cclxuICAgIHZhciBlZmZlY3RfYmFzZSA9IHJlcXVpcmUoJy4vZWZmZWN0X2Jhc2UuanMnKTtcclxuICAgIHZhciBnZXRCZXppZXIgPSByZXF1aXJlKCcuLy4uL2Jlemllci5qcycpO1xyXG4gICAgXHJcbiAgICB2YXIgYmFzZSA9IG5ldyBlZmZlY3RfYmFzZShjYXJvdXNlbCwge1xyXG5cclxuICAgIH0pO1xyXG5cclxuICAgIFxyXG4gICAgJC5leHRlbmQodGhpcywgYmFzZSk7XHJcbiAgICAkLmV4dGVuZCh0aGlzLnNldHRpbmdzLCBzZXR0aW5ncyk7XHJcblxyXG4gICAgdGhpcy5hcHBseSA9IGZ1bmN0aW9uIChlbGVtZW50VHJhbnNmb3JtLCBlbGVtZW50LCB2YWx1ZSkge1xyXG4gICAgICAgIHZhciByb290ID0gdGhpcy5jYXJvdXNlbC5wYXRoLnJvb3RWYWx1ZSgpO1xyXG4gICAgICAgIHZhciBkaXN0YW5jZSA9IE1hdGguYWJzKHZhbHVlIC0gcm9vdCk7XHJcbiAgICAgICAgdmFyIGFic0Rpc3QgPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMuZGlzdGFuY2UgKiB0aGlzLmNhcm91c2VsLm9wdGlvbnMuZmFkZUF3YXlOdW1iZXJPZkNvbmZpZ3VyYWJsZUVsZW1lbnRzO1xyXG5cclxuICAgICAgICB2YXIgYmV6aWVyVCA9IDEgLSBNYXRoLm1pbihkaXN0YW5jZSAvIGFic0Rpc3QsIDEpO1xyXG5cclxuICAgICAgICB2YXIgcG9pbnRzID0gdGhpcy5jYXJvdXNlbC5vcHRpb25zLmZhZGVBd2F5QmV6aWVyUG9pbnRzO1xyXG4gICAgICAgIHZhciBvcGFjaXR5ID0gZ2V0QmV6aWVyKGJlemllclQsIHBvaW50cy5wMSwgcG9pbnRzLnAyLCBwb2ludHMucDMsIHBvaW50cy5wNCkueTtcclxuICAgICAgICBlbGVtZW50LiRlbGVtZW50LmNzcyh7IG9wYWNpdHk6IG9wYWNpdHkgLyAxMDAgfSk7XHJcbiAgICB9O1xyXG59OyIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNhcm91c2VsLCBzZXR0aW5ncykge1xyXG5cclxuICAgIHZhciBlZmZlY3RfYmFzZSA9IHJlcXVpcmUoJy4vZWZmZWN0X2Jhc2UuanMnKTtcclxuXHJcbiAgICB2YXIgYmFzZSA9IG5ldyBlZmZlY3RfYmFzZShjYXJvdXNlbCwge1xyXG5cclxuICAgIH0pO1xyXG5cclxuICAgICQuZXh0ZW5kKHRoaXMsIGJhc2UpO1xyXG4gICAgJC5leHRlbmQodGhpcy5zZXR0aW5ncywgc2V0dGluZ3MpO1xyXG5cclxuICAgIHRoaXMuYXBwbHkgPSBmdW5jdGlvbiAoZWxlbWVudFRyYW5zZm9ybSwgZWxlbWVudCwgdmFsdWUpIHtcclxuICAgICAgICB2YXIgZGlzdEZyb21Sb290ID0gTWF0aC5hYnModmFsdWUgLSB0aGlzLmNhcm91c2VsLnBhdGgucm9vdFZhbHVlKCkpO1xyXG4gICAgICAgIHZhciBjb2VmID0gMSAtIGRpc3RGcm9tUm9vdCAvIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5kaXN0YW5jZTtcclxuICAgICAgICBjb2VmID0gTWF0aC5tYXgoY29lZiwgMCk7XHJcblxyXG4gICAgICAgIGVsZW1lbnRUcmFuc2Zvcm0udHJhbnNsYXRlWCArPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMucG9wb3V0U2VsZWN0ZWRTaGlmdFggKiBjb2VmO1xyXG4gICAgICAgIGVsZW1lbnRUcmFuc2Zvcm0udHJhbnNsYXRlWSArPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMucG9wb3V0U2VsZWN0ZWRTaGlmdFkgKiBjb2VmO1xyXG4gICAgICAgIGVsZW1lbnRUcmFuc2Zvcm0udHJhbnNsYXRlWiArPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMucG9wb3V0U2VsZWN0ZWRTaGlmdFogKiBjb2VmO1xyXG4gICAgfTtcclxufTsiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChjYXJvdXNlbCwgc2V0dGluZ3MpIHtcclxuXHJcbiAgICB2YXIgZWZmZWN0X2Jhc2UgPSByZXF1aXJlKCcuL2VmZmVjdF9iYXNlLmpzJyk7XHJcblxyXG4gICAgdmFyIGJhc2UgPSBuZXcgZWZmZWN0X2Jhc2UoY2Fyb3VzZWwsIHtcclxuXHJcbiAgICB9KTtcclxuXHJcbiAgICAkLmV4dGVuZCh0aGlzLCBiYXNlKTtcclxuICAgICQuZXh0ZW5kKHRoaXMuc2V0dGluZ3MsIHNldHRpbmdzKTtcclxuXHJcbiAgICB0aGlzLmFwcGx5ID0gZnVuY3Rpb24gKGVsZW1lbnRUcmFuc2Zvcm0sIGVsZW1lbnQsIHZhbHVlKSB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgaWYoZWxlbWVudC5pc1JlZmxlY3Rpb25BcHBsaWVkKVxyXG4gICAgICAgICAgICByZXR1cm47XHJcblxyXG4gICAgICAgIGVsZW1lbnQuaXNSZWZsZWN0aW9uQXBwbGllZCA9IHRydWU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgZWxlbWVudC4kZWxlbWVudC5jc3MoXCJib3gtcmVmbGVjdFwiLCBcImJlbG93IFwiICsgdGhpcy5jYXJvdXNlbC5vcHRpb25zLnJlZmxlY3Rpb25CZWxvdyArXHJcbiAgICAgICAgICAgIFwicHggLXdlYmtpdC1ncmFkaWVudChsaW5lYXIsIGxlZnQgdG9wLCBsZWZ0IGJvdHRvbSwgZnJvbSh0cmFuc3BhcmVudCksIGNvbG9yLXN0b3AoXCIgK1xyXG4gICAgICAgICAgICAgKDEgLSB0aGlzLmNhcm91c2VsLm9wdGlvbnMucmVmbGVjdGlvbkhlaWdodCkgKyBcIiwgdHJhbnNwYXJlbnQpLCB0byhibGFjaykpXCIpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnJldmVydCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5lbGVtZW50c1tpXS5pc1JlZmxlY3Rpb25BcHBsaWVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwuZWxlbWVudHNbaV0uJGVsZW1lbnQuY3NzKFwiYm94LXJlZmxlY3RcIiwgJycpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbn07IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoY2Fyb3VzZWwsIHNldHRpbmdzKSB7XHJcblxyXG4gICAgdmFyIGVmZmVjdF9iYXNlID0gcmVxdWlyZSgnLi9lZmZlY3RfYmFzZS5qcycpO1xyXG4gICAgdmFyIGdldEJlemllciA9IHJlcXVpcmUoJy4vLi4vYmV6aWVyLmpzJyk7XHJcbiAgICB2YXIgdmVjdG9yID0gcmVxdWlyZSgnLi8uLi92ZWN0b3IuanMnKTtcclxuICAgIHZhciByb3RhdGlvbiA9IHJlcXVpcmUoJy4vLi4vcm90YXRpb24uanMnKTtcclxuXHJcbiAgICB2YXIgYmFzZSA9IG5ldyBlZmZlY3RfYmFzZShjYXJvdXNlbCwge1xyXG5cclxuICAgIH0pO1xyXG5cclxuICAgICQuZXh0ZW5kKHRoaXMsIGJhc2UpO1xyXG4gICAgJC5leHRlbmQodGhpcy5zZXR0aW5ncywgc2V0dGluZ3MpO1xyXG5cclxuICAgIHRoaXMuYXBwbHkgPSBmdW5jdGlvbiAoZWxlbWVudFRyYW5zZm9ybSwgZWxlbWVudCwgdmFsdWUpIHtcclxuICAgICAgICB2YXIgcm9vdCA9IHRoaXMuY2Fyb3VzZWwucGF0aC5yb290VmFsdWUoKTtcclxuICAgICAgICB2YXIgZGlzdGFuY2UgPSBNYXRoLmFicyh2YWx1ZSAtIHJvb3QpO1xyXG4gICAgICAgIHZhciBhYnNEaXN0ID0gdGhpcy5jYXJvdXNlbC5vcHRpb25zLmRpc3RhbmNlICogdGhpcy5jYXJvdXNlbC5vcHRpb25zLnJvdGF0aW9uTnVtYmVyT2ZDb25maWd1cmFibGVFbGVtZW50cztcclxuXHJcbiAgICAgICAgdmFyIGJlemllclQgPSAxIC0gTWF0aC5taW4oZGlzdGFuY2UgLyBhYnNEaXN0LCAxKTtcclxuXHJcbiAgICAgICAgdmFyIHBvaW50cyA9IHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5yb3RhdGlvbkJlemllclBvaW50cztcclxuICAgICAgICB2YXIgYW5nbGUgPSBnZXRCZXppZXIoYmV6aWVyVCwgcG9pbnRzLnAxLCBwb2ludHMucDIsIHBvaW50cy5wMywgcG9pbnRzLnA0KS55O1xyXG5cclxuICAgICAgICBpZiAodmFsdWUgPCByb290ICYmIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5yb3RhdGlvbkludmVydEZvck5lZ2F0aXZlKVxyXG4gICAgICAgICAgICBhbmdsZSAqPSAtMTtcclxuXHJcbiAgICAgICAgdmFyIHJvdGF0aW9uVmVjdG9yID0gbmV3IHZlY3RvcigpO1xyXG4gICAgICAgIHJvdGF0aW9uVmVjdG9yLnggPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMucm90YXRpb25WZWN0b3JYLFxyXG4gICAgICAgIHJvdGF0aW9uVmVjdG9yLnkgPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMucm90YXRpb25WZWN0b3JZLFxyXG4gICAgICAgIHJvdGF0aW9uVmVjdG9yLnogPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMucm90YXRpb25WZWN0b3JaLFxyXG4gICAgICAgIGVsZW1lbnRUcmFuc2Zvcm0ucm90YXRpb25zLnB1c2gobmV3IHJvdGF0aW9uKHJvdGF0aW9uVmVjdG9yLCBhbmdsZSkpO1xyXG4gICAgfTtcclxufTsiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChjYXJvdXNlbCwgc2V0dGluZ3MpIHtcclxuXHJcbiAgICB2YXIgZWZmZWN0X2Jhc2UgPSByZXF1aXJlKCcuL2VmZmVjdF9iYXNlLmpzJyk7XHJcblxyXG4gICAgdmFyIGJhc2UgPSBuZXcgZWZmZWN0X2Jhc2UoY2Fyb3VzZWwsIHtcclxuXHJcbiAgICB9KTtcclxuXHJcbiAgICAkLmV4dGVuZCh0aGlzLCBiYXNlKTtcclxuICAgICQuZXh0ZW5kKHRoaXMuc2V0dGluZ3MsIHNldHRpbmdzKTtcclxuXHJcbiAgICB0aGlzLmFwcGx5ID0gZnVuY3Rpb24gKGVsZW1lbnRUcmFuc2Zvcm0sIGVsZW1lbnQsIHZhbHVlKSB7XHJcblxyXG4gICAgICAgIHZhciBib3hTaGFkb3cgPSAnMHB4IDBweCAnICsgdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNoYWRvd0JsdXJSYWRpdXMgKyAncHggJyArIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zaGFkb3dTcHJlYWRSYWRpdXMgKyAncHggIzAwMDAwMCc7XHJcblxyXG4gICAgICAgIGVsZW1lbnQuJGVsZW1lbnQuY3NzKHtcclxuICAgICAgICAgICAgJy13ZWJraXQtYm94LXNoYWRvdyc6IGJveFNoYWRvdyxcclxuICAgICAgICAgICAgJy1tb3otYm94LXNoYWRvdyc6IGJveFNoYWRvdyxcclxuICAgICAgICAgICAgJ2JveC1zaGFkb3cnOiBib3hTaGFkb3dcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5yZXZlcnQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwuZWxlbWVudHNbaV0uJGVsZW1lbnQuY3NzKHtcclxuICAgICAgICAgICAgICAgICctd2Via2l0LWJveC1zaGFkb3cnOiAnJyxcclxuICAgICAgICAgICAgICAgICctbW96LWJveC1zaGFkb3cnOiAnJyxcclxuICAgICAgICAgICAgICAgICdib3gtc2hhZG93JzogJydcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxufTsiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChjYXJvdXNlbCwgc2V0dGluZ3MpIHtcclxuXHJcbiAgICB2YXIgZWZmZWN0X2Jhc2UgPSByZXF1aXJlKCcuL2VmZmVjdF9iYXNlLmpzJyk7XHJcbiAgICB2YXIgZ2V0QmV6aWVyID0gcmVxdWlyZSgnLi8uLi9iZXppZXIuanMnKTtcclxuXHJcbiAgICB2YXIgYmFzZSA9IG5ldyBlZmZlY3RfYmFzZShjYXJvdXNlbCwge1xyXG5cclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICAkLmV4dGVuZCh0aGlzLCBiYXNlKTtcclxuICAgICQuZXh0ZW5kKHRoaXMuc2V0dGluZ3MsIHNldHRpbmdzKTtcclxuXHJcbiAgICB0aGlzLmFwcGx5ID0gZnVuY3Rpb24gKGVsZW1lbnRUcmFuc2Zvcm0sIGVsZW1lbnQsIHZhbHVlKSB7XHJcbiAgICAgICAgdmFyIHJvb3QgPSB0aGlzLmNhcm91c2VsLnBhdGgucm9vdFZhbHVlKCk7XHJcbiAgICAgICAgdmFyIGRpc3RhbmNlID0gTWF0aC5hYnModmFsdWUgLSByb290KTtcclxuICAgICAgICB2YXIgYWJzRGlzdCA9IHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5kaXN0YW5jZSAqIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zaXplQWRqdXN0bWVudE51bWJlck9mQ29uZmlndXJhYmxlRWxlbWVudHM7XHJcblxyXG4gICAgICAgIHZhciBiZXppZXJUID0gMSAtIE1hdGgubWluKGRpc3RhbmNlIC8gYWJzRGlzdCwgMSk7XHJcblxyXG4gICAgICAgIHZhciBwb2ludHMgPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2l6ZUFkanVzdG1lbnRCZXppZXJQb2ludHM7XHJcbiAgICAgICAgdmFyIHNjYWxlID0gZ2V0QmV6aWVyKGJlemllclQsIHBvaW50cy5wMSwgcG9pbnRzLnAyLCBwb2ludHMucDMsIHBvaW50cy5wNCkueTtcclxuICAgICAgICBlbGVtZW50VHJhbnNmb3JtLnNjYWxlID0gc2NhbGUgLyAxMDA7XHJcbiAgICB9O1xyXG59OyIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNhcm91c2VsKSB7XHJcblxyXG4gICAgdGhpcy5hdXRvcm90YXRpb25TdGFydGVkID0gZmFsc2U7XHJcbiAgICB0aGlzLmNhcm91c2VsID0gY2Fyb3VzZWw7XHJcbiAgICB0aGlzLmF1dG9yb3RhdGlvblRpbWVyID0gbnVsbDtcclxuXHJcbiAgICBleHRlbmQodGhpcyk7XHJcbiAgICB0aGlzLmNhcm91c2VsLndpZGdldCgpLm9uKFwibW90aW9uRW5kXCIsIHRoaXMub25Nb3Rpb25FbmQpO1xyXG4gICAgdGhpcy5hcHBseVNldHRpbmdzKCk7XHJcbn07XHJcblxyXG5mdW5jdGlvbiBleHRlbmQob2JqKSB7XHJcbiAgICBvYmouYXBwbHlTZXR0aW5ncyA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAodGhpcy5jYXJvdXNlbC5vcHRpb25zLmF1dG9yb3RhdGlvbilcclxuICAgICAgICAgICAgdGhpcy5lbnN1cmVTdGFydGVkKCk7XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXV0b3JvdGF0aW9uU3RhcnRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5hdXRvcm90YXRpb25UaW1lcilcclxuICAgICAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLmF1dG9yb3RhdGlvblRpbWVyKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5lbnN1cmVTdGFydGVkID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmF1dG9yb3RhdGlvblN0YXJ0ZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5hdXRvcm90YXRpb25TdGFydGVkID0gISEodGhpcy5tb3ZlKCkpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgb2JqLm1vdmUgPSAkLnByb3h5KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNhcm91c2VsLm9wdGlvbnMuYXV0b3JvdGF0aW9uRGlyZWN0aW9uID09ICdsZWZ0Jykge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jYXJvdXNlbC5tb3ZlQmFjaygpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5hdXRvcm90YXRpb25EaXJlY3Rpb24gPT0gJ3JpZ2h0Jykge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jYXJvdXNlbC5tb3ZlRm9yd2FyZCgpO1xyXG4gICAgICAgIH1cclxuICAgIH0sIG9iaik7XHJcblxyXG4gICAgb2JqLm9uTW90aW9uRW5kID0gJC5wcm94eShmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuYXV0b3JvdGF0aW9uU3RhcnRlZCkge1xyXG4gICAgICAgICAgICB0aGlzLmF1dG9yb3RhdGlvblRpbWVyID0gc2V0VGltZW91dCh0aGlzLm1vdmUsIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5hdXRvcm90YXRpb25QYXVzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSwgb2JqKTtcclxuXHJcbiAgICBvYmouZGVzdHJveSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuY2Fyb3VzZWwud2lkZ2V0KCkub2ZmKFwibW90aW9uRW5kXCIsIHRoaXMub25Nb3Rpb25FbmQpO1xyXG4gICAgfVxyXG59IiwiZnVuY3Rpb24gYjEodCkgeyByZXR1cm4gdCAqIHQgKiB0OyB9O1xyXG5cclxuZnVuY3Rpb24gYjIodCkgeyByZXR1cm4gMyAqIHQgKiB0ICogKDEgLSB0KTsgfTtcclxuXHJcbmZ1bmN0aW9uIGIzKHQpIHsgcmV0dXJuIDMgKiB0ICogKDEgLSB0KSAqICgxIC0gdCk7IH07XHJcblxyXG5mdW5jdGlvbiBiNCh0KSB7IHJldHVybiAoMSAtIHQpICogKDEgLSB0KSAqICgxIC0gdCk7IH07XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKHQsIHAxLCBjcDEsIGNwMiwgcDIpIHtcclxuICAgIHZhciBwb3MgPSB7eDowLHk6MH07XHJcbiAgICBwb3MueCA9IHAxLngqYjEodCkgKyBjcDEueCpiMih0KSArIGNwMi54KmIzKHQpICsgcDIueCpiNCh0KTtcclxuICAgIHBvcy55ID0gcDEueSpiMSh0KSArIGNwMS55KmIyKHQpICsgY3AyLnkqYjModCkgKyBwMi55KmI0KHQpO1xyXG4gICAgcmV0dXJuIHBvcztcclxufTsiLCJ2YXIgZ2V0QmV6aWVyID0gcmVxdWlyZSgnLi9iZXppZXIuanMnKVxyXG5cclxudmFyIGJlemllckJveENhY2hlID0gW107XHJcblxyXG5mdW5jdGlvbiBnZXRCZXppZXJCb3gocDEsIGNwMSwgY3AyLCBwMikge1xyXG4gICAgdmFyIHBvaW50ID0gZ2V0QmV6aWVyKDAsIHAxLCBjcDEsIGNwMiwgcDIpO1xyXG4gICAgdmFyIG1pblggPSBwb2ludC54O1xyXG4gICAgdmFyIG1pblkgPSBwb2ludC55O1xyXG4gICAgdmFyIG1heFggPSBwb2ludC54O1xyXG4gICAgdmFyIG1heFkgPSBwb2ludC55O1xyXG5cclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDw9IDIwOyBpKyspIHtcclxuICAgICAgICB2YXIgdGVtcFBvaW50ID0gZ2V0QmV6aWVyKGkgKiAwLjA1LCBwMSwgY3AxLCBjcDIsIHAyKTtcclxuICAgICAgICBtaW5YID0gTWF0aC5taW4odGVtcFBvaW50LngsIG1pblgpO1xyXG4gICAgICAgIG1pblkgPSBNYXRoLm1pbih0ZW1wUG9pbnQueSwgbWluWSk7XHJcbiAgICAgICAgbWF4WCA9IE1hdGgubWF4KHRlbXBQb2ludC54LCBtYXhYKTtcclxuICAgICAgICBtYXhZID0gTWF0aC5tYXgodGVtcFBvaW50LnksIG1heFkpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgbWluWDogbWluWCxcclxuICAgICAgICBtaW5ZOiBtaW5ZLFxyXG4gICAgICAgIG1heFg6IG1heFgsXHJcbiAgICAgICAgbWF4WTogbWF4WSxcclxuICAgICAgICB3aWR0aDogbWF4WCAtIG1pblgsXHJcbiAgICAgICAgaGVpZ2h0OiBtYXhZIC0gbWluWVxyXG4gICAgfTtcclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24ocDEsIGNwMSwgY3AyLCBwMikge1xyXG4gICAgdmFyIGtleSA9IHAxLnggKyAnLicgKyBwMS55ICtcclxuICAgICAgICAnLicgKyBjcDEueCArICcuJyArIGNwMS55ICtcclxuICAgICAgICAnLicgKyBjcDIueCArICcuJyArIGNwMi55ICtcclxuICAgICAgICAnLicgKyBwMi54ICsgJy4nICsgcDIueTtcclxuXHJcbiAgICBpZiAodHlwZW9mIChiZXppZXJCb3hDYWNoZVtrZXldKSA9PSBcInVuZGVmaW5lZFwiKVxyXG4gICAgICAgIGJlemllckJveENhY2hlW2tleV0gPSBnZXRCZXppZXJCb3gocDEsIGNwMSwgY3AyLCBwMik7XHJcbiAgICByZXR1cm4gYmV6aWVyQm94Q2FjaGVba2V5XTtcclxufTsiLCJyZXF1aXJlKCcuL2Vhc2luZ3MuanMnKSgpOyAvLyBwYXRjaCBqcXVlcnkgd2l0aCBlYXNpbmdzXHJcblxyXG4vKiFcclxuICogVE9ETzogYWRkIGFib3V0XHJcbiAqIFxyXG4gKi9cclxuXHJcbihmdW5jdGlvbiAoJCkge1xyXG4gICAgaWYgKCEkLnRoZXRhKSB7XHJcbiAgICAgICAgJC50aGV0YSA9IG5ldyBPYmplY3QoKTtcclxuICAgIH07XHJcblxyXG4gICAgdmFyIGNvbXVsYXRpdmVfYW5pbWF0aW9uID0gcmVxdWlyZSgnLi9jb211bGF0aXZlX2FuaW1hdGlvbi5qcycpO1xyXG4gICAgdmFyIGZsdWlkX2xheW91dCA9IHJlcXVpcmUoJy4vZmx1aWRfbGF5b3V0LmpzJyk7XHJcbiAgICB2YXIgZWxlbWVudHNfc2l6ZV91cGRhdGVyID0gcmVxdWlyZSgnLi9lbGVtZW50c19zaXplX3VwZGF0ZXIuanMnKTtcclxuICAgIHZhciBpbnB1dF9jb250cm9sbGVyID0gcmVxdWlyZSgnLi9pbnB1dF9jb250cm9sbGVyLmpzJyk7XHJcbiAgICB2YXIgbW90aW9uX2NvbnRyb2xsZXIgPSByZXF1aXJlKCcuL21vdGlvbl9jb250cm9sbGVyLmpzJyk7XHJcbiAgICB2YXIgcm90YXRpb25fbG9naWNfY29udHJvbGxlciA9IHJlcXVpcmUoJy4vcm90YXRpb25fbG9naWNfY29udHJvbGxlci5qcycpO1xyXG4gICAgdmFyIGVuZGxlc3Nfcm90YXRpb25fbG9naWNfY29udHJvbGxlciA9IHJlcXVpcmUoJy4vZW5kbGVzc19yb3RhdGlvbl9sb2dpY19jb250cm9sbGVyLmpzJyk7XHJcbiAgICB2YXIgZmFsbGJhY2tfcm90YXRpb25fbG9naWNfY29udHJvbGxlciA9IHJlcXVpcmUoJy4vZmFsbGJhY2tfcm90YXRpb25fbG9naWNfY29udHJvbGxlci5qcycpO1xyXG4gICAgdmFyIGF1dG9fcm90YXRvciA9IHJlcXVpcmUoJy4vYXV0b19yb3RhdG9yLmpzJyk7XHJcbiAgICB2YXIgc2l6ZSA9IHJlcXVpcmUoJy4vc2l6ZS5qcycpO1xyXG4gICAgdmFyIHV0aWxzID0gcmVxdWlyZSgnLi91dGlscy5qcycpO1xyXG5cclxuICAgIHZhciBwYXRoX2FyY2hpbWVkZXNfc3BpcmFsID0gcmVxdWlyZSgnLi9wYXRocy9wYXRoX2FyY2hpbWVkZXNfc3BpcmFsLmpzJyk7XHJcbiAgICB2YXIgcGF0aF9jdWJpYyA9IHJlcXVpcmUoJy4vcGF0aHMvcGF0aF9jdWJpYy5qcycpO1xyXG4gICAgdmFyIHBhdGhfY3ViaWNfYmV6aWVyID0gcmVxdWlyZSgnLi9wYXRocy9wYXRoX2N1YmljX2Jlemllci5qcycpO1xyXG4gICAgdmFyIHBhdGhfZWxsaXBzZSA9IHJlcXVpcmUoJy4vcGF0aHMvcGF0aF9lbGxpcHNlLmpzJyk7XHJcbiAgICB2YXIgcGF0aF9saW5lID0gcmVxdWlyZSgnLi9wYXRocy9wYXRoX2xpbmUuanMnKTtcclxuICAgIHZhciBwYXRoX3BhcmFib2xhID0gcmVxdWlyZSgnLi9wYXRocy9wYXRoX3BhcmFib2xhLmpzJyk7XHJcbiAgICBcclxuICAgIHZhciBlZmZlY3RfYWxsaWduX3RvX3BhdGggPSByZXF1aXJlKCcuL1Zpc3VhbEVmZmVjdHMvZWZmZWN0X2FsbGlnbl90b19wYXRoLmpzJyk7XHJcbiAgICB2YXIgZWZmZWN0X2ZhZGVfYXdheSA9IHJlcXVpcmUoJy4vVmlzdWFsRWZmZWN0cy9lZmZlY3RfZmFkZV9hd2F5LmpzJyk7XHJcbiAgICB2YXIgZWZmZWN0X3BvcF9vdXRfc2VsZWN0ZWQgPSByZXF1aXJlKCcuL1Zpc3VhbEVmZmVjdHMvZWZmZWN0X3BvcF9vdXRfc2VsZWN0ZWQuanMnKTtcclxuICAgIHZhciBlZmZlY3Rfcm90YXRpb24gPSByZXF1aXJlKCcuL1Zpc3VhbEVmZmVjdHMvZWZmZWN0X3JvdGF0aW9uLmpzJyk7XHJcbiAgICB2YXIgZWZmZWN0X3NoYWRvdyA9IHJlcXVpcmUoJy4vVmlzdWFsRWZmZWN0cy9lZmZlY3Rfc2hhZG93LmpzJyk7XHJcbiAgICB2YXIgZWZmZWN0X3NpemVfYWRqdXN0bWVudCA9IHJlcXVpcmUoJy4vVmlzdWFsRWZmZWN0cy9lZmZlY3Rfc2l6ZV9hZGp1c3RtZW50LmpzJyk7XHJcbiAgICB2YXIgZWZmZWN0X3JlZmxlY3Rpb24gPSByZXF1aXJlKCcuL1Zpc3VhbEVmZmVjdHMvZWZmZWN0X3JlZmxlY3Rpb24uanMnKTtcclxuICAgIFxyXG4gICAgdmFyIHZlcnNpb24gPSAnMS42LjQnO1xyXG4gICAgdmFyIGRlZmF1bHRPcHRpb25zID0ge1xyXG4gICAgICAgIGZpbHRlcjogXCJkaXZcIixcclxuICAgICAgICBzZWxlY3RlZEluZGV4OiAwLFxyXG4gICAgICAgIGRpc3RhbmNlOiA3MCxcclxuICAgICAgICBtb2RlM0Q6ICd6JyxcclxuICAgICAgICBzY2FsZVg6IDEsXHJcbiAgICAgICAgc2NhbGVZOiAxLFxyXG4gICAgICAgIHNjYWxlWjogMSxcclxuICAgICAgICBwZXJzcGVjdGl2ZTogMTAwMCxcclxuICAgICAgICBudW1iZXJPZkVsZW1lbnRzVG9EaXNwbGF5UmlnaHQ6IG51bGwsXHJcbiAgICAgICAgbnVtYmVyT2ZFbGVtZW50c1RvRGlzcGxheUxlZnQ6IG51bGwsXHJcbiAgICAgICAgc2Vuc2l0aXZpdHk6IDEsXHJcbiAgICAgICAgdmVydGljYWxSb3RhdGlvbjogZmFsc2UsXHJcbiAgICAgICAgbWluS2V5RG93bkZyZXF1ZW5jeTogMCxcclxuICAgICAgICByb3RhdGlvbkFuaW1hdGlvbkVhc2luZzogJ2Vhc2VPdXRDdWJpYycsXHJcbiAgICAgICAgcm90YXRpb25BbmltYXRpb25EdXJhdGlvbjogNTAwLFxyXG4gICAgICAgIGluZXJ0aWFGcmljdGlvbjogMTAsXHJcbiAgICAgICAgaW5lcnRpYUhpZ2hGcmljdGlvbjogNTAsXHJcbiAgICAgICAgcGF0aDoge1xyXG4gICAgICAgICAgICB0eXBlOiBcInBhcmFib2xhXCIsXHJcbiAgICAgICAgICAgIHNldHRpbmdzOiB7fVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGVzaWduZWRGb3JXaWR0aDogbnVsbCxcclxuICAgICAgICBkZXNpZ25lZEZvckhlaWdodDogbnVsbCxcclxuICAgICAgICBlbmFibGVkOiB0cnVlLFxyXG4gICAgICAgIG1vdXNld2hlZWxFbmFibGVkOiB0cnVlLFxyXG4gICAgICAgIGtleWJvYXJkRW5hYmxlZDogdHJ1ZSxcclxuICAgICAgICBnZXN0dXJlc0VuYWJsZWQ6IHRydWUsXHJcblxyXG4gICAgICAgIGF1dG9yb3RhdGlvbjogZmFsc2UsXHJcbiAgICAgICAgYXV0b3JvdGF0aW9uRGlyZWN0aW9uOiAncmlnaHQnLCAvKiBsZWZ0LCByaWdodCAqL1xyXG4gICAgICAgIGF1dG9yb3RhdGlvblBhdXNlOiAwLFxyXG5cclxuICAgICAgICAvLyBlZmZlY3RzXHJcbiAgICAgICAgYWxsaWduRWxlbWVudHNXaXRoUGF0aDogZmFsc2UsXHJcbiAgICAgICAgYWxsaWduRWxlbWVudHNXaXRoUGF0aENvZWZmaWNpZW50OiAxLFxyXG5cclxuICAgICAgICBmYWRlQXdheTogZmFsc2UsXHJcbiAgICAgICAgZmFkZUF3YXlOdW1iZXJPZkNvbmZpZ3VyYWJsZUVsZW1lbnRzOiA1LFxyXG4gICAgICAgIGZhZGVBd2F5QmV6aWVyUG9pbnRzOiB7IHAxOiB7IHg6IDAsIHk6IDEwMCB9LCBwMjogeyB4OiA1MCwgeTogNTAgfSwgcDM6IHsgeDogNTAsIHk6IDUwIH0sIHA0OiB7IHg6IDEwMCwgeTogMCB9IH0sXHJcblxyXG4gICAgICAgIHJvdGF0aW9uOiBmYWxzZSxcclxuICAgICAgICByb3RhdGlvblZlY3Rvclg6IDAsXHJcbiAgICAgICAgcm90YXRpb25WZWN0b3JZOiAwLFxyXG4gICAgICAgIHJvdGF0aW9uVmVjdG9yWjogMCxcclxuICAgICAgICByb3RhdGlvbk51bWJlck9mQ29uZmlndXJhYmxlRWxlbWVudHM6IDUsXHJcbiAgICAgICAgcm90YXRpb25CZXppZXJQb2ludHM6IHsgcDE6IHsgeDogMCwgeTogMCB9LCBwMjogeyB4OiA1MCwgeTogMCB9LCBwMzogeyB4OiA1MCwgeTogMCB9LCBwNDogeyB4OiAxMDAsIHk6IDAgfSB9LFxyXG4gICAgICAgIHJvdGF0aW9uSW52ZXJ0Rm9yTmVnYXRpdmU6IGZhbHNlLFxyXG5cclxuICAgICAgICBzaXplQWRqdXN0bWVudDogZmFsc2UsXHJcbiAgICAgICAgc2l6ZUFkanVzdG1lbnROdW1iZXJPZkNvbmZpZ3VyYWJsZUVsZW1lbnRzOiA1LFxyXG4gICAgICAgIHNpemVBZGp1c3RtZW50QmV6aWVyUG9pbnRzOiB7IHAxOiB7IHg6IDAsIHk6IDEwMCB9LCBwMjogeyB4OiA1MCwgeTogMTAwIH0sIHAzOiB7IHg6IDUwLCB5OiAxMDAgfSwgcDQ6IHsgeDogMTAwLCB5OiAxMDAgfSB9LFxyXG5cclxuICAgICAgICBzaGFkb3c6IGZhbHNlLFxyXG4gICAgICAgIHNoYWRvd0JsdXJSYWRpdXM6IDEwMCxcclxuICAgICAgICBzaGFkb3dTcHJlYWRSYWRpdXM6IDAsXHJcblxyXG4gICAgICAgIHBvcG91dFNlbGVjdGVkOiBmYWxzZSxcclxuICAgICAgICBwb3BvdXRTZWxlY3RlZFNoaWZ0WDogMCxcclxuICAgICAgICBwb3BvdXRTZWxlY3RlZFNoaWZ0WTogMCxcclxuICAgICAgICBwb3BvdXRTZWxlY3RlZFNoaWZ0WjogMCxcclxuXHJcbiAgICAgICAgcmVmbGVjdGlvbjogZmFsc2UsXHJcbiAgICAgICAgcmVmbGVjdGlvbkJlbG93OiAwLFxyXG4gICAgICAgIHJlZmxlY3Rpb25IZWlnaHQ6IDAuMyxcclxuXHJcbiAgICAgICAgZmFsbGJhY2s6ICdhdXRvJywgLy8gYXV0bywgYWx3YXlzLCBuZXZlclxyXG4gICAgICAgIGRpc3RhbmNlSW5GYWxsYmFja01vZGU6IDIwMFxyXG4gICAgfTtcclxuXHJcbiAgICAkLnRoZXRhLmNhcm91c2VsID0gZnVuY3Rpb24gKGRvbUVsZW1lbnQsIG9wdGlvbnMpIHtcclxuXHJcbiAgICAgICAgdmFyIGNhcm91c2VsID0gdGhpcztcclxuICAgICAgICBjYXJvdXNlbC4kZWxlbWVudCA9ICQoZG9tRWxlbWVudCk7XHJcbiAgICAgICAgY2Fyb3VzZWwuJGVsZW1lbnQuZGF0YShcInRoZXRhLmNhcm91c2VsXCIsIGNhcm91c2VsKTtcclxuICAgICAgICBjYXJvdXNlbC4kZWxlbWVudC5hZGRDbGFzcygndGhldGEtY2Fyb3VzZWwnKTtcclxuXHJcbiAgICAgICAgY2Fyb3VzZWwuX2NyZWF0ZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zID0gJC5leHRlbmQodHJ1ZSwge30sICQudGhldGEuY2Fyb3VzZWwuZGVmYXVsdE9wdGlvbnMsIG9wdGlvbnMpO1xyXG5cclxuICAgICAgICAgICAgLy8gcHJlcGFyZSBjb250YWluZXJcclxuICAgICAgICAgICAgdmFyIGNvbnRhaW5lclNpemUgPSBuZXcgc2l6ZSh0aGlzLndpZGdldCgpLndpZHRoKCksIHRoaXMud2lkZ2V0KCkuaGVpZ2h0KCkpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5jb250YWluZXIgPSAkKCc8ZGl2IGNsYXNzPVwidGhldGEtY2Fyb3VzZWwtaW5uZXItY29udGFpbmVyXCI+PC9kaXY+Jyk7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGFpbmVyLmFwcGVuZFRvKHRoaXMud2lkZ2V0KCkpO1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRhaW5lci5jc3Moe1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IGNvbnRhaW5lclNpemUud2lkdGggKyAncHgnLFxyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBjb250YWluZXJTaXplLmhlaWdodCArICdweCdcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLndpZGdldCgpLmF0dHIoJ3RhYmluZGV4JywgMCkuY3NzKHsgb3V0bGluZTogJ25vbmUnLCBvdmVyZmxvdzogJ2hpZGRlbicgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGFpbmVyLmNzcyh7XHJcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm1TdHlsZTogJ3ByZXNlcnZlLTNkJyxcclxuICAgICAgICAgICAgICAgIG92ZXJmbG93OiAnaGlkZGVuJyxcclxuICAgICAgICAgICAgICAgIHBlcnNwZWN0aXZlOiB0aGlzLm9wdGlvbnMucGVyc3BlY3RpdmUgKyAncHgnLFxyXG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlM2QoMHB4LDBweCwgMTAwMDAwcHgpJ1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIGluaXQgZWxlbWVudHNcclxuICAgICAgICAgICAgdGhpcy51cGRhdGUoKTtcclxuXHJcbiAgICAgICAgICAgIC8vIHByZXBhcmUgd2lkZ2V0XHJcbiAgICAgICAgICAgIHRoaXMuZWZmZWN0cyA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLl9jcmVhdGVQYXRoKCk7XHJcbiAgICAgICAgICAgIHRoaXMuX2NyZWF0ZUVmZmVjdHMoKTtcclxuICAgICAgICAgICAgdGhpcy5fY3JlYXRlUm90YXRpb25Mb2dpY0NvbnRyb2xsZXIoKTtcclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50c1NpemVVcGRhdGVyID0gbmV3IGVsZW1lbnRzX3NpemVfdXBkYXRlcih0aGlzKTtcclxuICAgICAgICAgICAgdGhpcy5mbHVpZExheW91dCA9IG5ldyBmbHVpZF9sYXlvdXQodGhpcyk7XHJcbiAgICAgICAgICAgIHRoaXMuX2FsaWduRWxlbWVudHMoKTtcclxuICAgICAgICAgICAgdGhpcy5hbmltYXRpb24gPSBuZXcgY29tdWxhdGl2ZV9hbmltYXRpb24odGhpcyk7XHJcbiAgICAgICAgICAgIHRoaXMubW90aW9uQ29udHJvbGxlciA9IG5ldyBtb3Rpb25fY29udHJvbGxlcih0aGlzLCAkLnByb3h5KHRoaXMuX21vdGlvbkNvbnN1bWVyLCB0aGlzKSk7XHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXRDb250cm9sbGVyID0gbmV3IGlucHV0X2NvbnRyb2xsZXIodGhpcyk7XHJcbiAgICAgICAgICAgIHRoaXMuYXV0b1JvdGF0b3IgPSBuZXcgYXV0b19yb3RhdG9yKHRoaXMpO1xyXG5cclxuICAgICAgICAgICAgLy8gYXR0YWNoIGV2ZW50IGxpc3RlbmVyc1xyXG4gICAgICAgICAgICAkKHRoaXMuYW5pbWF0aW9uKS5vbignc3RlcCcsICQucHJveHkoZnVuY3Rpb24gKGUsIHNoaWZ0KSB7IHRoaXMuX2FsaWduRWxlbWVudHMoc2hpZnQpOyB9LCB0aGlzKSk7XHJcbiAgICAgICAgICAgICQodGhpcy5hbmltYXRpb24pLm9uKCdkb25lJywgJC5wcm94eShmdW5jdGlvbiAoZSwgdmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3JvdGF0aW9uQW5pbWF0aW9uQ29tcGxldGVkKHZhbHVlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3JhaXNlTW90aW9uRW5kKCk7XHJcbiAgICAgICAgICAgIH0sIHRoaXMpKTtcclxuICAgICAgICAgICAgJCh0aGlzLm1vdGlvbkNvbnRyb2xsZXIpLm9uKCdlbmQnLCAkLnByb3h5KGZ1bmN0aW9uIChlLCB2YWx1ZSkgeyB0aGlzLl9tb3Rpb25FbmQodmFsdWUpOyB9LCB0aGlzKSk7XHJcbiAgICAgICAgICAgICQodGhpcy5tb3Rpb25Db250cm9sbGVyKS5vbignc3RhcnQnLCAkLnByb3h5KHRoaXMuX3JhaXNlTW90aW9uU3RhcnQsIHRoaXMpKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuaW5pdGlhbGl6ZWQgPSB0cnVlO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNhcm91c2VsLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuZWZmZWN0cy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lZmZlY3RzW2ldLnJldmVydCgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5yb3RhdGlvbkxvZ2ljQ29udHJvbGxlciAhPSBudWxsKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5yb3RhdGlvbkxvZ2ljQ29udHJvbGxlci5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXRDb250cm9sbGVyLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgdGhpcy5mbHVpZExheW91dC5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudHNTaXplVXBkYXRlci5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgIHRoaXMuYXV0b1JvdGF0b3IuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuZWxlbWVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudHNbaV0uJGVsZW1lbnQub2ZmKCd0YXAnLCB0aGlzLm1vdmVUbyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnRzW2ldLiRlbGVtZW50Lm9mZihcImNsaWNrXCIsIHRoaXMubW92ZVRvKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLndpZGdldCgpLmRhdGEoJ3RoZXRhLmNhcm91c2VsJywgbnVsbCk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY2Fyb3VzZWwubW92ZUJhY2sgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnJvdGF0aW9uTG9naWNDb250cm9sbGVyLm1vdmVCYWNrKCk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY2Fyb3VzZWwubW92ZUZvcndhcmQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnJvdGF0aW9uTG9naWNDb250cm9sbGVyLm1vdmVGb3J3YXJkKCk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY2Fyb3VzZWwubW92ZVRvID0gZnVuY3Rpb24gKGluZGV4KSB7XHJcbiAgICAgICAgICAgIHRoaXMucm90YXRpb25Mb2dpY0NvbnRyb2xsZXIubW92ZVRvKGluZGV4KTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5pbnZhbGlkYXRlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuX2lzSW5Nb3Rpb24pXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9hbGlnbkVsZW1lbnRzKCk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY2Fyb3VzZWwudXBkYXRlID0gZnVuY3Rpb24gKCkge1xyXG5cclxuICAgICAgICAgICAgdmFyIGl0ZW1zVG9BZGQgPSB0aGlzLndpZGdldCgpLmNvbnRlbnRzKCkuZmlsdGVyKFxyXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub2RlVHlwZSA9PSA4IC8qd2UgbmVlZCBjb21tZW50cyBmb3IgQW5ndWxhckpTKi9cclxuICAgICAgICAgICAgICAgICAgICAgICAgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgISQodGhpcykuaGFzQ2xhc3MoJ3RoZXRhLWlnbm9yZScpXHJcbiAgICAgICAgICAgICAgICAgICAgKSAmJiAhJCh0aGlzKS5oYXNDbGFzcygndGhldGEtY2Fyb3VzZWwtaW5uZXItY29udGFpbmVyJyk7IC8vIGlnbm9yZSBpbnRlcm5hbCBjb250YWluZXJcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaXRlbXNUb0FkZC5hcHBlbmRUbyh0aGlzLmNvbnRhaW5lcik7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnRzID0gdGhpcy5jb250YWluZXIuY2hpbGRyZW4oKS5maWx0ZXIodGhpcy5vcHRpb25zLmZpbHRlcikubWFwKGZ1bmN0aW9uIChpLCBlKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgJGUgPSAkKGUpO1xyXG5cclxuICAgICAgICAgICAgICAgIHZhciBvcmRlciA9ICRlLmRhdGEoJ3RoZXRhLW9yZGVyJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCFvcmRlcilcclxuICAgICAgICAgICAgICAgICAgICBvcmRlciA9IGk7XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIHJldHVybiB7ICRlbGVtZW50OiAkZSwgZWxlbWVudDogZSwgb3JkZXI6IG9yZGVyIH07XHJcbiAgICAgICAgICAgIH0pLnRvQXJyYXkoKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudHMuc29ydChmdW5jdGlvbiAoZTEsIGUyKSB7IHJldHVybiBlMS5vcmRlciAtIGUyLm9yZGVyOyB9KTtcclxuXHJcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5lbGVtZW50cy5sZW5ndGg7IGkrKykge1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudHNbaV0uaW5kZXggPSBpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50c1tpXS5lbGVtZW50LmluZGV4ID0gaTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuZWxlbWVudHNbaV0uJGVsZW1lbnQuaGFzQ2xhc3MoJ3RoZXRhLWNhcm91c2VsLWVsZW1lbnQnKSkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnRzW2ldLiRlbGVtZW50LmFkZENsYXNzKCd0aGV0YS1jYXJvdXNlbC1lbGVtZW50Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50c1tpXS4kZWxlbWVudC5jc3MoeyBwb3NpdGlvbjogJ2Fic29sdXRlJyB9KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnRzW2ldLiRlbGVtZW50Lm9uKCd0YXAnLCAkLnByb3h5KHRoaXMubW92ZVRvLCB0aGlzLCBpKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50c1tpXS4kZWxlbWVudC5jbGljaygkLnByb3h5KGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuZW5hYmxlZCAmJiAhdGhpcy5vcHRpb25zLmF1dG9yb3RhdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tb3ZlVG8oZS5pbmRleCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LCB0aGlzLCB0aGlzLmVsZW1lbnRzW2ldLmVsZW1lbnQpKTtcclxuXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5zZWxlY3RlZEluZGV4ID0gTWF0aC5tYXgoTWF0aC5taW4odGhpcy5vcHRpb25zLnNlbGVjdGVkSW5kZXgsIHRoaXMuZWxlbWVudHMubGVuZ3RoIC0gMSksIDApO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuZWxlbWVudHMubGVuZ3RoID09IDApXHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCA9IC0xO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuaW5pdGlhbGl6ZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudHNTaXplVXBkYXRlci51cGRhdGUoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52YWxpZGF0ZSgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9hcHBseUN1cnJlbnRJdGVtU3R5bGUoKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5nZXRJc0luTW90aW9uID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5faXNJbk1vdGlvbjtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5nZXRJc0ZhbGxiYWNrTW9kZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMucm90YXRpb25Mb2dpY0NvbnRyb2xsZXIuaXNGYWxsYmFja01vZGUoKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC53aWRnZXQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLiRlbGVtZW50O1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNhcm91c2VsLl9zZXRPcHRpb24gPSBmdW5jdGlvbiAobmFtZSwgdmFsdWUpIHtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHV0aWxzLnNldE9iamVjdFByb3BlcnR5VmFsdWUoY2Fyb3VzZWwub3B0aW9ucywgbmFtZSwgdmFsdWUpO1xyXG5cclxuICAgICAgICAgICAgaWYgKG5hbWUgPT09ICdyb3RhdGlvbkFuaW1hdGlvbkR1cmF0aW9uJyB8fCBuYW1lID09PSAncm90YXRpb25BbmltYXRpb25FYXNpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBkb24ndCBuZWVkIHRvIGRvIHNvbWV0aGluZyBpZiB0aGVzZSBwcm9wZXJ0aWVzIGhhcyBiZWVuIGNoYW5nZWRcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKG5hbWUgPT09ICdmaWx0ZXInKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZSgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAobmFtZSA9PT0gJ3BlcnNwZWN0aXZlJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250YWluZXIuY3NzKHsgcGVyc3BlY3RpdmU6IHZhbHVlICsgJ3B4JyB9KTtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMubW9kZTNEID09ICdzY2FsZScpXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fYWxpZ25FbGVtZW50cygpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAobmFtZS5pbmRleE9mKCdwYXRoJykgPT0gMCB8fCBuYW1lID09PSAnZmFsbGJhY2snKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9jcmVhdGVQYXRoKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9jcmVhdGVSb3RhdGlvbkxvZ2ljQ29udHJvbGxlcigpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fYWxpZ25FbGVtZW50cygpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAobmFtZSA9PT0gXCJzZWxlY3RlZEluZGV4XCIgfHwgbmFtZSA9PT0gXCJkaXN0YW5jZVwiIHx8IG5hbWUgPT09IFwibW9kZTNEXCJcclxuICAgICAgICAgICAgICAgIHx8IG5hbWUgPT09IFwibnVtYmVyT2ZFbGVtZW50c1RvRGlzcGxheVJpZ2h0XCIgfHwgbmFtZSA9PT0gXCJudW1iZXJPZkVsZW1lbnRzVG9EaXNwbGF5TGVmdFwiXHJcbiAgICAgICAgICAgICAgICB8fCBuYW1lID09PSBcInNjYWxlWFwiIHx8IG5hbWUgPT09IFwic2NhbGVZXCIgfHwgbmFtZSA9PT0gXCJzY2FsZVpcIlxyXG4gICAgICAgICAgICAgICAgfHwgbmFtZSA9PT0gXCJhbGxpZ25FbGVtZW50c1dpdGhQYXRoQ29lZmZpY2llbnRcIlxyXG4gICAgICAgICAgICAgICAgfHwgbmFtZSA9PT0gXCJmYWRlQXdheUJlemllclBvaW50c1wiIHx8IG5hbWUgPT09IFwiZmFkZUF3YXlOdW1iZXJPZkNvbmZpZ3VyYWJsZUVsZW1lbnRzXCJcclxuICAgICAgICAgICAgICAgIHx8IG5hbWUgPT09IFwicm90YXRpb25CZXppZXJQb2ludHNcIiB8fCBuYW1lID09PSBcInJvdGF0aW9uTnVtYmVyT2ZDb25maWd1cmFibGVFbGVtZW50c1wiIHx8IG5hbWUgPT09IFwicm90YXRpb25JbnZlcnRGb3JOZWdhdGl2ZVwiXHJcbiAgICAgICAgICAgICAgICB8fCBuYW1lID09PSBcInJvdGF0aW9uVmVjdG9yWFwiIHx8IG5hbWUgPT09IFwicm90YXRpb25WZWN0b3JZXCIgfHwgbmFtZSA9PT0gXCJyb3RhdGlvblZlY3RvclpcIlxyXG4gICAgICAgICAgICAgICAgfHwgbmFtZSA9PT0gXCJzaXplQWRqdXN0bWVudE51bWJlck9mQ29uZmlndXJhYmxlRWxlbWVudHNcIiB8fCBuYW1lID09PSBcInNpemVBZGp1c3RtZW50QmV6aWVyUG9pbnRzXCJcclxuICAgICAgICAgICAgICAgIHx8IG5hbWUgPT09IFwic2hhZG93Qmx1clJhZGl1c1wiIHx8IG5hbWUgPT09IFwic2hhZG93U3ByZWFkUmFkaXVzXCJcclxuICAgICAgICAgICAgICAgIHx8IG5hbWUgPT09IFwicG9wb3V0U2VsZWN0ZWRTaGlmdFhcIiB8fCBuYW1lID09PSBcInBvcG91dFNlbGVjdGVkU2hpZnRZXCIgfHwgbmFtZSA9PT0gXCJwb3BvdXRTZWxlY3RlZFNoaWZ0WlwiXHJcbiAgICAgICAgICAgICAgICB8fCBuYW1lID09PSBcImRpc3RhbmNlSW5GYWxsYmFja01vZGVcIlxyXG4gICAgICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9hbGlnbkVsZW1lbnRzKCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChuYW1lLmluZGV4T2YoJ2F1dG9yb3RhdGlvbicpICE9IC0xKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmF1dG9Sb3RhdG9yLmFwcGx5U2V0dGluZ3MoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKG5hbWUuaW5kZXhPZignYWxsaWduRWxlbWVudHNXaXRoUGF0aCcpICE9IC0xIHx8IG5hbWUuaW5kZXhPZignZmFkZUF3YXknKSAhPSAtMSB8fCBuYW1lLmluZGV4T2YoJ3JvdGF0aW9uJykgIT0gLTFcclxuICAgICAgICAgICAgICAgIHx8IG5hbWUuaW5kZXhPZignc2l6ZUFkanVzdG1lbnQnKSAhPSAtMSB8fCBuYW1lLmluZGV4T2YoJ3NoYWRvdycpICE9IC0xIHx8IG5hbWUuaW5kZXhPZigncG9wb3V0U2VsZWN0ZWQnKSAhPSAtMVxyXG4gICAgICAgICAgICAgICAgfHwgbmFtZS5pbmRleE9mKCdyZWZsZWN0aW9uJykgIT0gLTEpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2NyZWF0ZUVmZmVjdHMoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2FsaWduRWxlbWVudHMoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKG5hbWUgPT09ICdzZWxlY3RlZEluZGV4Jykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fYXBwbHlDdXJyZW50SXRlbVN0eWxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5fY3JlYXRlUm90YXRpb25Mb2dpY0NvbnRyb2xsZXIgPSBmdW5jdGlvbigpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5yb3RhdGlvbkxvZ2ljQ29udHJvbGxlciAhPSBudWxsKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5yb3RhdGlvbkxvZ2ljQ29udHJvbGxlci5kZXN0cm95KCk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5wYXRoLmlzRW5kbGVzcygpKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5yb3RhdGlvbkxvZ2ljQ29udHJvbGxlciA9IG5ldyBlbmRsZXNzX3JvdGF0aW9uX2xvZ2ljX2NvbnRyb2xsZXIodGhpcyk7XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIHRoaXMucm90YXRpb25Mb2dpY0NvbnRyb2xsZXIgPSBuZXcgcm90YXRpb25fbG9naWNfY29udHJvbGxlcih0aGlzKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuZmFsbGJhY2sgPT0gJ2Fsd2F5cycgfHwgKHRoaXMub3B0aW9ucy5mYWxsYmFjayA9PSAnYXV0bycgJiYgZmFsbGJhY2tfcm90YXRpb25fbG9naWNfY29udHJvbGxlci5mYWxsYmFjaygpKSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yb3RhdGlvbkxvZ2ljQ29udHJvbGxlciA9IG5ldyBmYWxsYmFja19yb3RhdGlvbl9sb2dpY19jb250cm9sbGVyKHRoaXMsIHRoaXMucm90YXRpb25Mb2dpY0NvbnRyb2xsZXIpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5hdXRvUm90YXRvcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hdXRvUm90YXRvci5hcHBseVNldHRpbmdzKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5fY3JlYXRlRWZmZWN0cyA9IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5lZmZlY3RzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVmZmVjdHNbaV0ucmV2ZXJ0KCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuZWZmZWN0cyA9IFtdO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5hbGxpZ25FbGVtZW50c1dpdGhQYXRoKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5lZmZlY3RzLnB1c2gobmV3IGVmZmVjdF9hbGxpZ25fdG9fcGF0aCh0aGlzLCB7fSkpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5mYWRlQXdheSlcclxuICAgICAgICAgICAgICAgIHRoaXMuZWZmZWN0cy5wdXNoKG5ldyBlZmZlY3RfZmFkZV9hd2F5KHRoaXMsIHt9KSk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnJvdGF0aW9uKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5lZmZlY3RzLnB1c2gobmV3IGVmZmVjdF9yb3RhdGlvbih0aGlzLCB7fSkpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5zaXplQWRqdXN0bWVudClcclxuICAgICAgICAgICAgICAgIHRoaXMuZWZmZWN0cy5wdXNoKG5ldyBlZmZlY3Rfc2l6ZV9hZGp1c3RtZW50KHRoaXMsIHt9KSk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnNoYWRvdylcclxuICAgICAgICAgICAgICAgIHRoaXMuZWZmZWN0cy5wdXNoKG5ldyBlZmZlY3Rfc2hhZG93KHRoaXMsIHt9KSk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnBvcG91dFNlbGVjdGVkKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5lZmZlY3RzLnB1c2gobmV3IGVmZmVjdF9wb3Bfb3V0X3NlbGVjdGVkKHRoaXMsIHt9KSk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnJlZmxlY3Rpb24pXHJcbiAgICAgICAgICAgICAgICB0aGlzLmVmZmVjdHMucHVzaChuZXcgZWZmZWN0X3JlZmxlY3Rpb24odGhpcywge30pKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5fY3JlYXRlUGF0aCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIG5ld1BhdGggPSBudWxsO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5wYXRoLnR5cGUgPT0gXCJwYXJhYm9sYVwiKSB7XHJcbiAgICAgICAgICAgICAgICBuZXdQYXRoID0gbmV3IHBhdGhfcGFyYWJvbGEodGhpcywgdGhpcy5vcHRpb25zLnBhdGguc2V0dGluZ3MpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnBhdGgudHlwZSA9PSBcImxpbmVcIikge1xyXG4gICAgICAgICAgICAgICAgbmV3UGF0aCA9IG5ldyBwYXRoX2xpbmUodGhpcywgdGhpcy5vcHRpb25zLnBhdGguc2V0dGluZ3MpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnBhdGgudHlwZSA9PSBcImN1YmljXCIpIHtcclxuICAgICAgICAgICAgICAgIG5ld1BhdGggPSBuZXcgcGF0aF9jdWJpYyh0aGlzLCB0aGlzLm9wdGlvbnMucGF0aC5zZXR0aW5ncyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMucGF0aC50eXBlID09IFwiYXJjaGltZWRlc19zcGlyYWxcIikge1xyXG4gICAgICAgICAgICAgICAgbmV3UGF0aCA9IG5ldyBwYXRoX2FyY2hpbWVkZXNfc3BpcmFsKHRoaXMsIHRoaXMub3B0aW9ucy5wYXRoLnNldHRpbmdzKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5wYXRoLnR5cGUgPT0gXCJlbGxpcHNlXCIpIHtcclxuICAgICAgICAgICAgICAgIG5ld1BhdGggPSBuZXcgcGF0aF9lbGxpcHNlKHRoaXMsIHRoaXMub3B0aW9ucy5wYXRoLnNldHRpbmdzKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5wYXRoLnR5cGUgPT0gXCJjdWJpY19iZXppZXJcIikge1xyXG4gICAgICAgICAgICAgICAgbmV3UGF0aCA9IG5ldyBwYXRoX2N1YmljX2Jlemllcih0aGlzLCB0aGlzLm9wdGlvbnMucGF0aC5zZXR0aW5ncyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChuZXdQYXRoICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucGF0aCA9IG5ld1BhdGg7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMucGF0aC5zZXR0aW5ncyA9IHRoaXMucGF0aC5zZXR0aW5ncztcclxuICAgICAgICAgICAgfSBlbHNlXHJcbiAgICAgICAgICAgICAgICB0aHJvdyBcInBhdGggXCIgKyB0aGlzLm9wdGlvbnMucGF0aC50eXBlICsgXCIgaXMgbm90IHN1cHBvcnRlZC5cIjtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5fcmFpc2VDaGFuZ2VFdmVudCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdGhpcy53aWRnZXQoKS50cmlnZ2VyKFwiY2hhbmdlXCIsIHsgaW5kZXg6IHRoaXMub3B0aW9ucy5zZWxlY3RlZEluZGV4IH0pO1xyXG4gICAgICAgICAgICB0aGlzLl9hcHBseUN1cnJlbnRJdGVtU3R5bGUoKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5fYXBwbHlDdXJyZW50SXRlbVN0eWxlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuZWxlbWVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGlmIChpID09PSB0aGlzLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudHNbaV0uJGVsZW1lbnQuYWRkQ2xhc3MoJ3RoZXRhLWN1cnJlbnQtaXRlbScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50c1tpXS4kZWxlbWVudC5yZW1vdmVDbGFzcygndGhldGEtY3VycmVudC1pdGVtJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5fcmFpc2VNb3Rpb25TdGFydCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdGhpcy5faXNJbk1vdGlvbiA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMud2lkZ2V0KCkuYWRkQ2xhc3MoJ3RoZXRhLWluLW1vdGlvbicpO1xyXG4gICAgICAgICAgICB0aGlzLndpZGdldCgpLnRyaWdnZXIoXCJtb3Rpb25TdGFydFwiLCB7IGluZGV4OiB0aGlzLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCB9KTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5fcmFpc2VNb3Rpb25FbmQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXRDb250cm9sbGVyLm5vbkludGVycnVwdGlibGVNb2RlKGZhbHNlKTtcclxuICAgICAgICAgICAgdGhpcy53aWRnZXQoKS5yZW1vdmVDbGFzcygndGhldGEtaW4tbW90aW9uJyk7XHJcbiAgICAgICAgICAgIHRoaXMuX2lzSW5Nb3Rpb24gPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy53aWRnZXQoKS50cmlnZ2VyKFwibW90aW9uRW5kXCIsIHsgaW5kZXg6IHRoaXMub3B0aW9ucy5zZWxlY3RlZEluZGV4IH0pO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNhcm91c2VsLl9yb3RhdGlvbkFuaW1hdGlvbkNvbXBsZXRlZCA9IGZ1bmN0aW9uIChpbmRleCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnNlbGVjdGVkSW5kZXggIT0gaW5kZXgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMub3B0aW9ucy5zZWxlY3RlZEluZGV4ID0gaW5kZXg7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9yYWlzZUNoYW5nZUV2ZW50KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5fYWxpZ25FbGVtZW50cygpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNhcm91c2VsLl9tb3Rpb25Db25zdW1lciA9IGZ1bmN0aW9uIChkaXN0YW5jZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5yb3RhdGlvbkxvZ2ljQ29udHJvbGxlci5jb25zdW1lTW90aW9uKGRpc3RhbmNlKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5fbW90aW9uRW5kID0gZnVuY3Rpb24gKHJlbWFpbmluZ0Rpc3RhbmNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMucm90YXRpb25Mb2dpY0NvbnRyb2xsZXIuaGFuZGxlTW90aW9uRW5kKHJlbWFpbmluZ0Rpc3RhbmNlKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5fYWxpZ25FbGVtZW50cyA9IGZ1bmN0aW9uIChhbmltYXRpb25TaGlmdCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5yb3RhdGlvbkxvZ2ljQ29udHJvbGxlci5hbGlnbkVsZW1lbnRzKGFuaW1hdGlvblNoaWZ0KTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXJvdXNlbC5fZ2V0Q29udGFpbmVyU2l6ZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIGNvbnRhaW5lciA9ICQoJy50aGV0YS1jYXJvdXNlbC1pbm5lci1jb250YWluZXInLCB0aGlzLndpZGdldCgpKTtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBzaXplKGNvbnRhaW5lci53aWR0aCgpLCBjb250YWluZXIuaGVpZ2h0KCkpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNhcm91c2VsLl9jcmVhdGUoKTtcclxuICAgIH07XHJcblxyXG4gICAgJC50aGV0YS5jYXJvdXNlbC5kZWZhdWx0T3B0aW9ucyA9IGRlZmF1bHRPcHRpb25zO1xyXG4gICAgJC50aGV0YS5jYXJvdXNlbC52ZXJzaW9uID0gdmVyc2lvbjtcclxuXHJcbiAgICAkLmZuLnRoZXRhX2Nhcm91c2VsID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcclxuICAgICAgICB2YXIgY2FsbEFyZ3VtZW50cyA9IGFyZ3VtZW50cztcclxuXHJcbiAgICAgICAgdmFyIGhhc0NhbGxSZXMgPSBmYWxzZTtcclxuICAgICAgICB2YXIgY2FsbFJlcyA9IG51bGw7XHJcblxyXG4gICAgICAgIHZhciBlYWNoUmVzID0gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyICRlbCA9ICQodGhpcyk7XHJcbiAgICAgICAgICAgIHZhciBpbnN0YW5jZSA9ICRlbC5kYXRhKCd0aGV0YS5jYXJvdXNlbCcpO1xyXG5cclxuICAgICAgICAgICAgaWYgKGluc3RhbmNlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIG9wdGlvbnMgPT09ICdzdHJpbmcnKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgaW5zdGFuY2Vbb3B0aW9uc10gPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChjYWxsQXJndW1lbnRzLCAxKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzQ2FsbFJlcyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxSZXMgPSBpbnN0YW5jZVtvcHRpb25zXS5hcHBseShpbnN0YW5jZSwgYXJncyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAob3B0aW9ucyA9PSAnb3B0aW9uJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoY2FsbEFyZ3VtZW50cy5sZW5ndGggPT0gMikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzQ2FsbFJlcyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsUmVzID0gdXRpbHMuZ2V0T2JqZWN0UHJvcGVydHlWYWx1ZShpbnN0YW5jZS5vcHRpb25zLCBjYWxsQXJndW1lbnRzWzFdKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNhbGxBcmd1bWVudHMubGVuZ3RoID09IDMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGluc3RhbmNlLl9zZXRPcHRpb24oY2FsbEFyZ3VtZW50c1sxXSwgY2FsbEFyZ3VtZW50c1syXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBjbG9uZSA9ICQuZXh0ZW5kKHRydWUsIHt9LCBvcHRpb25zKTtcclxuICAgICAgICAgICAgICAgICAgICAkLmVhY2goY2xvbmUsICQucHJveHkoJGVsLmRhdGEoJ3RoZXRhLmNhcm91c2VsJykuX3NldE9wdGlvbiwgJGVsLmRhdGEoJ3RoZXRhLmNhcm91c2VsJykpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIFxyXG4gICAgICAgICAgICAgICAgKG5ldyAkLnRoZXRhLmNhcm91c2VsKHRoaXMsIG9wdGlvbnMpKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKCFoYXNDYWxsUmVzKVxyXG4gICAgICAgICAgICByZXR1cm4gZWFjaFJlcztcclxuICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgIHJldHVybiBjYWxsUmVzO1xyXG4gICAgfTtcclxuXHJcbn0pKGpRdWVyeSk7IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihjYXJvdXNlbCkge1xyXG4gICAgdGhpcy5jYXJvdXNlbCA9IGNhcm91c2VsO1xyXG5cclxuICAgIHRoaXMuaXNJblByb2dyZXNzID0gZmFsc2U7XHJcbiAgICB0aGlzLmluUHJvZ3Jlc3NBbmltYXRpb25UYXJnZXQgPSBudWxsO1xyXG4gICAgdGhpcy5xdWV1ZSA9IFtdO1xyXG4gICAgdGhpcy5kaXN0YW5jZSA9IFtdO1xyXG4gICAgdGhpcy5jdXJyZW50RWxlbWVudCA9IG51bGw7XHJcblxyXG4gICAgdGhpcy5jbGVhclF1ZXVlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMucXVldWUgPSBbXTtcclxuICAgICAgICB0aGlzLmRpc3RhbmNlID0gW107XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuYW5pbWF0ZSA9IGZ1bmN0aW9uIChmcm9tLCB0bywgdGFyZ2V0VmFsdWUsIGVhc2luZywgZHVyYXRpb24pIHtcclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiAoZWFzaW5nKSA9PSBcInVuZGVmaW5lZFwiKVxyXG4gICAgICAgICAgICBlYXNpbmcgPSBudWxsO1xyXG4gICAgICAgIGlmICh0eXBlb2YgKGR1cmF0aW9uKSA9PSBcInVuZGVmaW5lZFwiKVxyXG4gICAgICAgICAgICBkdXJhdGlvbiA9IG51bGw7XHJcblxyXG4gICAgICAgIHRoaXMuYWRkRGlzdGFuY2UoTWF0aC5hYnModG8gLSBmcm9tKSk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnF1ZXVlLmxlbmd0aCA+IDUpXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuXHJcbiAgICAgICAgdGhpcy5xdWV1ZS5wdXNoKHsgZnJvbTogZnJvbSwgdG86IHRvLCB0YXJnZXRWYWx1ZTogdGFyZ2V0VmFsdWUsIGVhc2luZzogZWFzaW5nLCBkdXJhdGlvbjogZHVyYXRpb24gfSk7XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5pc0luUHJvZ3Jlc3MpIHtcclxuICAgICAgICAgICAgdGhpcy5wZWVrRnJvbVF1ZXVlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmNvbXBsZXRlQ3VycmVudEltbWVkaWF0ZWx5ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRFbGVtZW50ICE9IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50RWxlbWVudC5zdG9wKHRydWUsIHRydWUpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5wZWVrRnJvbVF1ZXVlID0gZnVuY3Rpb24gKCkge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5xdWV1ZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHZhciBlbGVtZW50ID0gdGhpcy5xdWV1ZVswXTtcclxuICAgICAgICAgICAgdGhpcy5xdWV1ZSA9IHRoaXMucXVldWUuc2xpY2UoMSk7XHJcbiAgICAgICAgICAgIHRoaXMuaW5Qcm9ncmVzc0FuaW1hdGlvblRhcmdldCA9IGVsZW1lbnQudGFyZ2V0VmFsdWU7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudEVsZW1lbnQgPSAkKGVsZW1lbnQpO1xyXG4gICAgICAgICAgICB2YXIgc3RlcERpc3QgPSBNYXRoLmFicyhlbGVtZW50LmZyb20gLSBlbGVtZW50LnRvKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBlYXNpbmcgPSBlbGVtZW50LmVhc2luZyA9PSBudWxsID8gdGhpcy5nZXRFYXNpbmcoc3RlcERpc3QpIDogZWxlbWVudC5lYXNpbmc7XHJcbiAgICAgICAgICAgIHZhciBkdXJhdGlvbiA9IChlbGVtZW50LmR1cmF0aW9uID09IG51bGwgPyB0aGlzLmNhcm91c2VsLm9wdGlvbnMucm90YXRpb25BbmltYXRpb25EdXJhdGlvbiA6IGVsZW1lbnQuZHVyYXRpb24pICogdGhpcy5nZXREdXJhdGlvbkNvZWZmaWNpZW50KHN0ZXBEaXN0KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudEVsZW1lbnQuYW5pbWF0ZSh7IGZyb206IGVsZW1lbnQudG8gfSwge1xyXG4gICAgICAgICAgICAgICAgZWFzaW5nOiBlYXNpbmcsXHJcbiAgICAgICAgICAgICAgICBkdXJhdGlvbjogZHVyYXRpb24sXHJcbiAgICAgICAgICAgICAgICBzdGFydDogJC5wcm94eSh0aGlzLm9uU3RhcnQsIHRoaXMpLFxyXG4gICAgICAgICAgICAgICAgc3RlcDogJC5wcm94eSh0aGlzLm9uU3RlcCwgdGhpcyksXHJcbiAgICAgICAgICAgICAgICBkb25lOiAkLnByb3h5KHRoaXMub25Eb25lLCB0aGlzKSxcclxuICAgICAgICAgICAgICAgIGFsd2F5czogJC5wcm94eSh0aGlzLm9uQWx3YXlzLCB0aGlzKVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuZ2V0VGFyZ2V0VmFsdWUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucXVldWUubGVuZ3RoID4gMClcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMucXVldWVbdGhpcy5xdWV1ZS5sZW5ndGggLSAxXS50YXJnZXRWYWx1ZTtcclxuICAgICAgICByZXR1cm4gdGhpcy5pblByb2dyZXNzQW5pbWF0aW9uVGFyZ2V0O1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm9uU3RhcnQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy5pc0luUHJvZ3Jlc3MgPSB0cnVlO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm9uU3RlcCA9IGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICAkKHRoaXMpLnRyaWdnZXIoJ3N0ZXAnLCB2YWwpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm9uRG9uZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKHRoaXMpLnRyaWdnZXIoJ2RvbmUnLCB0aGlzLmluUHJvZ3Jlc3NBbmltYXRpb25UYXJnZXQpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm9uQWx3YXlzID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuaXNJblByb2dyZXNzID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5wZWVrRnJvbVF1ZXVlKCk7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50RWxlbWVudCA9IG51bGw7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuYWRkRGlzdGFuY2UgPSBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICB0aGlzLmRpc3RhbmNlLnB1c2goeyBkYXRlOiBuZXcgRGF0ZSgpLCB2YWx1ZTogdmFsdWUgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuZGlzdGFuY2UgPSAkKHRoaXMuZGlzdGFuY2UpLmZpbHRlcihmdW5jdGlvbiAoaSwgZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gKG5ldyBEYXRlKCkgLSBkLmRhdGUpIDwgNTAwMDtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5nZXRBY3R1YWxEaXN0YW5jZSA9IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICAgICAgdmFyIGRpc3RhbmNlID0gMDtcclxuICAgICAgICB2YXIgZGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmRpc3RhbmNlLmxlbmd0aDsgaSsrKSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgZCA9IHRoaXMuZGlzdGFuY2VbaV07XHJcblxyXG4gICAgICAgICAgICBpZiAoKGRhdGUgLSBkLmRhdGUpIDwgdGhpcy5jYXJvdXNlbC5vcHRpb25zLnJvdGF0aW9uQW5pbWF0aW9uRHVyYXRpb24pXHJcbiAgICAgICAgICAgICAgICBkaXN0YW5jZSArPSBkLnZhbHVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGRpc3RhbmNlO1xyXG4gICAgfTtcclxuXHJcbiAgICAvLyBhZGp1c3Qgcm90YXRpb24gZHVyYXRpb24sIGlmIHVzZXIgcXVpcWx5IHByZXNzIG5leHQgYnV0dG9uIHNldmVyYWwgdGltZXNcclxuICAgIHRoaXMuZ2V0RHVyYXRpb25Db2VmZmljaWVudCA9IGZ1bmN0aW9uIChvbmVTdGVwRGlzdCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNhcm91c2VsLm9wdGlvbnMuYXV0b3JvdGF0aW9uKSAvLyBhZGp1c3RtZW50IGlzIG5vdCByZXF1aXJlZCBmb3IgYXV0byByb3RhdGlvblxyXG4gICAgICAgICAgICByZXR1cm4gMTsgLy8gXHJcblxyXG4gICAgICAgIGlmICh0aGlzLmdldEFjdHVhbERpc3RhbmNlKCkgPT0gMClcclxuICAgICAgICAgICAgcmV0dXJuIDE7XHJcbiAgICAgICAgcmV0dXJuIDEgLyAodGhpcy5nZXRBY3R1YWxEaXN0YW5jZSgpIC8gb25lU3RlcERpc3QpO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmdldEVhc2luZyA9IGZ1bmN0aW9uIChvbmVTdGVwRGlzdCkge1xyXG4gICAgICAgIGlmICh0aGlzLmdldER1cmF0aW9uQ29lZmZpY2llbnQob25lU3RlcERpc3QpID4gMC40KVxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jYXJvdXNlbC5vcHRpb25zLnJvdGF0aW9uQW5pbWF0aW9uRWFzaW5nO1xyXG4gICAgICAgIGVsc2VcclxuICAgICAgICAgICAgcmV0dXJuIFwibGluZWFyXCI7XHJcbiAgICB9O1xyXG59OyIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKCkge1xyXG5cclxuICAgIC8vIGJhc2VkIG9uIGVhc2luZyBlcXVhdGlvbnMgZnJvbSBSb2JlcnQgUGVubmVyIChodHRwOi8vd3d3LnJvYmVydHBlbm5lci5jb20vZWFzaW5nKVxyXG5cclxuICAgIHZhciBiYXNlRWFzaW5ncyA9IHt9O1xyXG5cclxuICAgICQuZWFjaChbXCJRdWFkXCIsIFwiQ3ViaWNcIiwgXCJRdWFydFwiLCBcIlF1aW50XCIsIFwiRXhwb1wiXSwgZnVuY3Rpb24gKGksIG5hbWUpIHtcclxuICAgICAgICBiYXNlRWFzaW5nc1tuYW1lXSA9IGZ1bmN0aW9uIChwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBNYXRoLnBvdyhwLCBpICsgMik7XHJcbiAgICAgICAgfTtcclxuICAgIH0pO1xyXG5cclxuICAgICQuZXh0ZW5kKGJhc2VFYXNpbmdzLCB7XHJcbiAgICAgICAgU2luZTogZnVuY3Rpb24gKHApIHtcclxuICAgICAgICAgICAgcmV0dXJuIDEgLSBNYXRoLmNvcyhwICogTWF0aC5QSSAvIDIpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgQ2lyYzogZnVuY3Rpb24gKHApIHtcclxuICAgICAgICAgICAgcmV0dXJuIDEgLSBNYXRoLnNxcnQoMSAtIHAgKiBwKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIEVsYXN0aWM6IGZ1bmN0aW9uIChwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBwID09PSAwIHx8IHAgPT09IDEgPyBwIDpcclxuICAgICAgICAgICAgICAgIC1NYXRoLnBvdygyLCA4ICogKHAgLSAxKSkgKiBNYXRoLnNpbigoKHAgLSAxKSAqIDgwIC0gNy41KSAqIE1hdGguUEkgLyAxNSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBCYWNrOiBmdW5jdGlvbiAocCkge1xyXG4gICAgICAgICAgICByZXR1cm4gcCAqIHAgKiAoMyAqIHAgLSAyKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIEJvdW5jZTogZnVuY3Rpb24gKHApIHtcclxuICAgICAgICAgICAgdmFyIHBvdzIsXHJcbiAgICAgICAgICAgICAgICBib3VuY2UgPSA0O1xyXG5cclxuICAgICAgICAgICAgd2hpbGUgKHAgPCAoKHBvdzIgPSBNYXRoLnBvdygyLCAtLWJvdW5jZSkpIC0gMSkgLyAxMSkgeyB9XHJcbiAgICAgICAgICAgIHJldHVybiAxIC8gTWF0aC5wb3coNCwgMyAtIGJvdW5jZSkgLSA3LjU2MjUgKiBNYXRoLnBvdygocG93MiAqIDMgLSAyKSAvIDIyIC0gcCwgMik7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgdmFyIGFsbEVhc2luZ3MgPSB7fTtcclxuICAgICQuZWFjaChiYXNlRWFzaW5ncywgZnVuY3Rpb24gKG5hbWUsIGVhc2VJbikge1xyXG4gICAgICAgIGFsbEVhc2luZ3NbXCJlYXNlSW5cIiArIG5hbWVdID0gZWFzZUluO1xyXG4gICAgICAgIGFsbEVhc2luZ3NbXCJlYXNlT3V0XCIgKyBuYW1lXSA9IGZ1bmN0aW9uIChwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAxIC0gZWFzZUluKDEgLSBwKTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIGFsbEVhc2luZ3NbXCJlYXNlSW5PdXRcIiArIG5hbWVdID0gZnVuY3Rpb24gKHApIHtcclxuICAgICAgICAgICAgcmV0dXJuIHAgPCAwLjUgP1xyXG4gICAgICAgICAgICAgICAgZWFzZUluKHAgKiAyKSAvIDIgOlxyXG4gICAgICAgICAgICAgICAgMSAtIGVhc2VJbihwICogLTIgKyAyKSAvIDI7XHJcbiAgICAgICAgfTtcclxuICAgIH0pO1xyXG5cclxuICAgICQuZWFjaChhbGxFYXNpbmdzLCBmdW5jdGlvbiAobmFtZSwgZSkge1xyXG4gICAgICAgIGlmICh0eXBlb2YgKCQuZWFzaW5nW25hbWVdKSA9PSBcInVuZGVmaW5lZFwiKVxyXG4gICAgICAgICAgICAkLmVhc2luZ1tuYW1lXSA9IGU7XHJcbiAgICB9KTtcclxuXHJcbn07IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoY2Fyb3VzZWwpIHtcclxuXHJcbiAgICBleHRlbmQodGhpcyk7XHJcblxyXG4gICAgdGhpcy5jYXJvdXNlbCA9IGNhcm91c2VsO1xyXG4gICAgdGhpcy5pbnRlcnZhbCA9IHNldEludGVydmFsKCQucHJveHkodGhpcy51cGRhdGUsIHRoaXMpLCA1MDApO1xyXG5cclxuICAgIHRoaXMudXBkYXRlKCk7XHJcbn07XHJcblxyXG5mdW5jdGlvbiBleHRlbmQob2JqKSB7XHJcbiAgICBcclxuICAgIG9iai51cGRhdGUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGludmFsaWRhdGUgPSBmYWxzZTtcclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdmFyIGUgPSB0aGlzLmNhcm91c2VsLmVsZW1lbnRzW2ldO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgdmFyIG9sZFNpemUgPSBudWxsO1xyXG4gICAgICAgICAgICBpZiAoZS5zaXplKVxyXG4gICAgICAgICAgICAgICAgb2xkU2l6ZSA9IGUuc2l6ZTtcclxuXHJcbiAgICAgICAgICAgIGUuc2l6ZSA9IHtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogZS4kZWxlbWVudC5oZWlnaHQoKSxcclxuICAgICAgICAgICAgICAgIHdpZHRoOiBlLiRlbGVtZW50LndpZHRoKClcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKCFvbGRTaXplIHx8IG9sZFNpemUuaGVpZ2h0ICE9IGUuc2l6ZS5oZWlnaHQgfHwgb2xkU2l6ZS53aWR0aCAhPSBlLnNpemUud2lkdGgpXHJcbiAgICAgICAgICAgICAgICBpbnZhbGlkYXRlID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChpbnZhbGlkYXRlKVxyXG4gICAgICAgICAgICB0aGlzLmNhcm91c2VsLmludmFsaWRhdGUoKTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLmludGVydmFsKTtcclxuICAgIH07XHJcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChjYXJvdXNlbCkge1xyXG5cclxuICAgIHZhciByb3RhdGlvbl9sb2dpY19jb250cm9sbGVyID0gcmVxdWlyZSgnLi9yb3RhdGlvbl9sb2dpY19jb250cm9sbGVyLmpzJyk7XHJcbiAgICB0aGlzLmJhc2UgPSBuZXcgcm90YXRpb25fbG9naWNfY29udHJvbGxlcihjYXJvdXNlbCk7XHJcbiAgICAkLmV4dGVuZCh0aGlzLCB0aGlzLmJhc2UpO1xyXG5cclxuICAgIGV4dGVuZCh0aGlzKTtcclxufTtcclxuXHJcbmZ1bmN0aW9uIGV4dGVuZChvYmopIHtcclxuXHJcbiAgICBvYmoubW92ZVRvSW50ZXJuYWwgPSBmdW5jdGlvbiAoaW5kZXgpIHtcclxuICAgICAgICB2YXIgc3RyYWlnaHQgPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCAtIGluZGV4O1xyXG4gICAgICAgIHZhciByZXZlcnNlID0gMDtcclxuICAgICAgICB2YXIgcmV2ZXJzZVByZWZlcmFibGUgPSBmYWxzZTtcclxuICAgICAgICBpZiAoaW5kZXggPiB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCkge1xyXG4gICAgICAgICAgICByZXZlcnNlID0gdGhpcy5jYXJvdXNlbC5lbGVtZW50cy5sZW5ndGggLSBpbmRleCArIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zZWxlY3RlZEluZGV4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgcmV2ZXJzZSA9ICh0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aCAtIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zZWxlY3RlZEluZGV4ICsgaW5kZXgpICogLTE7XHJcbiAgICAgICAgICAgIHJldmVyc2VQcmVmZXJhYmxlID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHZhciBkaXN0YW5jZSA9IHN0cmFpZ2h0ICogdGhpcy5nZXRBY3R1YWxEaXN0YW5jZSgpO1xyXG4gICAgICAgIGlmIChNYXRoLmFicyhyZXZlcnNlKSA8IE1hdGguYWJzKHN0cmFpZ2h0KSB8fCAocmV2ZXJzZVByZWZlcmFibGUgJiYgTWF0aC5hYnMocmV2ZXJzZSkgPT0gTWF0aC5hYnMoc3RyYWlnaHQpKSlcclxuICAgICAgICAgICAgZGlzdGFuY2UgPSByZXZlcnNlICogdGhpcy5nZXRBY3R1YWxEaXN0YW5jZSgpO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuY2Fyb3VzZWwuaW5wdXRDb250cm9sbGVyLm5vbkludGVycnVwdGlibGVNb2RlKHRydWUpO1xyXG4gICAgICAgIHRoaXMuY2Fyb3VzZWwuX3JhaXNlTW90aW9uU3RhcnQoKTtcclxuXHJcbiAgICAgICAgdGhpcy5jYXJvdXNlbC5hbmltYXRpb24uYW5pbWF0ZSgwLCBkaXN0YW5jZSwgaW5kZXgsIE1hdGguYWJzKHJldmVyc2UpID09PSAxID8gbnVsbCA6IFwibGluZWFyXCIpO1xyXG4gICAgfTtcclxuXHJcbiAgICBvYmoubW92ZUJhY2sgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIHBlbmRpbmdUYXJnZXQgPSB0aGlzLmNhcm91c2VsLmFuaW1hdGlvbi5pc0luUHJvZ3Jlc3MgPyB0aGlzLmNhcm91c2VsLmFuaW1hdGlvbi5nZXRUYXJnZXRWYWx1ZSgpIDogdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXg7XHJcblxyXG4gICAgICAgIHBlbmRpbmdUYXJnZXQtLTtcclxuXHJcbiAgICAgICAgaWYgKHBlbmRpbmdUYXJnZXQgPCAwKVxyXG4gICAgICAgICAgICBwZW5kaW5nVGFyZ2V0ID0gdGhpcy5jYXJvdXNlbC5lbGVtZW50cy5sZW5ndGggKyBwZW5kaW5nVGFyZ2V0O1xyXG5cclxuICAgICAgICB0aGlzLmNhcm91c2VsLmlucHV0Q29udHJvbGxlci5ub25JbnRlcnJ1cHRpYmxlTW9kZSh0cnVlKTtcclxuICAgICAgICB0aGlzLmNhcm91c2VsLl9yYWlzZU1vdGlvblN0YXJ0KCk7XHJcbiAgICAgICAgdGhpcy5jYXJvdXNlbC5hbmltYXRpb24uYW5pbWF0ZSgwLCB0aGlzLmdldEFjdHVhbERpc3RhbmNlKCksIHBlbmRpbmdUYXJnZXQsIG51bGwpO1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfTtcclxuXHJcbiAgICBvYmoubW92ZUZvcndhcmQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIHBlbmRpbmdUYXJnZXQgPSB0aGlzLmNhcm91c2VsLmFuaW1hdGlvbi5pc0luUHJvZ3Jlc3MgPyB0aGlzLmNhcm91c2VsLmFuaW1hdGlvbi5nZXRUYXJnZXRWYWx1ZSgpIDogdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXg7XHJcblxyXG4gICAgICAgIHBlbmRpbmdUYXJnZXQrKztcclxuICAgICAgICBpZiAocGVuZGluZ1RhcmdldCA+PSB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aClcclxuICAgICAgICAgICAgcGVuZGluZ1RhcmdldCAtPSB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aDtcclxuXHJcbiAgICAgICAgdGhpcy5jYXJvdXNlbC5pbnB1dENvbnRyb2xsZXIubm9uSW50ZXJydXB0aWJsZU1vZGUodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5jYXJvdXNlbC5fcmFpc2VNb3Rpb25TdGFydCgpO1xyXG4gICAgICAgIHRoaXMuY2Fyb3VzZWwuYW5pbWF0aW9uLmFuaW1hdGUoMCwgLTEgKiB0aGlzLmdldEFjdHVhbERpc3RhbmNlKCksIHBlbmRpbmdUYXJnZXQsIG51bGwpO1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfTtcclxuXHJcbiAgICBvYmouY29uc3VtZU1vdGlvbiA9IGZ1bmN0aW9uIChkaXN0YW5jZSkge1xyXG4gICAgICAgIHZhciBoaWdoRnJpY3Rpb25SYW5nZSA9IHRoaXMuY2Fyb3VzZWwuX2FsaWduRWxlbWVudHMoZGlzdGFuY2UpO1xyXG5cclxuICAgICAgICB2YXIgc2Nyb2xsZWRFbGVtZW50cyA9IHBhcnNlSW50KGRpc3RhbmNlIC8gdGhpcy5nZXRBY3R1YWxEaXN0YW5jZSgpLCAxMCk7XHJcblxyXG4gICAgICAgIHZhciBwcmV2SW5kZXggPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleDtcclxuICAgICAgICB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCAtPSBzY3JvbGxlZEVsZW1lbnRzO1xyXG5cclxuICAgICAgICB2YXIgY29uc3VtZWREaXN0YW5jZSA9IHByZXZJbmRleCAtIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zZWxlY3RlZEluZGV4O1xyXG4gICAgICAgIGlmICh0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCA8IDApIHtcclxuICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXggPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCAlIHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoICsgdGhpcy5jYXJvdXNlbC5lbGVtZW50cy5sZW5ndGg7XHJcbiAgICAgICAgICAgIGNvbnN1bWVkRGlzdGFuY2UgPSB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aCAtIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zZWxlY3RlZEluZGV4ICsgcHJldkluZGV4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXggPj0gdGhpcy5jYXJvdXNlbC5lbGVtZW50cy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXggPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCAlIHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoO1xyXG4gICAgICAgICAgICBjb25zdW1lZERpc3RhbmNlID0gdGhpcy5jYXJvdXNlbC5lbGVtZW50cy5sZW5ndGggLSBwcmV2SW5kZXggKyB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleDtcclxuICAgICAgICAgICAgY29uc3VtZWREaXN0YW5jZSAqPSAtMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgaWYgKHByZXZJbmRleCAhPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleClcclxuICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5fcmFpc2VDaGFuZ2VFdmVudCgpO1xyXG5cclxuICAgICAgICByZXR1cm4geyBkaXN0YW5jZTogY29uc3VtZWREaXN0YW5jZSAqIHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKSwgaGlnaEZyaWN0aW9uUmFuZ2U6IGhpZ2hGcmljdGlvblJhbmdlIH07XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5oYW5kbGVNb3Rpb25FbmQgPSBmdW5jdGlvbiAocmVtYWluaW5nRGlzdGFuY2UpIHtcclxuICAgICAgICBpZiAocmVtYWluaW5nRGlzdGFuY2UgPT0gMClcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICB2YXIgdGFyZ2V0SW5kZXggPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleDtcclxuXHJcbiAgICAgICAgaWYgKE1hdGguYWJzKHJlbWFpbmluZ0Rpc3RhbmNlKSA+IHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKSAvIDIpIHtcclxuICAgICAgICAgICAgaWYgKHJlbWFpbmluZ0Rpc3RhbmNlIDwgMClcclxuICAgICAgICAgICAgICAgIHRhcmdldEluZGV4Kys7XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIHRhcmdldEluZGV4LS07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgcmV2ZXJzZSA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aCA9PSAwKVxyXG4gICAgICAgICAgICB0YXJnZXRJbmRleCA9IDA7XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIGlmICh0YXJnZXRJbmRleCA8IDApIHtcclxuICAgICAgICAgICAgICAgIHRhcmdldEluZGV4ID0gdGFyZ2V0SW5kZXggJSB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aCArIHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoO1xyXG4gICAgICAgICAgICAgICAgcmV2ZXJzZSA9ICdiYWNrJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGFyZ2V0SW5kZXggPj0gdGhpcy5jYXJvdXNlbC5lbGVtZW50cy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIHRhcmdldEluZGV4ID0gdGFyZ2V0SW5kZXggJSB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aDtcclxuICAgICAgICAgICAgICAgIHJldmVyc2UgPSAnZm9yd2FyZCc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHZhciB0YXJnZXREaXN0YW5jZSA9IDA7XHJcbiAgICAgICAgaWYgKCFyZXZlcnNlKSB7XHJcbiAgICAgICAgICAgIHRhcmdldERpc3RhbmNlID0gKHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zZWxlY3RlZEluZGV4IC0gdGFyZ2V0SW5kZXgpICogdGhpcy5nZXRBY3R1YWxEaXN0YW5jZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgaWYocmV2ZXJzZSA9PT0gJ2JhY2snKVxyXG4gICAgICAgICAgICAgICAgdGFyZ2V0RGlzdGFuY2UgPSAodGhpcy5jYXJvdXNlbC5lbGVtZW50cy5sZW5ndGggLSB0YXJnZXRJbmRleCArIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zZWxlY3RlZEluZGV4KSAqIHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKTtcclxuICAgICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICAgICAgdGFyZ2V0RGlzdGFuY2UgPSAodGhpcy5jYXJvdXNlbC5lbGVtZW50cy5sZW5ndGggLSB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCArIHRhcmdldEluZGV4KSAqIHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKSAqIC0xO1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICB2YXIgZHVyYXRpb24gPSBNYXRoLmFicyh0aGlzLmNhcm91c2VsLm9wdGlvbnMucm90YXRpb25BbmltYXRpb25EdXJhdGlvbiAqIChyZW1haW5pbmdEaXN0YW5jZSAvIHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKSkpO1xyXG4gICAgICAgIGR1cmF0aW9uID0gTWF0aC5taW4oZHVyYXRpb24sIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5yb3RhdGlvbkFuaW1hdGlvbkR1cmF0aW9uIC8gMik7XHJcblxyXG4gICAgICAgIHRoaXMuY2Fyb3VzZWwuYW5pbWF0aW9uLmFuaW1hdGUocmVtYWluaW5nRGlzdGFuY2UsIHRhcmdldERpc3RhbmNlLCB0YXJnZXRJbmRleCwgbnVsbCwgZHVyYXRpb24pO1xyXG4gICAgfTtcclxuXHJcbiAgICBvYmouYWxpZ25FbGVtZW50cyA9IGZ1bmN0aW9uIChhbmltYXRpb25TaGlmdCkge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5jYXJvdXNlbC5lbGVtZW50cy5sZW5ndGggPT0gMCB8fCB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCA8IDApXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuXHJcbiAgICAgICAgdGhpcy5jYXJvdXNlbC5jb250YWluZXJTaXplID0gdGhpcy5jYXJvdXNlbC5fZ2V0Q29udGFpbmVyU2l6ZSgpO1xyXG5cclxuICAgICAgICB2YXIgc2hpZnQgPSAwO1xyXG4gICAgICAgIGlmICh0eXBlb2YgKGFuaW1hdGlvblNoaWZ0KSAhPSBcInVuZGVmaW5lZFwiKVxyXG4gICAgICAgICAgICBzaGlmdCA9IGFuaW1hdGlvblNoaWZ0O1xyXG5cclxuICAgICAgICB2YXIgbG9jYXRpb24gPSB0aGlzLmdldFJvb3RWYWx1ZSgpO1xyXG4gICAgICAgIHZhciByYW5nZXMgPSB0aGlzLmdldEZhZGVSYW5nZXMobG9jYXRpb24pO1xyXG5cclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5lbGVtZW50c1tpXS5pc0VuZGxlc3NQcm9jZXNzZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHZhciB2aXNpYmlsaXR5SW5mbyA9IFtdO1xyXG5cclxuICAgICAgICBmb3IgKHZhciBpID0gbmV3IGVuZGxlc3NJdGVyYXRvcih0aGlzLmNhcm91c2VsLCB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCkgOyAhaS5pc0N5Y2xlQ29tcGxldGVkKCk7IGkubW92ZU5leHQoKSkge1xyXG4gICAgICAgICAgICB2YXIgdmlzaWJsZSA9IHRoaXMuc2V0RWxlbWVudFBvc2l0aW9uKHRoaXMuY2Fyb3VzZWwuZWxlbWVudHNbaS5nZXRDdXJyZW50SW5kZXgoKV0sIGxvY2F0aW9uICsgc2hpZnQsIHJhbmdlcyk7XHJcbiAgICAgICAgICAgIGlmICh2aXNpYmxlKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5lbGVtZW50c1tpLmdldEN1cnJlbnRJbmRleCgpXS5pc0VuZGxlc3NQcm9jZXNzZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB2aXNpYmlsaXR5SW5mb1tpLmdldEN1cnJlbnRJbmRleCgpXSA9IHZpc2libGU7XHJcbiAgICAgICAgICAgIGxvY2F0aW9uID0gdGhpcy5pbmNyZW1lbnRWYWx1ZShsb2NhdGlvbiwgdGhpcy5nZXRBY3R1YWxEaXN0YW5jZSgpKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxvY2F0aW9uID0gdGhpcy5nZXRSb290VmFsdWUoKTtcclxuXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IG5ldyBlbmRsZXNzSXRlcmF0b3IodGhpcy5jYXJvdXNlbCwgdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXggLSAxKSA7ICFpLmlzQ3ljbGVDb21wbGV0ZWQoKSA7IGkubW92ZVByZXYoKSkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5jYXJvdXNlbC5lbGVtZW50c1tpLmdldEN1cnJlbnRJbmRleCgpXS5pc0VuZGxlc3NQcm9jZXNzZWQpXHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGxvY2F0aW9uID0gdGhpcy5kZWNyZW1lbnRWYWx1ZShsb2NhdGlvbiwgdGhpcy5nZXRBY3R1YWxEaXN0YW5jZSgpKTtcclxuICAgICAgICAgICAgdmlzaWJpbGl0eUluZm9baS5nZXRDdXJyZW50SW5kZXgoKV0gPSB0aGlzLnNldEVsZW1lbnRQb3NpdGlvbih0aGlzLmNhcm91c2VsLmVsZW1lbnRzW2kuZ2V0Q3VycmVudEluZGV4KCldLCBsb2NhdGlvbiArIHNoaWZ0LCByYW5nZXMpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYmFzZS5zZXRFbGVtZW50VmlzaWJpbGl0eUludGVybmFsKCF2aXNpYmlsaXR5SW5mb1tpXSwgdGhpcy5jYXJvdXNlbC5lbGVtZW50c1tpXSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldFpJbmRleGVzKCk7XHJcblxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLnNldEVsZW1lbnRWaXNpYmlsaXR5SW50ZXJuYWwgPSBmdW5jdGlvbiAoaGlkZSkgeyB9O1xyXG59XHJcblxyXG52YXIgZW5kbGVzc0l0ZXJhdG9yID0gZnVuY3Rpb24oY2Fyb3VzZWwsIGN1cnJlbnRJbmRleCkge1xyXG4gICAgdGhpcy5jdXJyZW50SW5kZXggPSBjdXJyZW50SW5kZXg7XHJcbiAgICBpZiAodGhpcy5jdXJyZW50SW5kZXggPT0gLTEpXHJcbiAgICAgICAgdGhpcy5jdXJyZW50SW5kZXggPSBjYXJvdXNlbC5lbGVtZW50cy5sZW5ndGggLSAxO1xyXG4gICAgdGhpcy5pdGVyYXRpb25zID0gMDtcclxuICAgIHRoaXMuY2Fyb3VzZWwgPSBjYXJvdXNlbDtcclxuICAgIFxyXG4gICAgdGhpcy5tb3ZlTmV4dCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRJbmRleCsrO1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRJbmRleCA9PSB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aClcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50SW5kZXggPSAwO1xyXG4gICAgICAgIHRoaXMuaXRlcmF0aW9ucysrO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm1vdmVQcmV2ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuY3VycmVudEluZGV4LS07XHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudEluZGV4ID09IC0xKVxyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRJbmRleCA9IHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoIC0gMTtcclxuICAgICAgICB0aGlzLml0ZXJhdGlvbnMtLTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5pc0N5Y2xlQ29tcGxldGVkID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiBNYXRoLmFicyh0aGlzLml0ZXJhdGlvbnMpID49IHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmdldEN1cnJlbnRJbmRleCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50SW5kZXg7XHJcbiAgICB9O1xyXG59IiwidmFyIHRyYW5zZm9ybSA9IHJlcXVpcmUoJy4vdHJhbnNmb3JtLmpzJyk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChjYXJvdXNlbCwgYmFzZVJvdGF0b3IpIHtcclxuICAgIHRoaXMuYmFzZSA9IGJhc2VSb3RhdG9yO1xyXG4gICAgJC5leHRlbmQodGhpcywgdGhpcy5iYXNlKTtcclxuICAgIGV4dGVuZCh0aGlzKTtcclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzLmZhbGxiYWNrID0gZnVuY3Rpb24oKXtcclxuICAgIHJldHVybiAhaGFzM2QoKTtcclxufVxyXG5cclxuZnVuY3Rpb24gZXh0ZW5kKG9iaikge1xyXG5cclxuICAgIG9iai5pc0ZhbGxiYWNrTW9kZSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRydWU7IH07XHJcblxyXG4gICAgb2JqLmdldEFjdHVhbERpc3RhbmNlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNhcm91c2VsLm9wdGlvbnMuZGlzdGFuY2VJbkZhbGxiYWNrTW9kZTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLmdldFJvb3RWYWx1ZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiAwO1xyXG4gICAgfTtcclxuXHJcbiAgICBvYmouaW5jcmVtZW50VmFsdWUgPSBmdW5jdGlvbiAodmFsdWUsIGluY3JlbWVudCkge1xyXG4gICAgICAgIHJldHVybiB2YWx1ZSArIGluY3JlbWVudDtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLmRlY3JlbWVudFZhbHVlID0gZnVuY3Rpb24gKHZhbHVlLCBkZWNyZW1lbnQpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUgLSBkZWNyZW1lbnQ7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5taW5WYWx1ZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gLTEgKiB0aGlzLm1heFZhbHVlKCk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5tYXhWYWx1ZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gICggdGhpcy5jYXJvdXNlbC5jb250YWluZXJTaXplLndpZHRoICsgdGhpcy5nZXRBY3R1YWxEaXN0YW5jZSgpICkgLyAyO1xyXG4gICAgfTtcclxuXHJcbiAgICBvYmouc2V0RWxlbWVudFBvc2l0aW9uID0gZnVuY3Rpb24oZWxlbWVudCwgdmFsdWUpIHtcclxuXHJcbiAgICAgICAgdmFyIGxlZnQgPSB2YWx1ZSAtIGVsZW1lbnQuc2l6ZS53aWR0aCAvIDIgKyB0aGlzLmNhcm91c2VsLmNvbnRhaW5lclNpemUud2lkdGggLyAyO1xyXG4gICAgICAgIHZhciB0b3AgPSB0aGlzLmNhcm91c2VsLmNvbnRhaW5lclNpemUuaGVpZ2h0IC8gMiAtIGVsZW1lbnQuc2l6ZS5oZWlnaHQgLyAyO1xyXG5cclxuICAgICAgICBlbGVtZW50LiRlbGVtZW50LmNzcyh7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogJycsXHJcbiAgICAgICAgICAgIGxlZnQ6IGxlZnQgKyAncHgnLFxyXG4gICAgICAgICAgICB0b3A6IHRvcCArICdweCdcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdmFyIHZpc2libGUgPSB2YWx1ZSA+IHRoaXMubWluVmFsdWUoKSAmJiB2YWx1ZSA8IHRoaXMubWF4VmFsdWUoKVxyXG5cclxuICAgICAgICBpZiAodmlzaWJsZSlcclxuICAgICAgICAgICAgZWxlbWVudC4kZWxlbWVudC5zaG93KCk7XHJcbiAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICBlbGVtZW50LiRlbGVtZW50LmhpZGUoKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHZpc2libGU7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xyXG5cclxuICAgICAgICB0aGlzLmJhc2UuZGVzdHJveSgpO1xyXG5cclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5lbGVtZW50c1tpXS4kZWxlbWVudC5jc3Moe1xyXG4gICAgICAgICAgICAgICAgbGVmdDogJzAnLFxyXG4gICAgICAgICAgICAgICAgdG9wOiAnMCdcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxufTtcclxuXHJcbmZ1bmN0aW9uIGhhczNkKCkge1xyXG4gICAgaWYgKGRvY3VtZW50LmJvZHkgJiYgZG9jdW1lbnQuYm9keS5zdHlsZS5wZXJzcGVjdGl2ZSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICB2YXIgX3RlbXBEaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpLFxyXG4gICAgICAgIHN0eWxlID0gX3RlbXBEaXYuc3R5bGUsXHJcbiAgICAgICAgYSA9IFtcIldlYmtpdFwiLCBcIk1velwiLCBcIk9cIiwgXCJNc1wiLCBcIm1zXCJdLFxyXG4gICAgICAgIGkgPSBhLmxlbmd0aDtcclxuICAgIGlmIChfdGVtcERpdi5zdHlsZS5wZXJzcGVjdGl2ZSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICB3aGlsZSAoLS1pID4gLTEpIHtcclxuICAgICAgICBpZiAoc3R5bGVbYVtpXSArIFwiUGVyc3BlY3RpdmVcIl0gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbn1cclxuXHJcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oY2Fyb3VzZWwpIHtcclxuXHJcbiAgICBleHRlbmQodGhpcyk7XHJcblxyXG4gICAgdGhpcy5jYXJvdXNlbCA9IGNhcm91c2VsO1xyXG4gICAgdGhpcy5pbnRlcnZhbCA9IHNldEludGVydmFsKCQucHJveHkodGhpcy51cGRhdGUsIHRoaXMpLCAyMDApO1xyXG5cclxuICAgIHRoaXMudXBkYXRlKCk7XHJcblxyXG4gICAgdGhpcy5hcHBsaWVkV2lkdGggPSBudWxsO1xyXG4gICAgdGhpcy5hcHBsaWVkSGVpZ2h0ID0gbnVsbDtcclxuICAgIHRoaXMuYXBwbGllZFNjYWxlID0gbnVsbDtcclxufTtcclxuXHJcbmZ1bmN0aW9uIGV4dGVuZChvYmopIHtcclxuICAgIG9iai51cGRhdGUgPSBmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgICAgIHZhciB3aWR0aFRvQXBwbHkgPSB0aGlzLmNhcm91c2VsLndpZGdldCgpLndpZHRoKCk7XHJcbiAgICAgICAgdmFyIGhlaWdodFRvQXBwbHkgPSB0aGlzLmNhcm91c2VsLndpZGdldCgpLmhlaWdodCgpO1xyXG4gICAgICAgIHZhciBzY2FsZVRvQXBwbHkgPSBudWxsO1xyXG5cclxuICAgICAgICBpZiAod2lkdGhUb0FwcGx5ID09IDAgfHwgaGVpZ2h0VG9BcHBseSA9PSAwKVxyXG4gICAgICAgICAgICByZXR1cm47XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmNhcm91c2VsLm9wdGlvbnMuZGVzaWduZWRGb3JXaWR0aCAhPSBudWxsICYmIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5kZXNpZ25lZEZvckhlaWdodCAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgIHNjYWxlVG9BcHBseSA9IE1hdGgubWluKHdpZHRoVG9BcHBseSAvIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5kZXNpZ25lZEZvcldpZHRoLCBoZWlnaHRUb0FwcGx5IC8gdGhpcy5jYXJvdXNlbC5vcHRpb25zLmRlc2lnbmVkRm9ySGVpZ2h0KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh3aWR0aFRvQXBwbHkgIT0gdGhpcy5hcHBsaWVkV2lkdGggfHwgaGVpZ2h0VG9BcHBseSAhPSB0aGlzLmFwcGxpZWRIZWlnaHQgfHwgc2NhbGVUb0FwcGx5ICE9IHRoaXMuYXBwbGllZFNjYWxlKSB7XHJcbiAgICAgICAgICAgIHZhciBjb250YWluZXIgPSAkKCcudGhldGEtY2Fyb3VzZWwtaW5uZXItY29udGFpbmVyJywgdGhpcy5jYXJvdXNlbC53aWRnZXQoKSk7XHJcblxyXG4gICAgICAgICAgICBpZiAoc2NhbGVUb0FwcGx5ICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoVG9BcHBseSA9IHdpZHRoVG9BcHBseSAvIHNjYWxlVG9BcHBseTtcclxuICAgICAgICAgICAgICAgIGhlaWdodFRvQXBwbHkgPSBoZWlnaHRUb0FwcGx5IC8gc2NhbGVUb0FwcGx5O1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIGtlZXAgcHJvcG9ydGlvblxyXG4gICAgICAgICAgICAgICAgaWYgKHdpZHRoVG9BcHBseSAvIGhlaWdodFRvQXBwbHkgPCB0aGlzLmNhcm91c2VsLm9wdGlvbnMuZGVzaWduZWRGb3JXaWR0aCAvIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5kZXNpZ25lZEZvckhlaWdodCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodFRvQXBwbHkgPSB3aWR0aFRvQXBwbHkgKiAodGhpcy5jYXJvdXNlbC5vcHRpb25zLmRlc2lnbmVkRm9ySGVpZ2h0IC8gdGhpcy5jYXJvdXNlbC5vcHRpb25zLmRlc2lnbmVkRm9yV2lkdGgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aFRvQXBwbHkgPSBoZWlnaHRUb0FwcGx5ICogKHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5kZXNpZ25lZEZvcldpZHRoIC8gdGhpcy5jYXJvdXNlbC5vcHRpb25zLmRlc2lnbmVkRm9ySGVpZ2h0KTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyByZXBvc2l0aW9uXHJcbiAgICAgICAgICAgICAgICBjb250YWluZXIuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICBsZWZ0OiB0aGlzLmNhcm91c2VsLndpZGdldCgpLndpZHRoKCkgLyAyIC0gd2lkdGhUb0FwcGx5IC8gMixcclxuICAgICAgICAgICAgICAgICAgICB0b3A6IHRoaXMuY2Fyb3VzZWwud2lkZ2V0KCkuaGVpZ2h0KCkgLyAyIC0gaGVpZ2h0VG9BcHBseSAvIDIsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIGFwcGxheWluZ1xyXG4gICAgICAgICAgICBpZiAod2lkdGhUb0FwcGx5ICE9IHRoaXMuYXBwbGllZFdpZHRoKSB7XHJcbiAgICAgICAgICAgICAgICBjb250YWluZXIud2lkdGgod2lkdGhUb0FwcGx5KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwuaW52YWxpZGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBsaWVkV2lkdGggPSB3aWR0aFRvQXBwbHk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChoZWlnaHRUb0FwcGx5ICE9IHRoaXMuYXBwbGllZEhlaWdodCkge1xyXG4gICAgICAgICAgICAgICAgY29udGFpbmVyLmhlaWdodChoZWlnaHRUb0FwcGx5KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwuaW52YWxpZGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBsaWVkSGVpZ2h0ID0gaGVpZ2h0VG9BcHBseTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHNjYWxlVG9BcHBseSAhPSB0aGlzLmFwcGxpZWRTY2FsZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHNjYWxlVG9BcHBseSA9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZTNkKDBweCwwcHgsIDEwMDAwMHB4KScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiAnc3RhdGljJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjb250YWluZXIuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlM2QoMHB4LDBweCwgMTAwMDAwcHgpIHNjYWxlKCcgKyBzY2FsZVRvQXBwbHkgKyAnKScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNhcm91c2VsLmludmFsaWRhdGUoKTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcGxpZWRTY2FsZSA9IHNjYWxlVG9BcHBseTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgb2JqLmdldEFwcGxpZWRTY2FsZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAodGhpcy5hcHBsaWVkU2NhbGUpXHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwcGxpZWRTY2FsZTtcclxuICAgICAgICByZXR1cm4gMTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLmludGVydmFsKTtcclxuICAgIH07XHJcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGNhcm91c2VsKSB7XHJcbiAgICB0aGlzLmNhcm91c2VsID0gY2Fyb3VzZWw7XHJcbiAgICB0aGlzLm1vdmVtZW50cyA9IFtdO1xyXG4gICAgdGhpcy5wYXNzZWRCcmFraW5nRGlzdGFuY2UgPSAwO1xyXG4gICAgdGhpcy5pc0luUHJvZ3Jlc3MgPSBmYWxzZTtcclxuICAgIHRoaXMubG93RnJpY3Rpb25JblByb2dyZXNzID0gZmFsc2U7XHJcbiAgICB0aGlzLnN3aXRjaGluZ1RvSGlnaEZyaWN0aW9uID0gZmFsc2U7XHJcblxyXG4gICAgdGhpcy5yZWdpc3Rlck1vdmVtZW50ID0gZnVuY3Rpb24gKGRpc3RhbmNlKSB7XHJcbiAgICAgICAgdGhpcy5tb3ZlbWVudHMucHVzaCh7IGRhdGU6IG5ldyBEYXRlKCksIGRpc3RhbmNlOiBkaXN0YW5jZSB9KTtcclxuICAgICAgICB0aGlzLmNsZWFyT2xkTW92ZW1lbnRzKCk7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuc3RvcCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB0aGlzLnN3aXRjaGluZ1RvSGlnaEZyaWN0aW9uID0gZmFsc2U7XHJcbiAgICAgICAgJCh0aGlzKS5zdG9wKGZhbHNlLCBmYWxzZSk7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMubW92ZWRJbnRvSGlnaEZyaWN0aW9uUmFuZ2UgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMubG93RnJpY3Rpb25JblByb2dyZXNzKSB7XHJcbiAgICAgICAgICAgIHRoaXMubG93RnJpY3Rpb25JblByb2dyZXNzID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuc3dpdGNoaW5nVG9IaWdoRnJpY3Rpb24gPSB0cnVlO1xyXG4gICAgICAgICAgICAkKHRoaXMpLnN0b3AoZmFsc2UsIGZhbHNlKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBicmFraW5nRGlzdExlZnQgPSB0aGlzLm1vdGlvbkRhdGEuZnJvbVBvc2l0aW9uICsgdGhpcy5tb3Rpb25EYXRhLmJyYWtpbmdEaXN0YW5jZSAtIHRoaXMucGFzc2VkQnJha2luZ0Rpc3RhbmNlO1xyXG4gICAgICAgICAgICB2YXIgc3BlZWRMZWZ0ID0gdGhpcy5tb3Rpb25EYXRhLmluaXRpYWxTcGVlZCAqIE1hdGguYWJzKGJyYWtpbmdEaXN0TGVmdCAvIHRoaXMubW90aW9uRGF0YS5icmFraW5nRGlzdGFuY2UpO1xyXG5cclxuICAgICAgICAgICAgdmFyIGZyaWN0aW9uID0gdGhpcy5jYXJvdXNlbC5vcHRpb25zLmluZXJ0aWFIaWdoRnJpY3Rpb24gKiAxMDA7XHJcbiAgICAgICAgICAgIHZhciBicmFraW5nRGlzdGFuY2UgPSAoc3BlZWRMZWZ0ICogc3BlZWRMZWZ0KSAvICgyICogZnJpY3Rpb24pO1xyXG4gICAgICAgICAgICBpZiAoc3BlZWRMZWZ0IDwgMClcclxuICAgICAgICAgICAgICAgIGJyYWtpbmdEaXN0YW5jZSAqPSAtMTtcclxuXHJcbiAgICAgICAgICAgIHZhciBicmFraW5nVGltZSA9IGJyYWtpbmdEaXN0YW5jZSAvIChzcGVlZExlZnQgLyAyKTtcclxuXHJcbiAgICAgICAgICAgICQodGhpcykuYW5pbWF0ZSh7IHBhc3NlZEJyYWtpbmdEaXN0YW5jZTogdGhpcy5wYXNzZWRCcmFraW5nRGlzdGFuY2UgKyBicmFraW5nRGlzdGFuY2UgfSwge1xyXG4gICAgICAgICAgICAgICAgZWFzaW5nOiAnZWFzZU91dFF1YWQnLFxyXG4gICAgICAgICAgICAgICAgZHVyYXRpb246IGJyYWtpbmdUaW1lICogMTAwMCxcclxuICAgICAgICAgICAgICAgIHN0ZXA6ICQucHJveHkodGhpcy5vblN0ZXAsIHRoaXMpLFxyXG4gICAgICAgICAgICAgICAgY29tcGxldGU6ICQucHJveHkodGhpcy5vbkNvbXBsZXRlLCB0aGlzKSxcclxuICAgICAgICAgICAgICAgIGZhaWw6ICQucHJveHkodGhpcy5vbkZhaWwsIHRoaXMpXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5ydW4gPSBmdW5jdGlvbiAoZnJvbVBvc2l0aW9uKSB7XHJcblxyXG4gICAgICAgIHZhciBzcGVlZCA9IHRoaXMuZ2V0U3BlZWQoKTtcclxuICAgICAgICB0aGlzLm1vdmVtZW50cyA9IFtdO1xyXG5cclxuICAgICAgICBpZiAoc3BlZWQgPT0gMCkge1xyXG4gICAgICAgICAgICAkKHRoaXMpLnRyaWdnZXIoJ2NvbXBsZXRlJyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHZhciBmcmljdGlvbiA9IHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5pbmVydGlhRnJpY3Rpb24gKiAxMDA7XHJcbiAgICAgICAgdmFyIGJyYWtpbmdEaXN0YW5jZSA9IChzcGVlZCAqIHNwZWVkKSAvICgyICogZnJpY3Rpb24pO1xyXG4gICAgICAgIGlmIChzcGVlZCA8IDApXHJcbiAgICAgICAgICAgIGJyYWtpbmdEaXN0YW5jZSAqPSAtMTtcclxuXHJcbiAgICAgICAgdmFyIGJyYWtpbmdUaW1lID0gYnJha2luZ0Rpc3RhbmNlIC8gKHNwZWVkIC8gMik7XHJcblxyXG4gICAgICAgIHRoaXMucGFzc2VkQnJha2luZ0Rpc3RhbmNlID0gZnJvbVBvc2l0aW9uO1xyXG5cclxuICAgICAgICB0aGlzLmlzSW5Qcm9ncmVzcyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5sb3dGcmljdGlvbkluUHJvZ3Jlc3MgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMubW90aW9uRGF0YSA9IHtcclxuICAgICAgICAgICAgaW5pdGlhbFNwZWVkOiBzcGVlZCxcclxuICAgICAgICAgICAgYnJha2luZ0Rpc3RhbmNlOiBicmFraW5nRGlzdGFuY2UsXHJcbiAgICAgICAgICAgIGZyb21Qb3NpdGlvbjogZnJvbVBvc2l0aW9uXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgJCh0aGlzKS5hbmltYXRlKHsgcGFzc2VkQnJha2luZ0Rpc3RhbmNlOiBmcm9tUG9zaXRpb24gKyBicmFraW5nRGlzdGFuY2UgfSwge1xyXG4gICAgICAgICAgICBlYXNpbmc6ICdlYXNlT3V0Q2lyYycsXHJcbiAgICAgICAgICAgIGR1cmF0aW9uOiBicmFraW5nVGltZSAqIDEwMDAsXHJcbiAgICAgICAgICAgIHN0ZXA6ICQucHJveHkodGhpcy5vblN0ZXAsIHRoaXMpLFxyXG4gICAgICAgICAgICBjb21wbGV0ZTogJC5wcm94eSh0aGlzLm9uQ29tcGxldGUsIHRoaXMpLFxyXG4gICAgICAgICAgICBmYWlsOiAkLnByb3h5KHRoaXMub25GYWlsLCB0aGlzKVxyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm9uU3RlcCA9IGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICBpZiAoaXNOYU4odmFsKSlcclxuICAgICAgICAgICAgcmV0dXJuOyAvL2ZvciBzb21lIGVhc2luZ3Mgd2UgY2FuIGdldCBOYU5zXHJcblxyXG4gICAgICAgIHRoaXMucGFzc2VkQnJha2luZ0Rpc3RhbmNlID0gdmFsO1xyXG4gICAgICAgICQodGhpcykudHJpZ2dlcignc3RlcCcsIHZhbCk7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMub25Db21wbGV0ZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB0aGlzLmlzSW5Qcm9ncmVzcyA9IGZhbHNlO1xyXG4gICAgICAgICQodGhpcykudHJpZ2dlcignY29tcGxldGUnKTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5vbkZhaWwgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLnN3aXRjaGluZ1RvSGlnaEZyaWN0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNJblByb2dyZXNzID0gZmFsc2U7XHJcbiAgICAgICAgICAgICQodGhpcykudHJpZ2dlcignc3RvcCcpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5jbGVhck9sZE1vdmVtZW50cyA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB0aGlzLm1vdmVtZW50cyA9ICQodGhpcy5tb3ZlbWVudHMpLmZpbHRlcihmdW5jdGlvbiAoaSwgZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gKG5ldyBEYXRlKCkgLSBkLmRhdGUpIDwgNTAwMDtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5nZXRTcGVlZCA9IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICAgICAgdmFyIGRpc3RhbmNlID0gMDtcclxuICAgICAgICB2YXIgZGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLm1vdmVtZW50cy5sZW5ndGg7IGkrKykge1xyXG5cclxuICAgICAgICAgICAgdmFyIGQgPSB0aGlzLm1vdmVtZW50c1tpXTtcclxuXHJcbiAgICAgICAgICAgIGlmICgoZGF0ZSAtIGQuZGF0ZSkgPCAyMDApXHJcbiAgICAgICAgICAgICAgICBkaXN0YW5jZSArPSBkLmRpc3RhbmNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGRpc3RhbmNlICogNTtcclxuICAgIH07XHJcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChjYXJvdXNlbCkge1xyXG4gICAgZXh0ZW5kKHRoaXMpO1xyXG5cclxuICAgIHRoaXMuY2Fyb3VzZWwgPSBjYXJvdXNlbDtcclxuICAgIHRoaXMubGFzdEtleURvd25UaW1lID0gbmV3IERhdGUoKTtcclxuICAgIHRoaXMubGFzdFByb2Nlc3NlZEV2ZW50ID0gbnVsbDtcclxuICAgIHRoaXMubm9uSW50ZXJydXB0aWJsZSA9IGZhbHNlO1xyXG4gICAgdGhpcy5kZXN0cm95YWJsZXMgPSBbXTtcclxuXHJcbiAgICB0aGlzLmNhcm91c2VsLndpZGdldCgpLmtleXVwKHRoaXMuZGVzdHJveWFibGUoXCJrZXl1cFwiLCAkLnByb3h5KHRoaXMub25LZXlVcCwgdGhpcykpKTtcclxuICAgIHRoaXMuY2Fyb3VzZWwud2lkZ2V0KCkua2V5ZG93bih0aGlzLmRlc3Ryb3lhYmxlKFwia2V5ZG93blwiLCAkLnByb3h5KHRoaXMub25LZXlEb3duLCB0aGlzKSkpO1xyXG5cclxuICAgIHRoaXMuY2Fyb3VzZWwud2lkZ2V0KCkubW91c2Vkb3duKHRoaXMuZGVzdHJveWFibGUoXCJtb3VzZWRvd25cIiwgJC5wcm94eShmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzUmVhZG9ubHkoKSB8fCAhdGhpcy5nZXRHZXN0dXJlc0VuYWJsZWQoKSlcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLmNhcm91c2VsLndpZGdldCgpLmZvY3VzKCk7XHJcbiAgICAgICAgdGhpcy5tb3Rpb25TdGFydGVkKHRoaXMuaXNWZXJ0aWNhbFJvdGF0aW9uKCkgPyBlLnBhZ2VZIDogZS5wYWdlWCk7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgfSwgdGhpcykpKTtcclxuICAgIHRoaXMuY2Fyb3VzZWwud2lkZ2V0KCkubW91c2Vtb3ZlKHRoaXMuZGVzdHJveWFibGUoXCJtb3VzZW1vdmVcIiwgJC5wcm94eShmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzUmVhZG9ubHkoKSB8fCAhdGhpcy5nZXRHZXN0dXJlc0VuYWJsZWQoKSlcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5jYW5Qcm9jZXNzRXZlbnQoKSkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZ2lzdGVyRXZlbnRBc1Byb2Nlc3NlZCgpO1xyXG4gICAgICAgICAgICB0aGlzLm1vdGlvbkNvbnRpbnVlZCh0aGlzLmlzVmVydGljYWxSb3RhdGlvbigpID8gZS5wYWdlWSA6IGUucGFnZVgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9LCB0aGlzKSkpO1xyXG4gICAgdGhpcy5jYXJvdXNlbC53aWRnZXQoKS5tb3VzZWxlYXZlKHRoaXMuZGVzdHJveWFibGUoXCJtb3VzZWxlYXZlXCIsICQucHJveHkoZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICBpZiAodGhpcy5pc1JlYWRvbmx5KCkgfHwgIXRoaXMuZ2V0R2VzdHVyZXNFbmFibGVkKCkpXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMubm9uSW50ZXJydXB0aWJsZSlcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIHRoaXMuY2Fyb3VzZWwubW90aW9uQ29udHJvbGxlci5tb3Rpb25FbmRlZCh0cnVlKTtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9LCB0aGlzKSkpO1xyXG4gICAgdGhpcy5jYXJvdXNlbC53aWRnZXQoKS5tb3VzZXVwKHRoaXMuZGVzdHJveWFibGUoXCJtb3VzZXVwXCIsICQucHJveHkoZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICBpZiAodGhpcy5pc1JlYWRvbmx5KCkgfHwgIXRoaXMuZ2V0R2VzdHVyZXNFbmFibGVkKCkpXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuXHJcbiAgICAgICAgdGhpcy5jYXJvdXNlbC5tb3Rpb25Db250cm9sbGVyLm1vdGlvbkVuZGVkKHRydWUpO1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIH0sIHRoaXMpKSk7XHJcblxyXG4gICAgaWYgKHR5cGVvZiAodGhpcy5jYXJvdXNlbC53aWRnZXQoKS5nZXQoMCkub25tb3VzZXdoZWVsKSAhPSBcInVuZGVmaW5lZFwiKSB7XHJcbiAgICAgICAgdGhpcy5jYXJvdXNlbC53aWRnZXQoKS5vbihcIm1vdXNld2hlZWxcIiwgdGhpcy5kZXN0cm95YWJsZShcIm1vdXNld2hlZWxcIiwgJC5wcm94eSh0aGlzLm9uTW91c2V3aGVlbCwgdGhpcykpKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5jYXJvdXNlbC53aWRnZXQoKS5vbihcIkRPTU1vdXNlU2Nyb2xsXCIsIHRoaXMuZGVzdHJveWFibGUoXCJET01Nb3VzZVNjcm9sbFwiLCAkLnByb3h5KHRoaXMub25Nb3VzZXdoZWVsLCB0aGlzKSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuY2Fyb3VzZWwud2lkZ2V0KCkub24oJ3RvdWNoc3RhcnQnLCB0aGlzLmRlc3Ryb3lhYmxlKFwidG91Y2hzdGFydFwiLCAkLnByb3h5KGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNSZWFkb25seSgpIHx8ICF0aGlzLmdldEdlc3R1cmVzRW5hYmxlZCgpKVxyXG4gICAgICAgICAgICByZXR1cm47XHJcblxyXG4gICAgICAgIHRoaXMubW90aW9uU3RhcnRlZCh0aGlzLmlzVmVydGljYWxSb3RhdGlvbigpID8gZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbMF0uc2NyZWVuWSA6IGUub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdLnNjcmVlblgpO1xyXG4gICAgfSwgdGhpcykpKTtcclxuICAgIHRoaXMuY2Fyb3VzZWwud2lkZ2V0KCkub24oJ3RvdWNobW92ZScsIHRoaXMuZGVzdHJveWFibGUoXCJ0b3VjaG1vdmVcIiwgJC5wcm94eShmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzUmVhZG9ubHkoKSB8fCAhdGhpcy5nZXRHZXN0dXJlc0VuYWJsZWQoKSlcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5jYW5Qcm9jZXNzRXZlbnQoKSkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZ2lzdGVyRXZlbnRBc1Byb2Nlc3NlZCgpO1xyXG4gICAgICAgICAgICB0aGlzLm1vdGlvbkNvbnRpbnVlZCh0aGlzLmlzVmVydGljYWxSb3RhdGlvbigpID8gZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbMF0uc2NyZWVuWSA6IGUub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdLnNjcmVlblgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9LCB0aGlzKSkpO1xyXG4gICAgdGhpcy5jYXJvdXNlbC53aWRnZXQoKS5vbigndG91Y2hlbmQnLCB0aGlzLmRlc3Ryb3lhYmxlKFwidG91Y2hlbmRcIiwgJC5wcm94eShmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzUmVhZG9ubHkoKSB8fCAhdGhpcy5nZXRHZXN0dXJlc0VuYWJsZWQoKSlcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLmNhcm91c2VsLm1vdGlvbkNvbnRyb2xsZXIubW90aW9uRW5kZWQodHJ1ZSk7XHJcbiAgICB9LCB0aGlzKSkpO1xyXG4gICAgdGhpcy5jYXJvdXNlbC53aWRnZXQoKS5vbigndG91Y2hjYW5jZWwnLCB0aGlzLmRlc3Ryb3lhYmxlKFwidG91Y2hjYW5jZWxcIiwgJC5wcm94eShmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzUmVhZG9ubHkoKSB8fCAhdGhpcy5nZXRHZXN0dXJlc0VuYWJsZWQoKSlcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLmNhcm91c2VsLm1vdGlvbkNvbnRyb2xsZXIubW90aW9uRW5kZWQodHJ1ZSk7XHJcbiAgICB9LCB0aGlzKSkpO1xyXG4gICAgdGhpcy5jYXJvdXNlbC53aWRnZXQoKS5vbigndGFwaG9sZCcsIHRoaXMuZGVzdHJveWFibGUoXCJ0YXBob2xkXCIsIGZ1bmN0aW9uIChlKSB7IGUucHJldmVudERlZmF1bHQoKTsgfSkpO1xyXG59O1xyXG5cclxuZnVuY3Rpb24gZXh0ZW5kKG9iaikge1xyXG5cclxuICAgIG9iai5pc1ZlcnRpY2FsUm90YXRpb24gPSBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jYXJvdXNlbC5vcHRpb25zLnZlcnRpY2FsUm90YXRpb24gJiYgIXRoaXMuY2Fyb3VzZWwucm90YXRpb25Mb2dpY0NvbnRyb2xsZXIuaXNGYWxsYmFja01vZGUoKTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLmdldFNlbnNpdGl2aXR5ID0gZnVuY3Rpb24gKCkge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5jYXJvdXNlbC5yb3RhdGlvbkxvZ2ljQ29udHJvbGxlci5pc0ZhbGxiYWNrTW9kZSgpKVxyXG4gICAgICAgICAgICByZXR1cm4gMTtcclxuICAgICAgICByZXR1cm4gdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbnNpdGl2aXR5IC8gdGhpcy5jYXJvdXNlbC5mbHVpZExheW91dC5nZXRBcHBsaWVkU2NhbGUoKTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLmlzUmVhZG9ubHkgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gIXRoaXMuY2Fyb3VzZWwub3B0aW9ucy5lbmFibGVkIHx8XHJcbiAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5hdXRvcm90YXRpb247XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5nZXRHZXN0dXJlc0VuYWJsZWQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5nZXN0dXJlc0VuYWJsZWQ7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5ub25JbnRlcnJ1cHRpYmxlTW9kZSA9IGZ1bmN0aW9uIChub25JbnRlcnJ1cHRpYmxlKSB7XHJcbiAgICAgICAgdGhpcy5ub25JbnRlcnJ1cHRpYmxlID0gbm9uSW50ZXJydXB0aWJsZTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLmNhblByb2Nlc3NFdmVudCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAodGhpcy5sYXN0UHJvY2Vzc2VkRXZlbnQgPT0gbnVsbClcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcblxyXG4gICAgICAgIGlmIChuZXcgRGF0ZSgpIC0gdGhpcy5sYXN0UHJvY2Vzc2VkRXZlbnQgPiA1MClcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcblxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLnJlZ2lzdGVyRXZlbnRBc1Byb2Nlc3NlZCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB0aGlzLmxhc3RQcm9jZXNzZWRFdmVudCA9IG5ldyBEYXRlKCk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5tb3Rpb25TdGFydGVkID0gZnVuY3Rpb24gKHgpIHtcclxuICAgICAgICB0aGlzLmNhcm91c2VsLm1vdGlvbkNvbnRyb2xsZXIubW90aW9uU3RhcnRlZCh4KTtcclxuICAgICAgICB0aGlzLmluaXRpYWxYID0geDtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLm1vdGlvbkNvbnRpbnVlZCA9IGZ1bmN0aW9uICh4KSB7XHJcbiAgICAgICAgdmFyIGRlbHRhID0gdGhpcy5pbml0aWFsWCAtIHg7XHJcbiAgICAgICAgZGVsdGEgKj0gdGhpcy5nZXRTZW5zaXRpdml0eSgpO1xyXG4gICAgICAgIHggPSB0aGlzLmluaXRpYWxYIC0gZGVsdGE7XHJcblxyXG4gICAgICAgIHRoaXMuY2Fyb3VzZWwubW90aW9uQ29udHJvbGxlci5tb3Rpb25Db250aW51ZWQoeCk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5vbk1vdXNld2hlZWwgPSBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICBpZiAoIXRoaXMuY2Fyb3VzZWwub3B0aW9ucy5tb3VzZXdoZWVsRW5hYmxlZClcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5pc1JlYWRvbmx5KCkpXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuXHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY2Fyb3VzZWwuZ2V0SXNJbk1vdGlvbigpKVxyXG4gICAgICAgICAgICByZXR1cm47XHJcblxyXG4gICAgICAgIHZhciB1cCA9IGZhbHNlO1xyXG4gICAgICAgIHZhciBkb3duID0gZmFsc2U7XHJcbiAgICAgICAgdmFyIG9yaWdpbmFsID0gZXZlbnQub3JpZ2luYWxFdmVudDtcclxuXHJcbiAgICAgICAgXHJcbiAgICAgICAgLy8ganF1ZXJ5IG1vdXNld2hlZWwgcGx1Z2luXHJcbiAgICAgICAgaWYgKGV2ZW50LmRlbHRhWSkge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnQuZGVsdGFZID09IDEpXHJcbiAgICAgICAgICAgICAgICB1cCA9IHRydWU7XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIGlmIChldmVudC5kZWx0YVkgPT0gLTEpXHJcbiAgICAgICAgICAgICAgICAgICAgZG93biA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAob3JpZ2luYWwud2hlZWxEZWx0YSkge1xyXG4gICAgICAgICAgICBpZiAob3JpZ2luYWwud2hlZWxEZWx0YSA+PSAxMjApIHtcclxuICAgICAgICAgICAgICAgIHVwID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlmIChvcmlnaW5hbC53aGVlbERlbHRhIDw9IC0xMjApIHtcclxuICAgICAgICAgICAgICAgICAgICBkb3duID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKG9yaWdpbmFsLmRldGFpbCkge1xyXG4gICAgICAgICAgICBpZiAob3JpZ2luYWwuZGV0YWlsID09IC0zKVxyXG4gICAgICAgICAgICAgICAgdXAgPSB0cnVlO1xyXG4gICAgICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgICAgICBpZiAob3JpZ2luYWwuZGV0YWlsID09IDMpXHJcbiAgICAgICAgICAgICAgICAgICAgZG93biA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIGlmICh1cCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5nZXRTZW5zaXRpdml0eSgpID4gMClcclxuICAgICAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwubW92ZUJhY2soKTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZ2V0U2Vuc2l0aXZpdHkoKSA8IDApXHJcbiAgICAgICAgICAgICAgICB0aGlzLmNhcm91c2VsLm1vdmVGb3J3YXJkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChkb3duKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmdldFNlbnNpdGl2aXR5KCkgPCAwKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5tb3ZlQmFjaygpO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5nZXRTZW5zaXRpdml0eSgpID4gMClcclxuICAgICAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwubW92ZUZvcndhcmQoKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5vbktleURvd24gPSBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICBpZiAoIXRoaXMuY2Fyb3VzZWwub3B0aW9ucy5rZXlib2FyZEVuYWJsZWQpXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuaXNSZWFkb25seSgpKVxyXG4gICAgICAgICAgICByZXR1cm47XHJcblxyXG4gICAgICAgIGlmIChldmVudC53aGljaCA9PSB0aGlzLmdldEJhY2tLZXkoKSB8fCBldmVudC53aGljaCA9PSB0aGlzLmdldEZvcndhcmRLZXkoKSlcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY2Fyb3VzZWwubW90aW9uQ29udHJvbGxlci5pc0luTW90aW9uIHx8IHRoaXMuY2Fyb3VzZWwubW90aW9uQ29udHJvbGxlci5pbmVydGlhLmlzSW5Qcm9ncmVzcylcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAoKG5ldyBEYXRlKCkgLSB0aGlzLmxhc3RLZXlEb3duVGltZSkgPCB0aGlzLmNhcm91c2VsLm9wdGlvbnMubWluS2V5RG93bkZyZXF1ZW5jeSlcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICB0aGlzLmxhc3RLZXlEb3duVGltZSA9IG5ldyBEYXRlKCk7XHJcblxyXG4gICAgICAgIGlmIChldmVudC53aGljaCA9PSB0aGlzLmdldEJhY2tLZXkoKSkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5nZXRTZW5zaXRpdml0eSgpID4gMClcclxuICAgICAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwubW92ZUJhY2soKTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZ2V0U2Vuc2l0aXZpdHkoKSA8IDApXHJcbiAgICAgICAgICAgICAgICB0aGlzLmNhcm91c2VsLm1vdmVGb3J3YXJkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChldmVudC53aGljaCA9PSB0aGlzLmdldEZvcndhcmRLZXkoKSkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5nZXRTZW5zaXRpdml0eSgpIDwgMClcclxuICAgICAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwubW92ZUJhY2soKTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZ2V0U2Vuc2l0aXZpdHkoKSA+IDApXHJcbiAgICAgICAgICAgICAgICB0aGlzLmNhcm91c2VsLm1vdmVGb3J3YXJkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBvYmoub25LZXlVcCA9IGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzUmVhZG9ubHkoKSlcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5jYXJvdXNlbC5tb3Rpb25Db250cm9sbGVyLmlzSW5Nb3Rpb24gfHwgdGhpcy5jYXJvdXNlbC5tb3Rpb25Db250cm9sbGVyLmluZXJ0aWEuaXNJblByb2dyZXNzKVxyXG4gICAgICAgICAgICByZXR1cm47XHJcblxyXG4gICAgICAgIGlmIChldmVudC53aGljaCA9PSB0aGlzLmdldEJhY2tLZXkoKSB8fCBldmVudC53aGljaCA9PSB0aGlzLmdldEZvcndhcmRLZXkoKSkge1xyXG4gICAgICAgICAgICB0aGlzLmNhcm91c2VsLmFuaW1hdGlvbi5jbGVhclF1ZXVlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBvYmouZGVzdHJveWFibGUgPSBmdW5jdGlvbiAoa2V5LCBmdW5jKSB7XHJcbiAgICAgICAgdGhpcy5kZXN0cm95YWJsZXNba2V5XSA9IGZ1bmM7XHJcbiAgICAgICAgcmV0dXJuIGZ1bmM7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGZvciAodmFyIGtleSBpbiB0aGlzLmRlc3Ryb3lhYmxlcykge1xyXG4gICAgICAgICAgICB0aGlzLmNhcm91c2VsLndpZGdldCgpLm9mZihrZXksIHRoaXMuZGVzdHJveWFibGVzW2tleV0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgb2JqLmdldEZvcndhcmRLZXkgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5jYXJvdXNlbC5vcHRpb25zLnZlcnRpY2FsUm90YXRpb24pXHJcbiAgICAgICAgICAgIHJldHVybiA0MDsgLy9kb3duXHJcbiAgICAgICAgcmV0dXJuIDM3OyAvL2xlZnRcclxuICAgIH07XHJcblxyXG4gICAgb2JqLmdldEJhY2tLZXkgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY2Fyb3VzZWwub3B0aW9ucy52ZXJ0aWNhbFJvdGF0aW9uKVxyXG4gICAgICAgICAgICByZXR1cm4gMzg7IC8vdXBcclxuICAgICAgICByZXR1cm4gMzk7IC8vcmlnaHRcclxuICAgIH07XHJcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChjYXJvdXNlbCwgbW90aW9uQ29uc3VtZXIpIHtcclxuICAgIHZhciBpbmVydGlhID0gcmVxdWlyZSgnLi9pbmVydGlhLmpzJyk7XHJcbiAgICBcclxuICAgIGV4dGVuZCh0aGlzKTtcclxuXHJcbiAgICB0aGlzLmlzSW5Nb3Rpb24gPSBmYWxzZTtcclxuICAgIHRoaXMubGFzdFBvc2l0aW9uID0gbnVsbDtcclxuICAgIHRoaXMuZGlzdGFuY2UgPSAwO1xyXG4gICAgdGhpcy5tb3Rpb25Db25zdW1lciA9IG1vdGlvbkNvbnN1bWVyO1xyXG4gICAgdGhpcy5pbmVydGlhID0gbmV3IGluZXJ0aWEoY2Fyb3VzZWwpO1xyXG4gICAgJCh0aGlzLmluZXJ0aWEpLm9uKCdjb21wbGV0ZScsICQucHJveHkodGhpcy5tb3Rpb25FbmRlZCwgdGhpcywgZmFsc2UpKTtcclxuICAgICQodGhpcy5pbmVydGlhKS5vbignc3RvcCcsICQucHJveHkodGhpcy5pbmVydGlhU3RvcCwgdGhpcykpO1xyXG4gICAgJCh0aGlzLmluZXJ0aWEpLm9uKCdzdGVwJywgJC5wcm94eShmdW5jdGlvbiAoZSwgdmFsdWUpIHsgdGhpcy5tb3Rpb25Db250aW51ZWRJbnRlcm5hbCh2YWx1ZSk7IH0sIHRoaXMpKTtcclxufTtcclxuXHJcbmZ1bmN0aW9uIGV4dGVuZChvYmopIHtcclxuICAgIG9iai5tb3Rpb25JblByb2dyZXNzID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmluZXJ0aWEuaXNJblByb2dyZXNzIHx8IHRoaXMuaXNJbk1vdGlvbjtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLm1vdGlvblN0YXJ0ZWQgPSBmdW5jdGlvbiAocG9zaXRpb24pIHtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLmluZXJ0aWEuaXNJblByb2dyZXNzKVxyXG4gICAgICAgICAgICB0aGlzLmRpc3RhbmNlID0gMDtcclxuXHJcbiAgICAgICAgdGhpcy5pbmVydGlhLnN0b3AoKTtcclxuICAgICAgICB0aGlzLmlzSW5Nb3Rpb24gPSB0cnVlO1xyXG4gICAgICAgIHRoaXMubGFzdFBvc2l0aW9uID0gcG9zaXRpb247XHJcbiAgICAgICAgdGhpcy5yYWlzZVN0YXJ0ID0gdHJ1ZTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLm1vdGlvbkNvbnRpbnVlZCA9IGZ1bmN0aW9uIChwb3NpdGlvbikge1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuaXNJbk1vdGlvbilcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5yYWlzZVN0YXJ0ICYmIHBvc2l0aW9uICE9IHRoaXMubGFzdFBvc2l0aW9uKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgJCh0aGlzKS50cmlnZ2VyKCdzdGFydCcpO1xyXG4gICAgICAgICAgICB0aGlzLnJhaXNlU3RhcnQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuaW5lcnRpYS5yZWdpc3Rlck1vdmVtZW50KHBvc2l0aW9uIC0gdGhpcy5sYXN0UG9zaXRpb24pO1xyXG4gICAgICAgIHRoaXMubW90aW9uQ29udGludWVkSW50ZXJuYWwocG9zaXRpb24pO1xyXG4gICAgfTtcclxuXHJcbiAgICBvYmoubW90aW9uQ29udGludWVkSW50ZXJuYWwgPSBmdW5jdGlvbiAocG9zaXRpb24pIHtcclxuXHJcbiAgICAgICAgdGhpcy5kaXN0YW5jZSArPSBwb3NpdGlvbiAtIHRoaXMubGFzdFBvc2l0aW9uO1xyXG4gICAgICAgIHRoaXMubGFzdFBvc2l0aW9uID0gcG9zaXRpb247XHJcbiAgICAgICAgdmFyIGNvbnN1bWluZ0RhdGEgPSB0aGlzLm1vdGlvbkNvbnN1bWVyKHRoaXMuZGlzdGFuY2UpO1xyXG4gICAgICAgIHRoaXMuZGlzdGFuY2UgLT0gY29uc3VtaW5nRGF0YS5kaXN0YW5jZTtcclxuXHJcbiAgICAgICAgaWYgKGNvbnN1bWluZ0RhdGEuaGlnaEZyaWN0aW9uUmFuZ2UgJiYgdGhpcy5pbmVydGlhLmlzSW5Qcm9ncmVzcykge1xyXG4gICAgICAgICAgICB0aGlzLmluZXJ0aWEubW92ZWRJbnRvSGlnaEZyaWN0aW9uUmFuZ2UoKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5tb3Rpb25FbmRlZCA9IGZ1bmN0aW9uICh1c2VJbmVydGlhKSB7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmluZXJ0aWEuaXNJblByb2dyZXNzKVxyXG4gICAgICAgICAgICByZXR1cm47XHJcblxyXG4gICAgICAgIHRoaXMuaXNJbk1vdGlvbiA9IGZhbHNlO1xyXG5cclxuICAgICAgICBpZiAoIXVzZUluZXJ0aWEpIHtcclxuICAgICAgICAgICAgJCh0aGlzKS50cmlnZ2VyKCdlbmQnLCB0aGlzLmRpc3RhbmNlKTtcclxuICAgICAgICAgICAgdGhpcy5kaXN0YW5jZSA9IDA7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5pbmVydGlhLnJ1bih0aGlzLmxhc3RQb3NpdGlvbik7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBvYmouaW5lcnRpYVN0b3AgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy5pc0luTW90aW9uID0gZmFsc2U7XHJcbiAgICB9O1xyXG59IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoY2Fyb3VzZWwsIHNldHRpbmdzKSB7XHJcbiAgICBcclxuICAgIHZhciBwYXRoX2Jhc2UgPSByZXF1aXJlKCcuL3BhdGhfYmFzZS5qcycpO1xyXG4gICAgdmFyIHBvaW50ID0gcmVxdWlyZSgnLi8uLi9wb2ludC5qcycpO1xyXG5cclxuICAgIHZhciBiYXNlID0gbmV3IHBhdGhfYmFzZShjYXJvdXNlbCwge1xyXG4gICAgICAgIGZpOiAxMCxcclxuICAgICAgICBmbGF0bmVzczogMTBcclxuICAgIH0pO1xyXG5cclxuICAgICQuZXh0ZW5kKHRoaXMsIGJhc2UpO1xyXG4gICAgJC5leHRlbmQodGhpcy5zZXR0aW5ncywgc2V0dGluZ3MpO1xyXG5cclxuICAgIHRoaXMuZ2V0UG9pbnRJbnRlcm5hbCA9IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG5cclxuICAgICAgICB2YXIgYW5nbGUgPSB2YWx1ZSAqIE1hdGguUEkgKiAyIC8gMzYwO1xyXG5cclxuICAgICAgICB2YXIgeCA9IHRoaXMuc2V0dGluZ3MuZmkgKiB2YWx1ZSAqIE1hdGguY29zKGFuZ2xlKSAvICgyICogTWF0aC5QSSk7XHJcbiAgICAgICAgdmFyIHkgPSB0aGlzLnNldHRpbmdzLmZpICogdmFsdWUgKiBNYXRoLnNpbihhbmdsZSkgLyAoMiAqIE1hdGguUEkpO1xyXG5cclxuICAgICAgICB2YXIgeiA9IC0xICogTWF0aC5wb3codmFsdWUgLSB0aGlzLnJvb3RWYWx1ZSgpLCAyKSAvIHRoaXMuc2V0dGluZ3MuZmxhdG5lc3M7XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgcG9pbnQoeCwgeSwgeik7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMucm9vdFZhbHVlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiA0NTA7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMubWluVmFsdWUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIDEwO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm1heFZhbHVlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiA2NTA7XHJcbiAgICB9O1xyXG59OyIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNhcm91c2VsLCBzZXR0aW5ncykge1xyXG4gICAgdmFyIHJvdGF0aW9uID0gcmVxdWlyZSgnLi8uLi9yb3RhdGlvbi5qcycpO1xyXG4gICAgdmFyIHZlY3RvciA9IHJlcXVpcmUoJy4vLi4vdmVjdG9yLmpzJyk7XHJcblxyXG4gICAgdGhpcy5zZXR0aW5ncyA9IHtcclxuICAgICAgICBzaGlmdFg6IDAsXHJcbiAgICAgICAgc2hpZnRZOiAwLFxyXG4gICAgICAgIHNoaWZ0WjogMCxcclxuICAgICAgICByb3RhdGlvbkFuZ2xlWFk6IDAsXHJcbiAgICAgICAgcm90YXRpb25BbmdsZVpZOiAwLFxyXG4gICAgICAgIHJvdGF0aW9uQW5nbGVaWDogMCxcclxuICAgICAgICByb3RhdGVFbGVtZW50czogZmFsc2UsXHJcbiAgICAgICAgZW5kbGVzczogZmFsc2VcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5jYXJvdXNlbCA9IGNhcm91c2VsO1xyXG4gICAgJC5leHRlbmQodGhpcy5zZXR0aW5ncywgc2V0dGluZ3MpO1xyXG5cclxuXHJcbiAgICB0aGlzLmdldENvbnRhaW5lclNpemUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2Fyb3VzZWwuX2dldENvbnRhaW5lclNpemUoKTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5pc0VuZGxlc3MgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MuZW5kbGVzcztcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5taW5WYWx1ZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5tYXhWYWx1ZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5yb290VmFsdWUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIDA7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuaW5jcmVtZW50VmFsdWUgPSBmdW5jdGlvbiAodmFsdWUsIGRpc3RhbmNlKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlICsgZGlzdGFuY2U7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuZGVjcmVtZW50VmFsdWUgPSBmdW5jdGlvbiAodmFsdWUsIGRpc3RhbmNlKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlIC0gZGlzdGFuY2U7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuZ2V0UG9pbnQgPSBmdW5jdGlvbiAodmFsdWUpIHtcclxuXHJcbiAgICAgICAgdmFyIHJlcyA9IHRoaXMuZ2V0UG9pbnRJbnRlcm5hbCh2YWx1ZSk7XHJcblxyXG4gICAgICAgIHJlcy54ID0gcmVzLnggKyB0aGlzLnNldHRpbmdzLnNoaWZ0WDtcclxuICAgICAgICByZXMueSA9IHJlcy55ICsgdGhpcy5zZXR0aW5ncy5zaGlmdFk7XHJcbiAgICAgICAgcmVzLnogPSByZXMueiArIHRoaXMuc2V0dGluZ3Muc2hpZnRaO1xyXG5cclxuICAgICAgICB2YXIgcGFpciA9IG51bGw7XHJcblxyXG4gICAgICAgIHBhaXIgPSB0aGlzLnJvdGF0ZSh7IGE6IHJlcy54LCBiOiByZXMueSB9LCB7IGE6IDAsIGI6IDAgfSwgdGhpcy5zZXR0aW5ncy5yb3RhdGlvbkFuZ2xlWFkpO1xyXG4gICAgICAgIHJlcy54ID0gcGFpci5hO1xyXG4gICAgICAgIHJlcy55ID0gcGFpci5iO1xyXG5cclxuICAgICAgICBwYWlyID0gdGhpcy5yb3RhdGUoeyBhOiByZXMueiwgYjogcmVzLnkgfSwgeyBhOiAwLCBiOiAwIH0sIHRoaXMuc2V0dGluZ3Mucm90YXRpb25BbmdsZVpZKTtcclxuICAgICAgICByZXMueiA9IHBhaXIuYTtcclxuICAgICAgICByZXMueSA9IHBhaXIuYjtcclxuXHJcbiAgICAgICAgcGFpciA9IHRoaXMucm90YXRlKHsgYTogcmVzLnosIGI6IHJlcy54IH0sIHsgYTogMCwgYjogMCB9LCB0aGlzLnNldHRpbmdzLnJvdGF0aW9uQW5nbGVaWCk7XHJcbiAgICAgICAgcmVzLnogPSBwYWlyLmE7XHJcbiAgICAgICAgcmVzLnggPSBwYWlyLmI7XHJcblxyXG4gICAgICAgIHJldHVybiByZXM7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMucm90YXRlID0gZnVuY3Rpb24gKHBhaXJUb1JvdGF0ZSwgcGFpckNlbnRlciwgYW5nbGUpIHtcclxuICAgICAgICBpZiAoYW5nbGUgPT0gMClcclxuICAgICAgICAgICAgcmV0dXJuIHBhaXJUb1JvdGF0ZTtcclxuXHJcbiAgICAgICAgdmFyIGFuZ2xlSW5SYWRpYW5zID0gYW5nbGUgKiAoTWF0aC5QSSAvIDE4MCk7XHJcbiAgICAgICAgdmFyIGNvc1RoZXRhID0gTWF0aC5jb3MoYW5nbGVJblJhZGlhbnMpO1xyXG4gICAgICAgIHZhciBzaW5UaGV0YSA9IE1hdGguc2luKGFuZ2xlSW5SYWRpYW5zKTtcclxuXHJcbiAgICAgICAgdmFyIGEgPSAoY29zVGhldGEgKiAocGFpclRvUm90YXRlLmEgLSBwYWlyQ2VudGVyLmEpIC0gc2luVGhldGEgKiAocGFpclRvUm90YXRlLmIgLSBwYWlyQ2VudGVyLmIpICsgcGFpckNlbnRlci5hKTtcclxuICAgICAgICB2YXIgYiA9IChzaW5UaGV0YSAqIChwYWlyVG9Sb3RhdGUuYSAtIHBhaXJDZW50ZXIuYSkgKyBjb3NUaGV0YSAqIChwYWlyVG9Sb3RhdGUuYiAtIHBhaXJDZW50ZXIuYikgKyBwYWlyQ2VudGVyLmIpO1xyXG4gICAgICAgIHBhaXJUb1JvdGF0ZS5hID0gYTtcclxuICAgICAgICBwYWlyVG9Sb3RhdGUuYiA9IGI7XHJcblxyXG4gICAgICAgIHJldHVybiBwYWlyVG9Sb3RhdGU7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMuZWxlbWVudHNSb3RhdGlvbiA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAoIXRoaXMuc2V0dGluZ3Mucm90YXRlRWxlbWVudHMpXHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zZXR0aW5ncy5yb3RhdGlvbkFuZ2xlWlkgPT0gMCAmJiB0aGlzLnNldHRpbmdzLnJvdGF0aW9uQW5nbGVaWCA9PSAwICYmIHRoaXMuc2V0dGluZ3Mucm90YXRpb25BbmdsZVhZID09IDApXHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIFxyXG4gICAgICAgIHZhciB2ID0gbmV3IHZlY3RvcigpO1xyXG4gICAgICAgIHYueCA9IHRoaXMuc2V0dGluZ3Mucm90YXRpb25BbmdsZVpZO1xyXG4gICAgICAgIHYueSA9IC0xICogdGhpcy5zZXR0aW5ncy5yb3RhdGlvbkFuZ2xlWlg7XHJcbiAgICAgICAgdi56ID0gLTEgKiB0aGlzLnNldHRpbmdzLnJvdGF0aW9uQW5nbGVYWTtcclxuXHJcbiAgICAgICAgdmFyIGFuZ2xlID0gLTEgKiB2Lmxlbmd0aCgpO1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IHJvdGF0aW9uKHYsIGFuZ2xlKTtcclxuICAgIH07XHJcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChjYXJvdXNlbCwgc2V0dGluZ3MpIHtcclxuXHJcbiAgICB2YXIgcGF0aF9iYXNlID0gcmVxdWlyZSgnLi9wYXRoX2Jhc2UuanMnKTtcclxuICAgIHZhciBwb2ludCA9IHJlcXVpcmUoJy4vLi4vcG9pbnQuanMnKTtcclxuXHJcbiAgICB2YXIgYmFzZSA9IG5ldyBwYXRoX2Jhc2UoY2Fyb3VzZWwsIHtcclxuICAgICAgICB3aWRlbmVzczogMjAwLFxyXG4gICAgfSk7XHJcblxyXG4gICAgJC5leHRlbmQodGhpcywgYmFzZSk7XHJcbiAgICAkLmV4dGVuZCh0aGlzLnNldHRpbmdzLCBzZXR0aW5ncyk7XHJcblxyXG4gICAgdGhpcy5nZXRQb2ludEludGVybmFsID0gZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgdmFyIHkgPSAodmFsdWUgKiB2YWx1ZSAqIHZhbHVlICsgdmFsdWUgKiAyMCkgLyAoMTAwMCAqIHRoaXMuc2V0dGluZ3Mud2lkZW5lc3MpO1xyXG4gICAgICAgIHZhciB6ID0gLTIgKiBNYXRoLmFicyh5KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBwb2ludCh2YWx1ZSwgeSwgeik7XHJcbiAgICB9O1xyXG59OyIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oY2Fyb3VzZWwsIHNldHRpbmdzKSB7XHJcblxyXG4gICAgdmFyIHBhdGhfYmFzZSA9IHJlcXVpcmUoJy4vcGF0aF9iYXNlLmpzJyk7XHJcbiAgICB2YXIgcG9pbnQgPSByZXF1aXJlKCcuLy4uL3BvaW50LmpzJyk7XHJcbiAgICB2YXIgZ2V0QmV6aWVyQm94ID0gcmVxdWlyZSgnLi8uLi9iZXppZXJfYm94LmpzJyk7XHJcbiAgICB2YXIgZ2V0QmV6aWVyID0gcmVxdWlyZSgnLi8uLi9iZXppZXIuanMnKTtcclxuXHJcbiAgICB2YXIgYmFzZSA9IG5ldyBwYXRoX2Jhc2UoY2Fyb3VzZWwsIHtcclxuICAgICAgICB4eVBhdGhCZXppZXJQb2ludHM6IHsgcDE6IHsgeDogLTEwMCwgeTogMCB9LCBwMjogeyB4OiAwLCB5OiAwIH0sIHAzOiB7IHg6IDAsIHk6IDAgfSwgcDQ6IHsgeDogMTAwLCB5OiAwIH0gfSxcclxuICAgICAgICB4elBhdGhCZXppZXJQb2ludHM6IHsgcDE6IHsgeDogLTEwMCwgeTogMCB9LCBwMjogeyB4OiAwLCB5OiAwIH0sIHAzOiB7IHg6IDAsIHk6IDAgfSwgcDQ6IHsgeDogMTAwLCB5OiAwIH0gfSxcclxuICAgICAgICB4eUFyY0xlbmd0aEJlemllclBvaW50czogeyBwMTogeyB4OiAwLCB5OiAwIH0sIHAyOiB7IHg6IDUwLCB5OiA1MCB9LCBwMzogeyB4OiA1MCwgeTogNTAgfSwgcDQ6IHsgeDogMTAwLCB5OiAxMDAgfSB9LFxyXG4gICAgICAgIHBhdGhMZW5ndGg6IDEwMDAsXHJcbiAgICAgICAgemVyb1Bvc2l0aW9uOiAwLjUsXHJcbiAgICAgICAgd2lkdGg6IDEwMDAsXHJcbiAgICAgICAgaGVpZ2h0OiAxMDAwLFxyXG4gICAgICAgIGRlcHRoOiAxMDAwXHJcbiAgICB9KTtcclxuXHJcbiAgICAkLmV4dGVuZCh0aGlzLCBiYXNlKTtcclxuICAgICQuZXh0ZW5kKHRoaXMuc2V0dGluZ3MsIHNldHRpbmdzKTtcclxuXHJcbiAgICB0aGlzLmdldFBvaW50SW50ZXJuYWwgPSBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICB2YXIgZGlzdGFuY2UgPSBNYXRoLmFicyh2YWx1ZSAtIHRoaXMubWluVmFsdWUoKSk7XHJcbiAgICAgICAgdmFyIGFic0Rpc3QgPSBNYXRoLmFicyh0aGlzLm1heFZhbHVlKCkgLSB0aGlzLm1pblZhbHVlKCkpO1xyXG5cclxuICAgICAgICB2YXIgYmV6aWVyVCA9IE1hdGgubWluKGRpc3RhbmNlIC8gYWJzRGlzdCwgMSk7XHJcblxyXG4gICAgICAgIHZhciB4eVBvaW50cyA9IHRoaXMuc2V0dGluZ3MueHlQYXRoQmV6aWVyUG9pbnRzO1xyXG4gICAgICAgIHZhciB4eUFyY0xlbmd0aFBvaW50cyA9IHRoaXMuc2V0dGluZ3MueHlBcmNMZW5ndGhCZXppZXJQb2ludHM7XHJcbiAgICAgICAgdmFyIHh6UG9pbnRzID0gdGhpcy5zZXR0aW5ncy54elBhdGhCZXppZXJQb2ludHM7XHJcblxyXG4gICAgICAgIHZhciBib3hYWSA9IGdldEJlemllckJveCh4eVBvaW50cy5wMSwgeHlQb2ludHMucDIsIHh5UG9pbnRzLnAzLCB4eVBvaW50cy5wNCk7XHJcbiAgICAgICAgdmFyIGJveFhaID0gZ2V0QmV6aWVyQm94KHh6UG9pbnRzLnAxLCB4elBvaW50cy5wMiwgeHpQb2ludHMucDMsIHh6UG9pbnRzLnA0KTtcclxuXHJcbiAgICAgICAgYmV6aWVyVCA9IGdldEJlemllcihiZXppZXJULCB4eUFyY0xlbmd0aFBvaW50cy5wMSwgeHlBcmNMZW5ndGhQb2ludHMucDIsIHh5QXJjTGVuZ3RoUG9pbnRzLnAzLCB4eUFyY0xlbmd0aFBvaW50cy5wNCkueSAvIDEwMDtcclxuXHJcbiAgICAgICAgdmFyIHBvaW50WFkgPSBnZXRCZXppZXIoYmV6aWVyVCwgeHlQb2ludHMucDEsIHh5UG9pbnRzLnAyLCB4eVBvaW50cy5wMywgeHlQb2ludHMucDQpO1xyXG4gICAgICAgIHZhciB4ID0gdGhpcy5ub3JtYWxpemVWYWwocG9pbnRYWS54LCBib3hYWS5taW5YLCBib3hYWS5tYXhYKTtcclxuICAgICAgICB2YXIgeSA9IHRoaXMubm9ybWFsaXplVmFsKHBvaW50WFkueSwgYm94WFkubWluWSwgYm94WFkubWF4WSk7XHJcbiAgICAgICAgdmFyIHogPSBnZXRCZXppZXIoYmV6aWVyVCwgeHpQb2ludHMucDEsIHh6UG9pbnRzLnAyLCB4elBvaW50cy5wMywgeHpQb2ludHMucDQpLnk7XHJcbiAgICAgICAgeiA9IHRoaXMubm9ybWFsaXplVmFsKHosIGJveFhaLm1pblksIGJveFhaLm1heFkpO1xyXG5cclxuICAgICAgICB4ID0gdGhpcy5zZXR0aW5ncy53aWR0aCAqIHggLSB0aGlzLnNldHRpbmdzLndpZHRoIC8gMjtcclxuICAgICAgICB5ID0gdGhpcy5zZXR0aW5ncy5oZWlnaHQgKiB5ICogLTE7XHJcbiAgICAgICAgeiA9IHRoaXMuc2V0dGluZ3MuZGVwdGggKiB6ICogLTE7XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgcG9pbnQoeCwgeSwgeik7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMubm9ybWFsaXplVmFsID0gZnVuY3Rpb24gKHZhbCwgYSwgYikge1xyXG4gICAgICAgIHZhciBkaXN0ID0gTWF0aC5hYnMoYiAtIGEpO1xyXG4gICAgICAgIHZhciBtaW4gPSBNYXRoLm1pbihhLCBiKTtcclxuXHJcbiAgICAgICAgaWYgKGRpc3QgIT0gMClcclxuICAgICAgICAgICAgcmV0dXJuICh2YWwgLSBtaW4pIC8gZGlzdDtcclxuICAgICAgICByZXR1cm4gdmFsIC0gbWluO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm1pblZhbHVlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiAwO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLm1heFZhbHVlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNldHRpbmdzLnBhdGhMZW5ndGg7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMucm9vdFZhbHVlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNldHRpbmdzLnBhdGhMZW5ndGggKiB0aGlzLnNldHRpbmdzLnplcm9Qb3NpdGlvbjtcclxuICAgIH07XHJcbn07IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoY2Fyb3VzZWwsIHNldHRpbmdzKSB7XHJcblxyXG4gICAgdmFyIHBhdGhfYmFzZSA9IHJlcXVpcmUoJy4vcGF0aF9iYXNlLmpzJyk7XHJcbiAgICB2YXIgcG9pbnQgPSByZXF1aXJlKCcuLy4uL3BvaW50LmpzJyk7XHJcblxyXG4gICAgdmFyIGJhc2UgPSBuZXcgcGF0aF9iYXNlKGNhcm91c2VsLCB7XHJcbiAgICAgICAgYTogMjAwLFxyXG4gICAgICAgIGI6IDIwMCxcclxuICAgIH0pO1xyXG5cclxuICAgICQuZXh0ZW5kKHRoaXMsIGJhc2UpO1xyXG4gICAgJC5leHRlbmQodGhpcy5zZXR0aW5ncywgc2V0dGluZ3MpO1xyXG5cclxuICAgIHRoaXMuZ2V0UG9pbnRJbnRlcm5hbCA9IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgIHZhbHVlICo9IC0xO1xyXG4gICAgICAgIHZhbHVlIC09IDE4MDtcclxuICAgICAgICB2YXIgYW5nbGUgPSB2YWx1ZSAqIE1hdGguUEkgKiAyIC8gMzYwO1xyXG4gICAgICAgIHZhciB6ID0gdGhpcy5zZXR0aW5ncy5iICogTWF0aC5zaW4oYW5nbGUpO1xyXG4gICAgICAgIHZhciB4ID0gdGhpcy5zZXR0aW5ncy5hICogTWF0aC5jb3MoYW5nbGUpO1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IHBvaW50KHgsIDAsIHopO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnJvb3RWYWx1ZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gOTA7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMubWluVmFsdWUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MuZW5kbGVzcyA/IC05MCA6IDA7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMubWF4VmFsdWUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MuZW5kbGVzcyA/IDI3MCA6IDE4MDtcclxuICAgIH07XHJcbn07IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoY2Fyb3VzZWwsIHNldHRpbmdzKSB7XHJcblxyXG4gICAgdmFyIHBhdGhfYmFzZSA9IHJlcXVpcmUoJy4vcGF0aF9iYXNlLmpzJyk7XHJcbiAgICB2YXIgcG9pbnQgPSByZXF1aXJlKCcuLy4uL3BvaW50LmpzJyk7XHJcblxyXG4gICAgdmFyIGJhc2UgPSBuZXcgcGF0aF9iYXNlKGNhcm91c2VsLCB7XHJcbiAgICAgICAgXHJcbiAgICB9KTtcclxuXHJcbiAgICAkLmV4dGVuZCh0aGlzLCBiYXNlKTtcclxuICAgICQuZXh0ZW5kKHRoaXMuc2V0dGluZ3MsIHNldHRpbmdzKTtcclxuXHJcbiAgICB0aGlzLmdldFBvaW50SW50ZXJuYWwgPSBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICB2YXIgeiA9IC0xICogdmFsdWU7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBwb2ludCgwLCAwLCB6KTtcclxuICAgIH07XHJcbn07IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoY2Fyb3VzZWwsIHNldHRpbmdzKSB7XHJcblxyXG4gICAgdmFyIHBhdGhfYmFzZSA9IHJlcXVpcmUoJy4vcGF0aF9iYXNlLmpzJyk7XHJcbiAgICB2YXIgcG9pbnQgPSByZXF1aXJlKCcuLy4uL3BvaW50LmpzJyk7XHJcblxyXG4gICAgdmFyIGJhc2UgPSBuZXcgcGF0aF9iYXNlKGNhcm91c2VsLCB7XHJcbiAgICAgICAgd2lkZW5lc3M6IDIwMCxcclxuICAgIH0pO1xyXG5cclxuICAgICQuZXh0ZW5kKHRoaXMsIGJhc2UpO1xyXG4gICAgJC5leHRlbmQodGhpcy5zZXR0aW5ncywgc2V0dGluZ3MpO1xyXG5cclxuICAgIHRoaXMuZ2V0UG9pbnRJbnRlcm5hbCA9IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgIHZhciB6ID0gLTEgKiB2YWx1ZSAqIHZhbHVlICogKDEgLyB0aGlzLnNldHRpbmdzLndpZGVuZXNzKTtcclxuICAgICAgICByZXR1cm4gbmV3IHBvaW50KHZhbHVlLCAwLCB6KTtcclxuICAgIH07XHJcbn07IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoeCwgeSwgeikge1xyXG4gICAgdGhpcy54ID0geDtcclxuICAgIHRoaXMueSA9IHk7XHJcbiAgICB0aGlzLnogPSB6O1xyXG59IiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbih2ZWN0b3IsIGFuZ2xlKSB7XHJcbiAgICB0aGlzLnZlY3RvciA9IHZlY3RvcjtcclxuICAgIHRoaXMuYW5nbGUgPSBhbmdsZTtcclxuXHJcbiAgICB0aGlzLmdldFN0cmluZyA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gJ3JvdGF0ZTNkKCcgKyB0aGlzLnZlY3Rvci54ICsgJywgJyArIHRoaXMudmVjdG9yLnkgKyAnLCAnICsgdGhpcy52ZWN0b3IueiArICcsICcgKyB0aGlzLmFuZ2xlICsgJ2RlZyknO1xyXG4gICAgfTtcclxufTsiLCJ2YXIgdHJhbnNmb3JtID0gcmVxdWlyZSgnLi90cmFuc2Zvcm0uanMnKTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNhcm91c2VsKSB7XHJcblxyXG4gICAgdGhpcy5jYXJvdXNlbCA9IGNhcm91c2VsO1xyXG4gICAgZXh0ZW5kKHRoaXMpO1xyXG59O1xyXG5cclxuZnVuY3Rpb24gZXh0ZW5kKG9iaikge1xyXG5cclxuICAgIG9iai5pc0ZhbGxiYWNrTW9kZSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIGZhbHNlOyB9O1xyXG5cclxuICAgIG9iai5nZXRBY3R1YWxEaXN0YW5jZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNhcm91c2VsLm9wdGlvbnMuZGlzdGFuY2U7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5nZXRSb290VmFsdWUgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jYXJvdXNlbC5wYXRoLnJvb3RWYWx1ZSgpO1xyXG4gICAgfTtcclxuXHJcbiAgICBvYmouaW5jcmVtZW50VmFsdWUgPSBmdW5jdGlvbiAodmFsdWUsIGluY3JlbWVudCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNhcm91c2VsLnBhdGguaW5jcmVtZW50VmFsdWUodmFsdWUsIGluY3JlbWVudCk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5kZWNyZW1lbnRWYWx1ZSA9IGZ1bmN0aW9uICh2YWx1ZSwgZGVjcmVtZW50KSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2Fyb3VzZWwucGF0aC5kZWNyZW1lbnRWYWx1ZSh2YWx1ZSwgZGVjcmVtZW50KTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLm1pblZhbHVlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNhcm91c2VsLnBhdGgubWluVmFsdWUoKTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLm1heFZhbHVlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNhcm91c2VsLnBhdGgubWF4VmFsdWUoKTtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLm1vdmVUbyA9IGZ1bmN0aW9uIChpbmRleCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNhcm91c2VsLm1vdGlvbkNvbnRyb2xsZXIubW90aW9uSW5Qcm9ncmVzcygpKVxyXG4gICAgICAgICAgICByZXR1cm47XHJcblxyXG4gICAgICAgIHRoaXMuY2Fyb3VzZWwuYW5pbWF0aW9uLmNsZWFyUXVldWUoKTtcclxuICAgICAgICB0aGlzLmNhcm91c2VsLmFuaW1hdGlvbi5jb21wbGV0ZUN1cnJlbnRJbW1lZGlhdGVseSgpO1xyXG5cclxuICAgICAgICBpZiAoaW5kZXggPT0gdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXgpXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICBpZiAoaW5kZXggPT0gdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXggKyAxKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwubW92ZUZvcndhcmQoKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoaW5kZXggPT0gdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXggLSAxKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwubW92ZUJhY2soKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaW5kZXggPSBNYXRoLm1heCgwLCBpbmRleCk7XHJcbiAgICAgICAgaW5kZXggPSBNYXRoLm1pbihpbmRleCwgdGhpcy5jYXJvdXNlbC5lbGVtZW50cy5sZW5ndGggLSAxKTtcclxuXHJcbiAgICAgICAgdGhpcy5tb3ZlVG9JbnRlcm5hbChpbmRleCk7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5tb3ZlVG9JbnRlcm5hbCA9IGZ1bmN0aW9uIChpbmRleCkge1xyXG4gICAgICAgIHZhciBkaXN0YW5jZSA9IHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKSAqICh0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCAtIGluZGV4KTtcclxuICAgICAgICB0aGlzLmNhcm91c2VsLmlucHV0Q29udHJvbGxlci5ub25JbnRlcnJ1cHRpYmxlTW9kZSh0cnVlKTtcclxuICAgICAgICB0aGlzLmNhcm91c2VsLl9yYWlzZU1vdGlvblN0YXJ0KCk7XHJcbiAgICAgICAgdGhpcy5jYXJvdXNlbC5hbmltYXRpb24uYW5pbWF0ZSgwLCBkaXN0YW5jZSwgaW5kZXgsIFwibGluZWFyXCIpO1xyXG4gICAgfTtcclxuXHJcbiAgICBvYmoubW92ZUJhY2sgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIHBlbmRpbmdUYXJnZXQgPSB0aGlzLmNhcm91c2VsLmFuaW1hdGlvbi5pc0luUHJvZ3Jlc3MgPyB0aGlzLmNhcm91c2VsLmFuaW1hdGlvbi5nZXRUYXJnZXRWYWx1ZSgpIDogdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXg7XHJcblxyXG4gICAgICAgIGlmIChwZW5kaW5nVGFyZ2V0ID4gMCkge1xyXG4gICAgICAgICAgICBwZW5kaW5nVGFyZ2V0LS07XHJcblxyXG4gICAgICAgICAgICB0aGlzLmNhcm91c2VsLmlucHV0Q29udHJvbGxlci5ub25JbnRlcnJ1cHRpYmxlTW9kZSh0cnVlKTtcclxuICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5fcmFpc2VNb3Rpb25TdGFydCgpO1xyXG4gICAgICAgICAgICB0aGlzLmNhcm91c2VsLmFuaW1hdGlvbi5hbmltYXRlKDAsIHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKSwgcGVuZGluZ1RhcmdldCwgbnVsbCk7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5tb3ZlRm9yd2FyZCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgcGVuZGluZ1RhcmdldCA9IHRoaXMuY2Fyb3VzZWwuYW5pbWF0aW9uLmlzSW5Qcm9ncmVzcyA/IHRoaXMuY2Fyb3VzZWwuYW5pbWF0aW9uLmdldFRhcmdldFZhbHVlKCkgOiB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleDtcclxuXHJcbiAgICAgICAgaWYgKHBlbmRpbmdUYXJnZXQgPCB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aCAtIDEpIHtcclxuICAgICAgICAgICAgcGVuZGluZ1RhcmdldCsrO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5pbnB1dENvbnRyb2xsZXIubm9uSW50ZXJydXB0aWJsZU1vZGUodHJ1ZSk7XHJcbiAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwuX3JhaXNlTW90aW9uU3RhcnQoKTtcclxuICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5hbmltYXRpb24uYW5pbWF0ZSgwLCAtMSAqIHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKSwgcGVuZGluZ1RhcmdldCwgbnVsbCk7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5jb25zdW1lTW90aW9uID0gZnVuY3Rpb24gKGRpc3RhbmNlKSB7XHJcbiAgICAgICAgdmFyIGhpZ2hGcmljdGlvblJhbmdlID0gdGhpcy5jYXJvdXNlbC5fYWxpZ25FbGVtZW50cyhkaXN0YW5jZSk7XHJcblxyXG4gICAgICAgIHZhciBvcHREaXN0YW5jZSA9IHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKTtcclxuXHJcbiAgICAgICAgdmFyIHNjcm9sbGVkRWxlbWVudHMgPSBwYXJzZUludChkaXN0YW5jZSAvIG9wdERpc3RhbmNlLCAxMCk7XHJcblxyXG4gICAgICAgIHZhciBwcmV2SW5kZXggPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleDtcclxuICAgICAgICB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCAtPSBzY3JvbGxlZEVsZW1lbnRzO1xyXG5cclxuICAgICAgICB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCA9IE1hdGgubWF4KDAsIHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zZWxlY3RlZEluZGV4KTtcclxuICAgICAgICB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCA9IE1hdGgubWluKHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zZWxlY3RlZEluZGV4LCB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aCAtIDEpO1xyXG5cclxuICAgICAgICBpZiAocHJldkluZGV4ICE9IHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zZWxlY3RlZEluZGV4KVxyXG4gICAgICAgICAgICB0aGlzLmNhcm91c2VsLl9yYWlzZUNoYW5nZUV2ZW50KCk7XHJcblxyXG4gICAgICAgIHJldHVybiB7IGRpc3RhbmNlOiAocHJldkluZGV4IC0gdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXgpICogb3B0RGlzdGFuY2UsIGhpZ2hGcmljdGlvblJhbmdlOiBoaWdoRnJpY3Rpb25SYW5nZSB9O1xyXG4gICAgfTtcclxuXHJcbiAgICBvYmouaGFuZGxlTW90aW9uRW5kID0gZnVuY3Rpb24gKHJlbWFpbmluZ0Rpc3RhbmNlKSB7XHJcbiAgICAgICAgaWYgKHJlbWFpbmluZ0Rpc3RhbmNlID09IDApXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuXHJcbiAgICAgICAgdmFyIHRhcmdldEluZGV4ID0gdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXg7XHJcblxyXG4gICAgICAgIGlmIChNYXRoLmFicyhyZW1haW5pbmdEaXN0YW5jZSkgPiB0aGlzLmdldEFjdHVhbERpc3RhbmNlKCkgLyAyKSB7XHJcbiAgICAgICAgICAgIGlmIChyZW1haW5pbmdEaXN0YW5jZSA8IDApXHJcbiAgICAgICAgICAgICAgICB0YXJnZXRJbmRleCsrO1xyXG4gICAgICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgICAgICB0YXJnZXRJbmRleC0tO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoID09IDApXHJcbiAgICAgICAgICAgIHRhcmdldEluZGV4ID0gMDtcclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgdGFyZ2V0SW5kZXggPSBNYXRoLm1heCgwLCB0YXJnZXRJbmRleCk7XHJcbiAgICAgICAgICAgIHRhcmdldEluZGV4ID0gTWF0aC5taW4odGFyZ2V0SW5kZXgsIHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoIC0gMSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgdGFyZ2V0RGlzdGFuY2UgPSAodGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXggLSB0YXJnZXRJbmRleCkgKiB0aGlzLmdldEFjdHVhbERpc3RhbmNlKCk7XHJcblxyXG4gICAgICAgIHZhciBkdXJhdGlvbiA9IE1hdGguYWJzKHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5yb3RhdGlvbkFuaW1hdGlvbkR1cmF0aW9uICogKHJlbWFpbmluZ0Rpc3RhbmNlIC8gdGhpcy5nZXRBY3R1YWxEaXN0YW5jZSgpKSk7XHJcbiAgICAgICAgZHVyYXRpb24gPSBNYXRoLm1pbihkdXJhdGlvbiwgdGhpcy5jYXJvdXNlbC5vcHRpb25zLnJvdGF0aW9uQW5pbWF0aW9uRHVyYXRpb24gLyAyKTtcclxuXHJcbiAgICAgICAgdGhpcy5jYXJvdXNlbC5hbmltYXRpb24uYW5pbWF0ZShyZW1haW5pbmdEaXN0YW5jZSwgdGFyZ2V0RGlzdGFuY2UsIHRhcmdldEluZGV4LCBudWxsLCBkdXJhdGlvbik7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5hbGlnbkVsZW1lbnRzID0gZnVuY3Rpb24gKGFuaW1hdGlvblNoaWZ0KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY2Fyb3VzZWwuZWxlbWVudHMubGVuZ3RoID09IDAgfHwgdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXggPCAwKVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcblxyXG4gICAgICAgIHRoaXMuY2Fyb3VzZWwuY29udGFpbmVyU2l6ZSA9IHRoaXMuY2Fyb3VzZWwuX2dldENvbnRhaW5lclNpemUoKTtcclxuXHJcbiAgICAgICAgdmFyIHNoaWZ0ID0gMDtcclxuICAgICAgICBpZiAodHlwZW9mIChhbmltYXRpb25TaGlmdCkgIT0gXCJ1bmRlZmluZWRcIilcclxuICAgICAgICAgICAgc2hpZnQgPSBhbmltYXRpb25TaGlmdDtcclxuXHJcbiAgICAgICAgdmFyIGhpZ2hGcmljdGlvblJhbmdlID0gZmFsc2U7XHJcbiAgICAgICAgLy8gc2xvdyBkb3duIGF0IHRoZSBlbmRzXHJcbiAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAodGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXggPT0gMCAmJiBzaGlmdCA+IDApIHx8XHJcbiAgICAgICAgICAgICh0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCA9PSB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aCAtIDEgJiYgc2hpZnQgPCAwKVxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgICBzaGlmdCA9IE1hdGgucG93KE1hdGguYWJzKHNoaWZ0KSwgMC43KSAqIChzaGlmdCAvIE1hdGguYWJzKHNoaWZ0KSk7XHJcbiAgICAgICAgICAgIGhpZ2hGcmljdGlvblJhbmdlID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHZhciBsb2NhdGlvbiA9IHRoaXMuZ2V0Um9vdFZhbHVlKCk7XHJcbiAgICAgICAgdmFyIHJhbmdlU2hpZnQgPSAwO1xyXG4gICAgICAgIGlmICgodGhpcy5jYXJvdXNlbC5vcHRpb25zLnNlbGVjdGVkSW5kZXggPT0gMCAmJiBzaGlmdCA+IDApIHx8ICh0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCA9PSB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aCAtIDEgJiYgc2hpZnQgPCAwKSlcclxuICAgICAgICAgICAgcmFuZ2VTaGlmdCA9IHNoaWZ0O1xyXG4gICAgICAgIHZhciByYW5nZXMgPSB0aGlzLmdldEZhZGVSYW5nZXMobG9jYXRpb24gKyByYW5nZVNoaWZ0KTtcclxuXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5zZWxlY3RlZEluZGV4OyBpIDwgdGhpcy5jYXJvdXNlbC5lbGVtZW50cy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLnNldEVsZW1lbnRQb3NpdGlvbih0aGlzLmNhcm91c2VsLmVsZW1lbnRzW2ldLCBsb2NhdGlvbiArIHNoaWZ0LCByYW5nZXMpO1xyXG4gICAgICAgICAgICBsb2NhdGlvbiA9IHRoaXMuaW5jcmVtZW50VmFsdWUobG9jYXRpb24sIHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsb2NhdGlvbiA9IHRoaXMuZ2V0Um9vdFZhbHVlKCk7XHJcblxyXG4gICAgICAgIGZvciAodmFyIGkgPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2VsZWN0ZWRJbmRleCAtIDE7IGkgPj0gMDsgaS0tKSB7XHJcbiAgICAgICAgICAgIGxvY2F0aW9uID0gdGhpcy5kZWNyZW1lbnRWYWx1ZShsb2NhdGlvbiwgdGhpcy5nZXRBY3R1YWxEaXN0YW5jZSgpKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRFbGVtZW50UG9zaXRpb24odGhpcy5jYXJvdXNlbC5lbGVtZW50c1tpXSwgbG9jYXRpb24gKyBzaGlmdCwgcmFuZ2VzKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc2V0WkluZGV4ZXMoKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGhpZ2hGcmljdGlvblJhbmdlO1xyXG4gICAgfTtcclxuXHJcbiAgICBvYmouc2V0RWxlbWVudFBvc2l0aW9uID0gZnVuY3Rpb24gKGVsZW1lbnQsIHZhbHVlLCByYW5nZXMpIHtcclxuICAgICAgICAvL3RoaXMgbWV0aG9kIGlzIHBlcmZvcm1hbmNlIGNyaXRpY2FsIHNvIHdlIHRyeWluZyB0byBhdm9pZCBqUXVlcnkgdXNhZ2VcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc2V0RWxlbWVudFZpc2liaWxpdHkocmFuZ2VzLCBlbGVtZW50LCB2YWx1ZSkpIHtcclxuXHJcbiAgICAgICAgICAgIHZhciBwb2ludCA9IHRoaXMuY2Fyb3VzZWwucGF0aC5nZXRQb2ludCh2YWx1ZSk7XHJcblxyXG4gICAgICAgICAgICB2YXIgZWxlbWVudFRyYW5zZm9ybSA9IG5ldyB0cmFuc2Zvcm0oKTtcclxuXHJcbiAgICAgICAgICAgIGVsZW1lbnRUcmFuc2Zvcm0udHJhbnNsYXRlWiA9IHBvaW50LnogKiB0aGlzLmNhcm91c2VsLm9wdGlvbnMuc2NhbGVaO1xyXG4gICAgICAgICAgICBlbGVtZW50VHJhbnNmb3JtLnRyYW5zbGF0ZVggPSBwb2ludC54ICogdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNjYWxlWCArIHRoaXMuY2Fyb3VzZWwuY29udGFpbmVyU2l6ZS53aWR0aCAvIDIgLSBlbGVtZW50LnNpemUud2lkdGggLyAyO1xyXG4gICAgICAgICAgICBlbGVtZW50VHJhbnNmb3JtLnRyYW5zbGF0ZVkgPSBwb2ludC55ICogdGhpcy5jYXJvdXNlbC5vcHRpb25zLnNjYWxlWSArIHRoaXMuY2Fyb3VzZWwuY29udGFpbmVyU2l6ZS5oZWlnaHQgLyAyIC0gZWxlbWVudC5zaXplLmhlaWdodCAvIDI7XHJcblxyXG4gICAgICAgICAgICB2YXIgcGF0aFJvdGF0aW9uID0gdGhpcy5jYXJvdXNlbC5wYXRoLmVsZW1lbnRzUm90YXRpb24oKTtcclxuICAgICAgICAgICAgaWYgKHBhdGhSb3RhdGlvbilcclxuICAgICAgICAgICAgICAgIGVsZW1lbnRUcmFuc2Zvcm0ucm90YXRpb25zLnB1c2gocGF0aFJvdGF0aW9uKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmNhcm91c2VsLm9wdGlvbnMubW9kZTNEID09ICdzY2FsZScpIHtcclxuICAgICAgICAgICAgICAgIGVsZW1lbnRUcmFuc2Zvcm0uc2NhbGUgPSB0aGlzLmNhcm91c2VsLm9wdGlvbnMucGVyc3BlY3RpdmUgLyAodGhpcy5jYXJvdXNlbC5vcHRpb25zLnBlcnNwZWN0aXZlIC0gZWxlbWVudFRyYW5zZm9ybS50cmFuc2xhdGVaKTtcclxuICAgICAgICAgICAgICAgIGVsZW1lbnRUcmFuc2Zvcm0udHJhbnNsYXRlWiA9IDA7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5jYXJvdXNlbC5lZmZlY3RzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5jYXJvdXNlbC5lZmZlY3RzW2ldLmFwcGx5UGhhc2UgPT09ICdwb3NpdGlvbmluZycpXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5lZmZlY3RzW2ldLmFwcGx5KGVsZW1lbnRUcmFuc2Zvcm0sIGVsZW1lbnQsIHZhbHVlKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZWxlbWVudFRyYW5zZm9ybS5hcHBseShlbGVtZW50LmVsZW1lbnQpO1xyXG4gICAgICAgICAgICBlbGVtZW50LmxvY2F0aW9uID0gcG9pbnQ7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5zZXRaSW5kZXhlcyA9IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICAgICAgdmFyIHRtcEVsZW1lbnRzID0gW107XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHZhciBlID0gdGhpcy5jYXJvdXNlbC5lbGVtZW50c1tpXTtcclxuICAgICAgICAgICAgaWYgKGUubG9jYXRpb24pIC8vIGVsZW1lbnQgaGFzIGJlZW4gcG9zaXRpb25lZFxyXG4gICAgICAgICAgICAgICAgdG1wRWxlbWVudHMucHVzaChlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRtcEVsZW1lbnRzLnNvcnQoZnVuY3Rpb24gKGUxLCBlMikge1xyXG4gICAgICAgICAgICByZXR1cm4gZTEubG9jYXRpb24ueiAtIGUyLmxvY2F0aW9uLno7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0bXBFbGVtZW50cy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICB0bXBFbGVtZW50c1tpXS4kZWxlbWVudC5nZXQoMCkuc3R5bGUuekluZGV4ID0gaTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5nZXRGYWRlUmFuZ2VzID0gZnVuY3Rpb24gKHJvb3QpIHtcclxuICAgICAgICB2YXIgbG9jYXRpb24gPSByb290O1xyXG5cclxuICAgICAgICB2YXIgcmVzID0gW107XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmNhcm91c2VsLm9wdGlvbnMubnVtYmVyT2ZFbGVtZW50c1RvRGlzcGxheUxlZnQgIT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBmcm9tOiB0aGlzLmRlY3JlbWVudFZhbHVlKGxvY2F0aW9uLCB0aGlzLmdldEFjdHVhbERpc3RhbmNlKCkgKiAodGhpcy5jYXJvdXNlbC5vcHRpb25zLm51bWJlck9mRWxlbWVudHNUb0Rpc3BsYXlMZWZ0ICsgMSkpLFxyXG4gICAgICAgICAgICAgICAgdG86IHRoaXMuZGVjcmVtZW50VmFsdWUobG9jYXRpb24sIHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKSAqICh0aGlzLmNhcm91c2VsLm9wdGlvbnMubnVtYmVyT2ZFbGVtZW50c1RvRGlzcGxheUxlZnQpKSxcclxuICAgICAgICAgICAgICAgIGhpZGU6ICdiZWZvcmUnXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY2Fyb3VzZWwub3B0aW9ucy5udW1iZXJPZkVsZW1lbnRzVG9EaXNwbGF5UmlnaHQgIT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBmcm9tOiB0aGlzLmluY3JlbWVudFZhbHVlKGxvY2F0aW9uLCB0aGlzLmdldEFjdHVhbERpc3RhbmNlKCkgKiAodGhpcy5jYXJvdXNlbC5vcHRpb25zLm51bWJlck9mRWxlbWVudHNUb0Rpc3BsYXlSaWdodCkpLFxyXG4gICAgICAgICAgICAgICAgdG86IHRoaXMuaW5jcmVtZW50VmFsdWUobG9jYXRpb24sIHRoaXMuZ2V0QWN0dWFsRGlzdGFuY2UoKSAqICh0aGlzLmNhcm91c2VsLm9wdGlvbnMubnVtYmVyT2ZFbGVtZW50c1RvRGlzcGxheVJpZ2h0ICsgMSkpLFxyXG4gICAgICAgICAgICAgICAgaGlkZTogJ2FmdGVyJ1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiByZXM7XHJcbiAgICB9O1xyXG5cclxuICAgIG9iai5zZXRFbGVtZW50VmlzaWJpbGl0eSA9IGZ1bmN0aW9uIChmYWRlUmFuZ2VzLCBlbGVtZW50LCB2YWx1ZSkge1xyXG4gICAgICAgIHZhciAkZWxlbWVudCA9IGVsZW1lbnQuJGVsZW1lbnQ7XHJcbiAgICAgICAgdmFyIGhpZGRlbiA9IGZhbHNlO1xyXG5cclxuICAgICAgICBpZiAoKHRoaXMubWluVmFsdWUoKSAhPSBudWxsICYmIHZhbHVlIDwgdGhpcy5taW5WYWx1ZSgpKVxyXG4gICAgICAgICAgICB8fCAodGhpcy5tYXhWYWx1ZSgpICE9IG51bGwgJiYgdmFsdWUgPiB0aGlzLm1heFZhbHVlKCkpKVxyXG4gICAgICAgICAgICBoaWRkZW4gPSB0cnVlO1xyXG4gICAgICAgIGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgaWYgKGZhZGVSYW5nZXMubGVuZ3RoID09IDApXHJcbiAgICAgICAgICAgICAgICAkZWxlbWVudC5jc3MoeyBvcGFjaXR5OiAxIH0pO1xyXG5cclxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmYWRlUmFuZ2VzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgcmFuZ2UgPSBmYWRlUmFuZ2VzW2ldO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICgocmFuZ2UuaGlkZSA9PSAnYmVmb3JlJyAmJiB2YWx1ZSA8PSByYW5nZS5mcm9tKSB8fCAocmFuZ2UuaGlkZSA9PSAnYWZ0ZXInICYmIHZhbHVlID49IHJhbmdlLnRvKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGhpZGRlbiA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHZhbHVlID4gcmFuZ2UuZnJvbSAmJiB2YWx1ZSA8IHJhbmdlLnRvKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGRpc3RhbmNlID0gcmFuZ2UudG8gLSByYW5nZS5mcm9tO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBwYXNzZWQgPSBNYXRoLmFicyh2YWx1ZSAtIChyYW5nZS5oaWRlID09ICdhZnRlcicgPyByYW5nZS5mcm9tIDogcmFuZ2UudG8pKTtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgb3BhY2l0eSA9IChkaXN0YW5jZSAtIHBhc3NlZCkgLyBkaXN0YW5jZTtcclxuICAgICAgICAgICAgICAgICAgICAkZWxlbWVudC5jc3MoeyBvcGFjaXR5OiBvcGFjaXR5IH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAkZWxlbWVudC5jc3MoeyBvcGFjaXR5OiAxIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNldEVsZW1lbnRWaXNpYmlsaXR5SW50ZXJuYWwoaGlkZGVuLCBlbGVtZW50KTtcclxuICAgICAgICByZXR1cm4gIWhpZGRlbjtcclxuICAgIH07XHJcblxyXG4gICAgb2JqLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmNhcm91c2VsLmVsZW1lbnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2Fyb3VzZWwuZWxlbWVudHNbaV0uJGVsZW1lbnQuY3NzKHtcclxuICAgICAgICAgICAgICAgIGxlZnQ6ICcwJyxcclxuICAgICAgICAgICAgICAgIHRvcDogJzAnLFxyXG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAnJyxcclxuICAgICAgICAgICAgICAgIG9wYWNpdHk6ICcxJ1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5jYXJvdXNlbC5lbGVtZW50c1tpXS4kZWxlbWVudC5zaG93KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBvYmouc2V0RWxlbWVudFZpc2liaWxpdHlJbnRlcm5hbCA9IGZ1bmN0aW9uIChoaWRlLCBlbGVtZW50KSB7XHJcbiAgICAgICAgaWYgKGhpZGUpIHtcclxuICAgICAgICAgICAgZWxlbWVudC4kZWxlbWVudC5oaWRlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZWxlbWVudC4kZWxlbWVudC5zaG93KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxufSIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKHdpZHRoLCBoZWlnaHQpIHtcclxuICAgIHRoaXMud2lkdGggPSB3aWR0aDtcclxuICAgIHRoaXMuaGVpZ2h0ID0gaGVpZ2h0O1xyXG59OyIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oKSB7XHJcbiAgICB0aGlzLnRyYW5zbGF0ZVggPSAwO1xyXG4gICAgdGhpcy50cmFuc2xhdGVZID0gMDtcclxuICAgIHRoaXMudHJhbnNsYXRlWiA9IDA7XHJcbiAgICB0aGlzLnNjYWxlID0gMTtcclxuXHJcbiAgICB0aGlzLnNjYWxlWCA9IDE7XHJcbiAgICB0aGlzLnNjYWxlWSA9IDE7XHJcbiAgICB0aGlzLnNjYWxlWiA9IDE7XHJcblxyXG4gICAgdGhpcy5yb3RhdGlvbnMgPSBbXTtcclxuXHJcbiAgICB0aGlzLmFwcGx5ID0gZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICB2YXIgc3RyID0gJyc7XHJcbiAgICAgICAgaWYgKHRoaXMudHJhbnNsYXRlWCAhPSAwIHx8IHRoaXMudHJhbnNsYXRlWSAhPSAwIHx8IHRoaXMudHJhbnNsYXRlWiAhPSAwKVxyXG4gICAgICAgICAgICBzdHIgPSAndHJhbnNsYXRlM2QoJyArIHRoaXMudHJhbnNsYXRlWCArICdweCwgJyArIHRoaXMudHJhbnNsYXRlWSArICdweCwgJyArIHRoaXMudHJhbnNsYXRlWiArICdweCknO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zY2FsZSAhPSAxKVxyXG4gICAgICAgICAgICBzdHIgKz0gJyBzY2FsZSgnICsgdGhpcy5zY2FsZSArICcpJztcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc2NhbGVYICE9IDEpXHJcbiAgICAgICAgICAgIHN0ciArPSAnIHNjYWxlWCgnICsgdGhpcy5zY2FsZVggKyAnKSc7XHJcbiAgICAgICAgaWYgKHRoaXMuc2NhbGVZICE9IDEpXHJcbiAgICAgICAgICAgIHN0ciArPSAnIHNjYWxlWSgnICsgdGhpcy5zY2FsZVkgKyAnKSc7XHJcbiAgICAgICAgaWYgKHRoaXMuc2NhbGVaICE9IDEpXHJcbiAgICAgICAgICAgIHN0ciArPSAnIHNjYWxlWignICsgdGhpcy5zY2FsZVogKyAnKSc7XHJcblxyXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5yb3RhdGlvbnMubGVuZ3RoOyBpKyspXHJcbiAgICAgICAgICAgIHN0ciArPSAnICcgKyB0aGlzLnJvdGF0aW9uc1tpXS5nZXRTdHJpbmcoKTtcclxuXHJcbiAgICAgICAgZS5zdHlsZS50cmFuc2Zvcm0gPSBzdHI7XHJcbiAgICAgICAgZS5zdHlsZS53ZWJraXRUcmFuc2Zvcm0gPSBzdHI7XHJcbiAgICAgICAgZS5zdHlsZS5tc1RyYW5zZm9ybSA9IHN0cjtcclxuICAgIH07XHJcbn07IiwidmFyIHV0aWxzID0gZnVuY3Rpb24oKSB7XHJcbiAgICB0aGlzLmdldE9iamVjdFByb3BlcnR5VmFsdWUgPSBmdW5jdGlvbiAob2JqLCBwcm9wZXJ0eSkge1xyXG4gICAgICAgIHZhciBwYXJ0cyA9IHByb3BlcnR5LnNwbGl0KCcuJyk7XHJcbiAgICAgICAgdmFyIHJlcyA9IG9iajtcclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHBhcnRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHJlcyA9IHJlc1twYXJ0c1tpXV07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzO1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLnNldE9iamVjdFByb3BlcnR5VmFsdWUgPSBmdW5jdGlvbiAob2JqLCBwcm9wZXJ0eSwgdmFsdWUpIHtcclxuICAgICAgICB2YXIgcGFydHMgPSBwcm9wZXJ0eS5zcGxpdCgnLicpO1xyXG4gICAgICAgIHZhciB0YXJnZXQgPSBvYmo7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYXJ0cy5sZW5ndGggLSAxOyBpKyspIHtcclxuICAgICAgICAgICAgdGFyZ2V0ID0gdGFyZ2V0W3BhcnRzW2ldXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRhcmdldFtwYXJ0c1twYXJ0cy5sZW5ndGggLSAxXV0gPSB2YWx1ZTtcclxuICAgIH1cclxufVxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBuZXcgdXRpbHMoKTsiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICB0aGlzLnggPSAwO1xyXG4gICAgdGhpcy55ID0gMDtcclxuICAgIHRoaXMueiA9IDA7XHJcblxyXG4gICAgdGhpcy5pbml0RnJvbVBvaW50cyA9IGZ1bmN0aW9uIChwMSwgcDIpIHtcclxuICAgICAgICB0aGlzLnggPSBwMi54IC0gcDEueDtcclxuICAgICAgICB0aGlzLnkgPSBwMi55IC0gcDEueTtcclxuICAgICAgICB0aGlzLnogPSBwMi56IC0gcDEuejtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5ub3JtYWxpemUgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGxlbmd0aCA9IHRoaXMubGVuZ3RoKCk7XHJcbiAgICAgICAgdGhpcy54IC89IGxlbmd0aDtcclxuICAgICAgICB0aGlzLnkgLz0gbGVuZ3RoO1xyXG4gICAgICAgIHRoaXMueiAvPSBsZW5ndGg7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMubGVuZ3RoID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiBNYXRoLnNxcnQoKHRoaXMueCAqIHRoaXMueCkgKyAodGhpcy55ICogdGhpcy55KSArICh0aGlzLnogKiB0aGlzLnopKTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5hbmdsZSA9IGZ1bmN0aW9uICh2KSB7XHJcblxyXG4gICAgICAgIHZhciBzY2FsYXJNdWx0aXBsaWNhdGlvbiA9IHRoaXMueCAqIHYueCArIHRoaXMueSAqIHYueSArIHRoaXMueiAqIHYuejtcclxuICAgICAgICB2YXIgYWJzVGhpcyA9IHRoaXMubGVuZ3RoKCk7XHJcbiAgICAgICAgdmFyIGFic1YgPSBNYXRoLnNxcnQodi54ICogdi54ICsgdi55ICogdi55ICsgdi56ICogdi56KTtcclxuXHJcbiAgICAgICAgdmFyIGNvc0EgPSBzY2FsYXJNdWx0aXBsaWNhdGlvbiAvIChhYnNUaGlzICogYWJzVik7XHJcblxyXG4gICAgICAgIHZhciBhID0gTWF0aC5hY29zKGNvc0EpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy56ID4gdi56KVxyXG4gICAgICAgICAgICBhICo9IC0xO1xyXG5cclxuICAgICAgICByZXR1cm4gYSAqIDE4MCAvIE1hdGguUEk7XHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMucGVycGVuZGljdWxhclggPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIHJlcyA9IG5ldyB2ZWN0b3IoKTtcclxuICAgICAgICByZXMueSA9IHRoaXMueTtcclxuICAgICAgICByZXMueiA9IHRoaXMuejtcclxuICAgICAgICByZXMueCA9IC0xICogKHRoaXMueSAqIHJlcy55ICsgdGhpcy56ICogcmVzLnopIC8gdGhpcy54O1xyXG4gICAgICAgIHJldHVybiByZXM7XHJcbiAgICB9O1xyXG59OyJdfQ==

var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'gilde';
  locals.filters = {
		post: req.params,
	};
  locals.data = {
    posts: [],
  };

	// Load the galleries by sortOrder
	// view.query('galleries', keystone.list('Gallery').model.find().sort('sortOrder'));
  // Load other posts
	view.on('init', function (next) {
		var q = keystone.list('Post').model.find()
      .populate( 'category', null, { key: locals.section } )

		q.exec(function (err, results) {
			locals.data.posts = results.filter(function(res){
        return res.category != null;
      });
      console.log(locals.data.posts);
			next(err);
		});

	});
	// Render the view
	view.render('gilde');

};

var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'gilde';
  locals.filters = {
		post: req.params,
	};
  locals.data = {
    post: [],
  };
  console.log(locals.filters);

	view.on('init', function (next) {
		var q = keystone.list('Post').model.findOne({'slug': locals.filters.post.detailPage})
      // .populate( 'category', null, { key: locals.section } )

		q.exec(function (err, results) {
			locals.data.post = results
      console.log(locals.data.post);
			next(err);
		});

	});
	// Render the view
	view.render('detail');

};

var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';

	locals.data = {
		posts: [],
	};

	view.on('init', function (next) {

		keystone.list('Post').model.find()
			.populate('category')
			.then(function(posts){
				locals.data.posts = posts;
				posts.forEach(function(post){
					console.log(post);
				})
				next();
			})
	});
	// Render the view
	view.render('index');
};

/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
};

// Setup Route Bindings
exports = module.exports = function (app) {
	// Views
	app.get('/', routes.views.index);
	app.get('/blog/:category?', routes.views.blog);
	app.get('/blog/post/:post', routes.views.post);
	app.get('/gallery', routes.views.gallery);
	app.get('/creaties', routes.views.creaties);
	app.get('/creaties/info/:detailPage', routes.views.detail);
	app.get('/spinsels', routes.views.spinsels);
	app.get('/spinsels/info/:detailPage', routes.views.detail);
	app.get('/gilde', routes.views.gilde);
	app.get('/gilde/info', routes.views.gilde_info);
	app.get('/gilde/info/:detailPage', routes.views.detail);
	app.get('/main', routes.views.main);
	app.get('/main/:detailPage', routes.views.detail);
	app.get('/communities', routes.views.communities);
	app.get('/communities/info', routes.views.communities_info);
	app.get('/communities/info/:detailPage', routes.views.detail);
	app.all('/contact', routes.views.contact);

	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);

};
